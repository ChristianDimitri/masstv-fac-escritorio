Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class BrwFacturas_Cancelar

    Private customersByCityReport As ReportDocument
    Dim bloqueado, identi As Integer
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)
        If IdSistema = "SA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
            ba = True
        ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            ba = True
        ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReportesFacturasXsd.rpt"
            ba = True
        ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        Else
            If IdSistema = "VA" Then
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            Else
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            End If
        End If

        ReportesFacturasXsd(Clv_Factura, 0, 0, "01-01-1900", "01-01-1900", 0, reportPath)
        'End If

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '' End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

        If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then
            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        Else
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        End If

        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        If path.Contains("reynosa") = True Then
            customersByCityReport = New ReporteCajasTickets_2AG
        Else
            customersByCityReport = New ReporteCajasTickets_2SA

        End If
        'Select Case IdSistema
        '    Case "VA"
        '        customersByCityReport = New ReporteCajasTickets_2VA
        '    Case "LO"
        '        customersByCityReport = New ReporteCajasTickets_2Log
        '    Case "AG"
        '        customersByCityReport = New ReporteCajasTickets_2AG
        '    Case "SA"
        '        customersByCityReport = New ReporteCajasTickets_2SA
        '    Case "TO"
        '        customersByCityReport = New ReporteCajasTickets_2TOM
        '    Case Else
        '        customersByCityReport = New ReporteCajasTickets_2OLD
        'End Select

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable

        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)


        'customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, GloClv_Factura)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub


    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    '    Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '    For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '        myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '    Next
    'End Sub

    Private Sub BrwFacturas_Cancelar_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'entra cuando se ha cancelado una factura
        Try

            If eEntra = True Then
                eEntra = False
                Dim MSG As String = Nothing
                Dim BNDERROR As Integer = 0
                Dim CON As New SqlConnection(MiConexion)
                If IdSistema = "LO" Then

                    CON.Open()
                    Me.CANCELACIONFACTURASTableAdapter.Connection = CON
                    Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
                    CON.Close()
                Else

                    ''INICIO CANCELACION FACTURA DIGITAL
                    'CANCELA LA FACTURA
                    DameId_FacturaCDF(Me.Clv_FacturaLabel1.Text, "N")
                    If GloClv_FacturaCFD > 0 Then
                        Cancelacion_FacturaCFD(GloClv_FacturaCFD)
                    End If


                    CON.Open()
                    '--Me.CANCELACIONFACTURASTableAdapter.Connection = CON
                    '--Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)


                    Dim comando2 As New SqlClient.SqlCommand
                    With comando2
                        .CommandText = "CANCELACIONFACTURAS"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON
                        Dim prm As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.Clv_FacturaLabel1.Text
                        .Parameters.Add(prm)

                        Dim prm2 As New SqlParameter("@OP", SqlDbType.Int)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = 0
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@MSG", SqlDbType.VarChar, 250)
                        prm3.Direction = ParameterDirection.Output
                        prm3.Value = ""
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@BNDERROR", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Output
                        prm4.Value = 0
                        .Parameters.Add(prm4)

                        Dim i As Integer = comando2.ExecuteNonQuery

                    End With




                    Me.CancelaCNRPPETableAdapter.Connection = CON
                    Me.CancelaCNRPPETableAdapter.Fill(Me.EricDataSet.CancelaCNRPPE, Me.Clv_FacturaLabel1.Text)
                    Me.CancelaCambioServClienteTableAdapter.Connection = CON
                    Me.CancelaCambioServClienteTableAdapter.Fill(Me.DataSetEdgar.CancelaCambioServCliente, Me.Clv_FacturaLabel1.Text)
                    Dim comando As New SqlClient.SqlCommand
                    With comando
                        .CommandText = "Cancela_NotasDeFactura"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON
                        Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.Clv_FacturaLabel1.Text

                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery

                    End With
                    CON.Close()
                End If

                
                bitsist(GloUsuario, Me.Clv_FacturaLabel1.Text, GloSistema, Me.Name, "", "Se Cancelo la Factura", "El DIa: " + Me.FECHADateTimePicker.Text, LocClv_Ciudad)
                FrmMotivoCancelacionFactura.Show()
                'MsgBox(MSG)
                If BNDERROR = 0 Then
                    Busca(0)
                End If

            End If

            'entra cuando se ha reimpreso una factura
            If eEntraReImprimir = True Then
                eEntraReImprimir = False
                GloClv_Factura = Me.Clv_FacturaLabel1.Text
                'Genera_Factura_Digital(GloClv_Factura)
                'GeneraDocumentoTxt_DIGITAL(GloClv_Factura, "\\192.168.123.11\exes\softv\FacturaDig")
                GloClv_Factura = Me.Clv_FacturaLabel1.Text

                identi = BusFacFiscalOledb(GloClv_Factura)

                '------------------------------- FIN DE BONIFICACIONES
                If CInt(identi) > 0 Then
                    DameId_FacturaCDF(GloClv_Factura, "N")
                    ''FrmImprimirFacturaDigital.Show()
                    Locop = 0
                    'If GloClv_FacturaCFD > 0 Then
                    Try
                        FormPruebaDigital.Show()
                    Catch ex As Exception

                    End Try
                    Dim r As New Globalization.CultureInfo("es-MX")
                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
                    System.Threading.Thread.CurrentThread.CurrentCulture = r
                    'Else
                    '    MsgBox("No Existe una Factura Digital de este Recibo", vbInformation)
                    'End If

                    'SAUL IMPRIMIR FACTURA FISCAL
                    'LiTipo = 2
                    'FrmImprimir.ShowDialog()
                    'SAUL IMPRIMIR FACTURA FISCAL(FIN)
                Else
                    'ConfigureCrystalReports(GloClv_Factura)
                    AuxImprimirTicket = 1
                    LiTipo = 2
                    'FrmImprimir.Show()
                    Dim Fmrimp3 As New FrmImprimir
                    Fmrimp3.ShowDialog()
                End If

                'If IdSistema = "AG" Or IdSistema = "VA" Then
                '    LiTipo = 2
                '    FrmImprimir.Show()
                'Else
                '    If IdSistema = "LO" Then
                '        LiTipo = 6
                '        FrmImprimir.Show()
                '    Else
                '        If LocImpresoraTickets = "" Then
                '            MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                '        Else
                '            ConfigureCrystalReports(GloClv_Factura)
                '        End If
                '    End If
                'End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Private Sub BrwFacturas_Cancelar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        Dim op As Integer
        CON.Open()

        eEntra = False
        eEntraReImprimir = False
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRATIPOFACTURA' Puede moverla o quitarla seg�n sea necesario.
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewsoftvDataSet1.DAMEFECHADELSERVIDOR, Me.FECHADateTimePicker.Text)

        If IdSistema = "LO" Then
            CMBLabel5.Text = "Tipo de Pago"
            CMBLabel1.Text = "Buscar Pago por :"
            Button2.Text = "&Cancelar Pago"
            Button6.Text = "&Reimprimir Pago"
            Button8.Text = "&Ver Pago"
            Label8.Text = "Datos del Pago"
        ElseIf IdSistema = "AG" Then
            op = 2
        Else
            op = 1
        End If
        CON.Close()
        'Tipo_Factura()
        If GloOpFacturas = 0 Then
            If IdSistema = "LO" Then
                Me.Text = "Cancelaci�n de Pagos"
            Else
                Me.Text = "Cancelaci�n de Facturas"
            End If
            Me.CMBPanel2.Visible = True
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            Me.Button6.TabStop = False
            Me.Button8.TabStop = False
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, 1)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON1
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            BuscaFacturas(0, "", 0, "01/01/1900", 0, "", GloTipo)
            CON1.Close()
        ElseIf GloOpFacturas = 1 Then
            If IdSistema = "LO" Then
                Me.Text = "Reimpresi�n de Pagos"
            Else
                Me.Text = "Reimpresi�n de Facturas"
            End If
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = True
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            GloTipo = CStr(Me.ComboBox4.SelectedValue)
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, 1)
            CON.Close()
            Busca(0)
            Me.Button8.TabStop = False
            Me.Button2.TabStop = False
        ElseIf GloOpFacturas = 3 Then
            Me.Text = "Ver Historial de Pagos"
            'Me.ComboBox4.SelectedValue = GloTipo
            GloTipo = CStr(Me.ComboBox4.SelectedValue)
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = True
            Me.Panel5.Visible = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, op)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON2
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 13, "", 0, "01/01/1900", GloContrato, "", GloTipo)
            BuscaFacturas(13, "", 0, "01/01/1900", GloContrato, "", GloTipo)
            CON2.Close()
            Me.Button6.TabStop = False
            Me.Button2.TabStop = False
        End If

        dtpFecha.Value = DateTime.Today

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub buscaNotas(ByVal opcion As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Try
            conlidia.Open()
            Me.BUSCANOTASDECREDITOTableAdapter.Connection = conlidia
            If opcion = 5 Then
                'If Me.FECHATextBox.Text.Length = 0 Or IsDate(Me.FECHATextBox.Text) = False Then
                '    Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, 3, 0, "01/01/1900", GloContrato, 0, "")
                'Else
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, dtpFecha.Value, GloContrato, 0, "")
                'End If
            ElseIf opcion = 6 Then
            Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, 0, "")
            ElseIf opcion = 2 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, dtpFecha.Value, 0, 0, "")
            ElseIf opcion = 3 Then
            Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, "01/01/1900", Me.CONTRATOTextBox.Text, 0, "")
            ElseIf opcion = 4 Then
            Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, 3, 0, "01/01/1900", GloContrato, 0, "")
            End If

            conlidia.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Busca(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloOpFacturas = 3 Then
                If OP = 0 Then
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 10, "", 0, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(10, "", 0, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)

                ElseIf OP = 2 Then
                    'If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 12, "", 0, Me.FECHATextBox.Text, GloContrato, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(12, "", 0, dtpFecha.Value, GloContrato, "", Me.ComboBox4.SelectedValue)
                End If
            Else
                If OP = 0 Then
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(0, "", 0, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 2 Then
                    'If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 2, "", 0, Me.FECHATextBox.Text, 0, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(2, "", 0, dtpFecha.Value, 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 3 Then
                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 3, "", 0, "01/01/1900", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                    BuscaFacturas(3, "", 0, "01/01/1900", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 4 Then
                    If Len(Trim(Me.NOMBRETextBox.Text)) > 0 Then
                        'Me.BUSCAFACTURASTableAdapter.Connection = CON
                        'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 4, "", 0, "01/01/1900", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                        BuscaFacturas(4, "", 0, "01/01/1900", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                    Else
                        MsgBox("La B�squeda no se puede realizar sin Datos", MsgBoxStyle.Information)
                    End If
                End If
            End If
            CON.Close()
            Me.SERIETextBox.Clear()
            Me.FOLIOTextBox.Clear()
            Me.FECHATextBox.Clear()
            Me.CONTRATOTextBox.Clear()
            Me.NOMBRETextBox.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If Me.ComboBox4.SelectedValue = "N" Then
            Me.BUSCANOTASDECREDITODataGridView.Visible = True
            Me.CMBLabel1.Text = "Buscar Nota por:"
            Me.Label3.Visible = False
            Me.Panel2.Visible = True
            Me.SERIETextBox.Visible = False
            Me.Label4.Text = "Nota :"
            Me.Button8.Text = "&Ver Nota"
            buscaNotas(4)
        Else
            Me.Button8.Text = "&Ver Factura"
            Me.Label3.Visible = True
            Me.SERIETextBox.Visible = True
            Me.Panel2.Visible = False
            Me.Label4.Text = "Folio :"
            Me.CMBLabel1.Text = "Buscar Factura por:"
            If IdSistema = "LO" Then
                Me.Button8.Text = "&Ver Pago"
                Me.CMBLabel1.Text = "Buscar Pago por:"
            End If
            Me.BUSCANOTASDECREDITODataGridView.Visible = False
            Busca(0)
        End If

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(6)
        Else
            Busca(1)
        End If

    End Sub

    Private Sub FECHATextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FECHATextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(5)
            Else
                Busca(2)
            End If

        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(5)
        Else
            Busca(2)
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub SERIETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SERIETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub FOLIOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FOLIOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(6)
            Else
                Busca(1)
            End If
        End If
    End Sub


    Private Sub CONTRATOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CONTRATOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub NOMBRETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NOMBRETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resp As MsgBoxResult
        Dim eRes As Integer = 0
        Dim eMsg As String = Nothing

        If IsNumeric(Me.Clv_FacturaLabel1.Text) = False Then
            If IdSistema = "LO" Then
                MsgBox("Seleccione el Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
            End If
            Exit Sub
        End If
            'Eric Cambio hecho el 6Nov2008
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ValidaCancelacionFacturaTableAdapter.Connection = CON
            Me.ValidaCancelacionFacturaTableAdapter.Fill(Me.EricDataSet2.ValidaCancelacionFactura, CLng(Me.Clv_FacturaLabel1.Text), eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                Exit Sub
            End If
            resp = MsgBox("Deseas Cancelar la " & Me.ComboBox4.Text & " : " & Me.SerieLabel1.Text & "-" & Me.FacturaLabel1.Text, MsgBoxStyle.OkCancel, "Cancelaci�n de Facturas")
            If resp = MsgBoxResult.Ok Then
                eCveFactura = Me.Clv_FacturaLabel1.Text
                'Variable que me permite saber si se cancela o se reimprime factura
                eReImprimirF = 0
                FrmMotivoCancelacionFactura.Show()
            End If

    End Sub


    Private Sub CANCELAFACTURA()
        'Try
        '    Dim MSG As String = nothing
        '    Dim BNDERROR As Integer = 0
        '    Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
        '    FrmMotivoCancelacionFactura.Show()
        '    'MsgBox(MSG)
        '    If BNDERROR = 0 Then
        '        Busca(0)
        '    End If
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then

            eReImprimirF = 1
            eCveFactura = Me.Clv_FacturaLabel1.Text
            FrmMotivoCancelacionFactura.Show()
            'GloClv_Factura = Me.Clv_FacturaLabel1.Text
            'FrmImprimir.Show()
        Else
            If IdSistema = "LO" Then
                MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            If IsNumeric(Me.Label20.Text) = True Then
                GLONOTA = Me.Label20.Text
                LiTipo = 3
                FrmImprimir.Show()
            Else
                MsgBox("Seleccione la Nota ", MsgBoxStyle.Information)

            End If
        Else
            If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then

                GloClv_Factura = Me.Clv_FacturaLabel1.Text
                AuxImprimirTicket = 0
                If IdSistema = "LO" Then
                    LiTipo = 6
                Else
                    LiTipo = 2
                End If
                FrmImprimir.Show()
            Else
                If IdSistema = "LO" Then
                    MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
                Else
                    MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
                End If
            End If
        End If
        
    End Sub

    Private Sub FacturaLabel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub BUSCANOTASDECREDITODataGridView_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BUSCANOTASDECREDITODataGridView.SelectionChanged
        Dim conelidia As New SqlConnection(MiConexion)
        'If Me.Label20.Text = "" Then
        '    Me.Label20.Text = 0
        'End If
        If Me.Label20.Text <> "" Then
            conelidia.Open()
            Me.DetalleNOTASDECREDITOTableAdapter.Connection = conelidia
            Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, Me.Label20.Text)
            conelidia.Close()
        ElseIf IsNumeric(Me.FOLIOTextBox.Text) = True Then
            'conelidia.Open()
            'Me.DetalleNOTASDECREDITOTableAdapter.Connection = conelidia
            'Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, Me.FOLIOTextBox.Text)
            'conelidia.Close()

        End If
    End Sub
    Private Sub BuscaFacturas(ByVal OpBF As Integer, ByVal SerieBF As String, ByVal FacturaBF As Long, ByVal FechaBF As Date, ByVal ContratoBF As Long, ByVal NombreBF As String, ByVal TipoBF As String)

        'Se coment� por que estaba tronando 12 Marzo 2011
        '-----------------------------------------------------------------------------
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim strSQL As New StringBuilder

        'strSQL.Append("EXEC BUSCAFACTURAS_OLDB ")
        'strSQL.Append(CStr(OpBF) & ",'")
        'strSQL.Append(SerieBF & "',")
        'strSQL.Append(CStr(FacturaBF) & ",")
        'strSQL.Append(CStr(FechaBF) & ",")
        'strSQL.Append(CStr(ContratoBF) & ",'")
        'strSQL.Append(NombreBF & "','")
        'strSQL.Append(TipoBF & "'")

        'Dim dataTable As New DataTable
        'Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        'Dim bindingSource As New BindingSource

        'Try
        '    conexion.Open()
        '    dataAdapter.Fill(dataTable)
        '    bindingSource.DataSource = dataTable
        '    DataGridView1.DataSource = bindingSource.DataSource
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        'Finally
        '    conexion.Close()
        '    conexion.Dispose()
        'End Try
        '-----------------------------------------------------------------------------

        Dim conn As New SqlConnection(MiConexion)
        Dim dt As New DataTable

        Try
            conn.Open()

            Dim comando As New SqlClient.SqlCommand("BUSCAFACTURAS_OLDB", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 30

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            If TipoBF = Nothing Then
                TipoBF = ""
            End If

            'P�rametros
            Adaptador.SelectCommand.Parameters.Add("@OP", SqlDbType.Int).Value = OpBF
            Adaptador.SelectCommand.Parameters.Add("@SERIE", SqlDbType.VarChar, 5).Value = SerieBF
            Adaptador.SelectCommand.Parameters.Add("@FACTURA", SqlDbType.Int).Value = FacturaBF
            Adaptador.SelectCommand.Parameters.Add("@FECHA", SqlDbType.DateTime).Value = FechaBF
            Adaptador.SelectCommand.Parameters.Add("@CONTRATO", SqlDbType.Int).Value = ContratoBF
            Adaptador.SelectCommand.Parameters.Add("@NOMBRE", SqlDbType.VarChar, 250).Value = NombreBF
            Adaptador.SelectCommand.Parameters.Add("@TIPO", SqlDbType.VarChar, 1).Value = TipoBF

            Adaptador.Fill(dt)

            'Llenamos el GridV
            DataGridView1.DataSource = dt

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

            conn.Close()
            conn.Dispose()

        End Try

        'Si hay Fcaturas encontradas asignamos la Factura por default (la de la posici�n 1)
        Try
            If DataGridView1.RowCount > 0 Then
                Try
                    'Asignamos los valores

                    'Clave de la Fcatura
                    Clv_FacturaLabel1.Text = DataGridView1.Rows(0).Cells(1).Value.ToString

                    'Nombre
                    CMBNOMBRETextBox1.Text = DataGridView1.Rows(0).Cells(6).Value.ToString
                    'Factura 
                    CMBTextBox1.Text = DataGridView1.Rows(0).Cells(1).Value.ToString & " - " & DataGridView1.Rows(0).Cells(5).Value.ToString
                    'Monto
                    ImporteLabel1.Text = DataGridView1.Rows(0).Cells(7).Value.ToString
                    'Fecha
                    FECHALabel1.Text = DataGridView1.Rows(0).Cells(4).Value.ToString

                    SerieLabel1.Text = DataGridView1.Rows(0).Cells(2).Value.ToString
                    FacturaLabel1.Text = DataGridView1.Rows(0).Cells(3).Value.ToString
                    ClienteLabel1.Text = DataGridView1.Rows(0).Cells(5).Value.ToString
                    Me.Label9.Text = DataGridView1.Rows(0).Cells(0).Value.ToString

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Detalle_Faturas_OLDB()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DETALLE_FACTURAS_OLDB", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = CLng(DataGridView1.SelectedCells.Item(0).Value())
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@SERIE", SqlDbType.VarChar, 5)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@FOLIO", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@FECHA", SqlDbType.DateTime)
        PRM4.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        PRM5.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM5)

        Dim PRM6 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 250)
        PRM6.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@IMPORTE", SqlDbType.Decimal, 10, 2)
        PRM7.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM7)

        Dim PRM8 As New SqlParameter("@STATUS", SqlDbType.VarChar, 20)
        PRM8.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM8)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.SerieLabel1.Text = PRM2.Value
            Me.FacturaLabel1.Text = PRM3.Value
            Me.FECHALabel1.Text = PRM4.Value
            Me.ClienteLabel1.Text = PRM5.Value
            Me.CMBNOMBRETextBox1.Text = PRM6.Value
            Me.ImporteLabel1.Text = PRM7.Value
            Me.Label9.Text = PRM8.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

   
    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        'validamos que haya registros
        If DataGridView1.RowCount > 0 Then
            Try
                'Asignamos los valores

                'Clave de la Fcatura
                Clv_FacturaLabel1.Text = DataGridView1.CurrentRow.Cells(1).Value.ToString

                'Nombre
                CMBNOMBRETextBox1.Text = DataGridView1.CurrentRow.Cells(6).Value.ToString
                'Factura 
                CMBTextBox1.Text = DataGridView1.CurrentRow.Cells(1).Value.ToString & " - " & DataGridView1.CurrentRow.Cells(5).Value.ToString
                'Monto
                ImporteLabel1.Text = DataGridView1.CurrentRow.Cells(7).Value.ToString
                'Fecha
                FECHALabel1.Text = DataGridView1.CurrentRow.Cells(4).Value.ToString

                SerieLabel1.Text = DataGridView1.CurrentRow.Cells(2).Value.ToString
                FacturaLabel1.Text = DataGridView1.CurrentRow.Cells(3).Value.ToString
                ClienteLabel1.Text = DataGridView1.CurrentRow.Cells(5).Value.ToString
                Me.Label9.Text = DataGridView1.CurrentRow.Cells(0).Value.ToString

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub ReportesFacturasXsd(ByVal prmClvFactura As Long, ByVal prmClvFacturaIni As Long, ByVal prmClvFacturaFin As Long, ByVal prmFechaIni As Date, _
                                   ByVal prmFechaFin As Date, ByVal prmOp As Integer, ByVal RUTAREP As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("ReportesFacturasXsd", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Factura", prmClvFactura)
        CMD.Parameters.AddWithValue("@Clv_Factura_Ini", prmClvFacturaIni)
        CMD.Parameters.AddWithValue("@Clv_Factura_Fin", prmClvFacturaFin)
        CMD.Parameters.AddWithValue("@Fecha_Ini", prmFechaIni)
        CMD.Parameters.AddWithValue("@Fecha_Fin", prmFechaFin)
        CMD.Parameters.AddWithValue("@op", prmOp)

        Dim DA As New SqlDataAdapter(CMD)
        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "FacturaFiscal"

        customersByCityReport.Load(RUTAREP)
        customersByCityReport.SetDataSource(DS)
    End Sub
End Class