﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Collections
Public Class DACobroAdeudoBajas

    'Función que obtiene un Cobro Adeudo en base al ID de SesiÓn
    Public Shared Function GetCobroAdeudoByidSesion(ByVal idSesion As Integer) As CobraABajas
        Dim objCobraABajas = New CobraABajas()
        Dim conexion As New SqlConnection(MiConexion)

        Try

            Dim cmd As New SqlCommand("uspConsultaLeyendaCobroAdeudo", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@idSesion", SqlDbType.Int)
            cmd.Parameters("@idSesion").Value = idSesion

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()

            Using dr
                While dr.Read
                    objCobraABajas = New CobraABajas()
                    objCobraABajas.idSesion = dr("idSesion").ToString().Trim()
                    objCobraABajas.MontoPorMes = dr("MontoPorMes").ToString().Trim()
                    objCobraABajas.Proporcional = dr("Proporcional").ToString().Trim()
                    objCobraABajas.MontoPlanForzoso = dr("MontoPlanForzoso").ToString().Trim()
                    objCobraABajas.MontoRentaDeCaja = dr("MontoRentaDeCaja").ToString().Trim()
                    objCobraABajas.MontoTotalDelAdeudo = dr("MontoTotalDelAdeudo").ToString().Trim()
                    objCobraABajas.MesesPlanForzoso = dr("MesesPlanForzoso").ToString().Trim()
                    objCobraABajas.MesesCliente = dr("MesesCliente").ToString().Trim()
                End While
                dr.Close()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        Return objCobraABajas
    End Function

End Class
