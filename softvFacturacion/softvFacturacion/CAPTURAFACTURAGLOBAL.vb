Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class CAPTURAFACTURAGLOBAL
    Dim locerror As Integer
    Private customersByCityReport As ReportDocument
    Dim Mes As Integer
    Dim StrMes As String
    Dim a�o As Integer
    Dim fecha As Date
    Dim glofac As Long = 0
    Dim locSucursal As Long = 1

    'Private Sub Llena_Compa�ias()

    '    Dim oIdCompania As Long = 0
    '    Try
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
    '        BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
    '        ComboBox1.DataSource = BaseII.ConsultaDT("Usp_MUESTRAtblCompanias_Factura_global")
    '        ComboBox1.ValueMember = "clvCompania"
    '        ComboBox1.DisplayMember = "razon_social"
    '        'oIdCompania = Usp_DameIdCompaniaFacturaGlobal(Contrato)
    '        'If oIdCompania > 0 Then
    '        '    ComboBox1.SelectedValue = oIdCompania
    '        'End If
    '    Catch ex As Exception
    '        MsgBox("El campo ID Grupo es Numerico", MsgBoxStyle.Exclamation, "Error")

    '    End Try
    'End Sub

    Private Sub CAPTURAFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bnd = 3 Then
            If glotipoFacGlo = 0 Then
                Me.DateTimePicker2.Visible = False
                Me.Label1.Visible = False
            ElseIf glotipoFacGlo = 1 Then
                Me.DateTimePicker2.Visible = True
            End If
            bnd = 0
        End If
    End Sub


    Private Sub CAPTURAFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        If Gloop = "N" Then
            Me.Button9.Enabled = True
            Me.CMBLblNomCompania.Text = BWRFACTURAGLOBAL.CmbCompamia.Text
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
            Me.DameFechadelServidorHoraTableAdapter.Connection = CON1
            Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.mueastrasuc' Puede moverla o quitarla seg�n sea necesario.
            'Me.MueastrasucTableAdapter.Connection = CON1
            'Me.MueastrasucTableAdapter.Fill(Me.NewsoftvDataSet2.mueastrasuc, bec_tipo)
            CON1.Close()
            Me.SerieTextBox.Enabled = False
            Me.NuevafacturaTextBox.Enabled = False
            ValidaFechas()
            'If Me.ComboBox1.Items.Count > 0 Then
            '    Me.ComboBox1.SelectedIndex = 0
            desglosa_detalle()
            'End If
            'Me.ComboBox1.Text = ""
            'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRASUCURSALES' Puede moverla o quitarla seg�n sea necesario.
            If eAccesoAdmin = True Then
                Me.DateTimePicker1.Enabled = True
                Me.DateTimePicker2.Enabled = True
            Else
                Me.DateTimePicker2.Enabled = False
                Me.DateTimePicker1.Enabled = False
            End If
            Me.DateTimePicker2.Visible = False
            Label1.Visible = False


            'If bnd = 1 Then
            '    FrmTipoNota.Show()
            'End If

        ElseIf Gloop = "C" Then
            Me.GroupBox2.Enabled = False
            Me.Button9.Enabled = False
            Me.Panel1.Enabled = False
            Me.GroupBox1.Enabled = False
            'Me.ComboBox1.Text = Locclv_sucursalglo
            Me.CMBLblNomCompania.Text = BWRFACTURAGLOBAL.CmbCompamia.Text
            Me.SerieTextBox.Text = LocSerieglo
            Me.NuevafacturaTextBox.Text = LocFacturaGlo
            'Me.DateTimePicker2.Text = LocFechaGloFinal
            Me.DateTimePicker1.Text = LocFechaGlo
            Me.TextBox4.Text = FormatCurrency(LocImporteGlo, 2, TriState.True)
            Me.TextBox5.Text = LocCajeroGlo

        End If
    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String, ByVal ieps As Double)
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim txtsubtotal As String = Nothing
            Dim subtotal2 As Double
            Dim iva2 As Double
            Dim myString As String = iva2.ToString("00.00")
            Dim reportPath As String = Nothing

            Select Case Locclv_empresa
                Case "AG"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
                Case "TO"
                    reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
                Case "SA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
                Case "VA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            End Select
            customersByCityReport.Load(reportPath)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then
                'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                'subtotal2 = CDec(importe2) / 1.15
                'txtsubtotal = subtotal2
                'txtsubtotal = subtotal2.ToString("##0.00")
                'iva2 = CDec(importe2) / 1.15 * 0.15
                'myString = iva2.ToString("##0.00")
                'ElseIf Locclv_empresa = "AG" Then
                '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                '    subtotal2 = CDec(importe2) / 1.15
                '    txtsubtotal = subtotal2
                '    txtsubtotal = subtotal2.ToString("##0.00")
                '    iva2 = CDec(importe2) / 1.15 * 0.15
                '    myString = iva2.ToString("##0.00")
            End If

            ieps = 0
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Factura", SqlDbType.Int, bec_factura)
                Dim tblImpuestos As DataTable = BaseII.ConsultaDT("uspTraeImpuestosFacGlobal")

                For Each Fila As DataRow In tblImpuestos.Rows
                    ieps = CDbl(Fila("IEPS"))
                    myString = Fila("IVA")
                    txtsubtotal = Fila("SubTotal")
                    importe2 = Fila("Total")
                Next
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try

            customersByCityReport.DataDefinition.FormulaFields("IEPS").Text = "'" & ieps.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
            customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"




            Select Case Locclv_empresa
                Case "AG"
                    'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "TO"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "SA"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "VA"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            End Select


            'customersByCityReport.PrintToPrinter(1, True, 1, 1)

            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub oldConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword




        Dim cliente2 As String = "P�blico en General"
        Dim concepto2 As String = "Ingreso por Pago de Servicios"
        Dim txtsubtotal As String = Nothing
        Dim subtotal2 As Double
        Dim iva2 As Double
        Dim myString As String = iva2.ToString("00.00")
        Dim reportPath As String = Nothing
        If Locclv_empresa <> "TO" Then
            reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
        Else
            reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
        End If
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
        subtotal2 = CDec(importe2) / 1.15
        txtsubtotal = subtotal2
        txtsubtotal = subtotal2.ToString("##0.00")
        iva2 = CDec(importe2) / 1.15 * 0.15
        myString = iva2.ToString("##0.00")
        customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
        customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

        If Locclv_empresa = "SA" Then
            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        ElseIf Locclv_empresa <> "TO" Then
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        Else
            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        End If

        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing


    End Sub

   

    Private Sub Guardar()
        Try

            Dim comando As New SqlClient.SqlCommand
            Dim Comando2 As New SqlClient.SqlCommand
            Dim CON3 As New SqlConnection(MiConexion)
            'Me.NUEVAFACTGLOBALTableAdapter.Connection = CON3
            'Me.NUEVAFACTGLOBALTableAdapter.Fill(Me.DataSetLydia.NUEVAFACTGLOBAL, glofac, bec_tipo, Me.SerieTextBox.Text, Me.NuevafacturaTextBox.Text, Me.DateTimePicker2.Text, Me.TextBox5.Text, CDec(Me.TextBox4.Text), 0, CStr(Me.ComboBox1.SelectedValue), CDec(Me.Label6.Tag), bec_consecutivo)
            glofac = ups_NUEVAFACTGLOBALporcompania(GloFacGlobalclvcompania, DateTimePicker1.Value.ToShortDateString, GloUsuario, locSucursal)

            If glofac = 0 Then
                MsgBox("No Se Grabo la Factura Global ", vbInformation)
                Exit Sub
            End If
            bitsist(GloUsuario, 0, GloSistema, "Nueva Factura Global", "Sucursal En Donde Se Gener�: " + CStr(GloSucursal) + ", Nombre de la Caja: " + CStr(GlonOMCaja), "Se Gener� la Factura Global del cajero: " + Me.TextBox5.Text, "Del Dia:" + Me.DateTimePicker1.Text + " Por la Cantidad de: " + Me.TextBox4.Text, LocNomciudad)


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Me.TextBox4.Text), 0, bec_letra)
            miLetra2 = bec_letra
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ValidaFechas()
        Mes = CInt(Month(Me.DateTimePicker1.Text))
        a�o = CInt(Year(Me.DateTimePicker1.Text))
        fecha = DateSerial(a�o, Mes + 1, 0)
        StrMes = CStr(Mes)
        StrMes = StrMes.PadLeft(2, "0")
        'Me.DateTimePicker1.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        If StrMes = "01" Then
            Me.DateTimePicker1.MinDate = CDate("01/01/" + CStr(a�o - 1))
        Else
            Me.DateTimePicker1.MinDate = CDate("01/01/" + CStr(a�o))
        End If

        Me.DateTimePicker1.MaxDate = CDate(fecha)
        'Me.DateTimePicker2.MinDate = CDate("01/" + StrMes + "/" + CStr(a�o))
        If StrMes = "01" Then
            Me.DateTimePicker2.MinDate = CDate("01/01/" + CStr(a�o - 1))
        Else
            Me.DateTimePicker2.MinDate = CDate("01/01/" + CStr(a�o))
        End If
        Me.DateTimePicker2.MaxDate = CDate(fecha)
        Me.Label1.Visible = True
    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
        Dim num As Integer
        If Gloop = "N" Then
            If (glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) <= CDate(Me.DateTimePicker2.Text))) Or glotipoFacGlo = 0 Then

                If Me.TextBox4.Text <> "" Then
                    If CInt(Me.TextBox4.Text) <> 0 Then

                        If locerror = 0 Then
                            bec_bandera = 1
                            conlidia2.Open()
                            Dim comando2 As New SqlClient.SqlCommand
                            With comando2
                                .Connection = conlidia2
                                .CommandText = "Dime_FacGlo_Ex "
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                ' Create a SqlParameter for each parameter in the stored procedure.
                                Dim prm As New SqlParameter("@CLVCOMPANIA", SqlDbType.Int)
                                Dim prm1 As New SqlParameter("@fecha", SqlDbType.DateTime)
                                Dim prm2 As New SqlParameter("@Fecha_2", SqlDbType.DateTime)
                                Dim prm4 As New SqlParameter("@Cont", SqlDbType.Int)
                                Dim prm5 As New SqlParameter("@Sucursal", SqlDbType.Int)

                                prm.Direction = ParameterDirection.Input
                                prm1.Direction = ParameterDirection.Input
                                prm2.Direction = ParameterDirection.Input
                                prm4.Direction = ParameterDirection.Output
                                prm5.Direction = ParameterDirection.Input

                                prm.Value = GloFacGlobalclvcompania
                                prm1.Value = Me.DateTimePicker1.Text
                                prm2.Value = Me.DateTimePicker1.Text
                                prm4.Value = 0
                                prm5.Value = locSucursal
                                .Parameters.Add(prm)
                                .Parameters.Add(prm1)
                                .Parameters.Add(prm2)
                                .Parameters.Add(prm4)
                                .Parameters.Add(prm5)

                                Dim i As Integer = comando2.ExecuteNonQuery()
                                num = prm4.Value

                            End With
                            conlidia2.Close()

                            

                            Dim ieps As Double = 0
                            If num = 0 Then
                                Guardar()
                                'bec_serie = Me.SerieTextBox.Text
                                'bec_factura = Me.NuevafacturaTextBox.Text
                                'bec_fecha = Me.DateTimePicker1.Text
                                'LocFechaGlo = bec_fecha
                                'bec_importe = Me.TextBox4.Text
                                'CantidadaLetra()
                                'BaseII.limpiaParametros()
                                'BaseII.CreateMyParameter("@tipo", SqlDbType.NVarChar, bec_tipo, 1)
                                ''BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, Me.ComboBox1.SelectedValue)
                                'BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, CDate(Me.DateTimePicker1.Text))
                                'BaseII.Inserta("uspCalculaImpuestosFacGlobal")

                                'BaseII.limpiaParametros()
                                'BaseII.CreateMyParameter("@Factura", SqlDbType.Int, bec_factura)
                                'Dim tblImpuestos As DataTable = BaseII.ConsultaDT("uspTraeImpuestosFacGlobal")
                                'For Each Fila As DataRow In tblImpuestos.Rows
                                '    ieps = CDbl(Fila("IEPS"))
                                'Next


                            ElseIf num > 0 And glotipoFacGlo = 0 Then
                                MsgBox("La factura Global del dia " + Me.DateTimePicker1.Text + " ya ha sido Generada ", MsgBoxStyle.Information)
                                Exit Sub
                            ElseIf num > 0 And glotipoFacGlo = 1 Then
                                MsgBox("Ya Existe una Factura Global en el Rango del : " + Me.DateTimePicker1.Text + " Al D�a " + Me.DateTimePicker2.Text, MsgBoxStyle.Information)
                                Exit Sub
                            ElseIf num = -3 Then
                                MsgBox("La Factura Global se Genera Cada Mes Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                            ElseIf num = -4 Then
                                MsgBox("La Factura Global se Genera Diario Autom�ticamente, por lo que no es Posible Generarla Manual", MsgBoxStyle.Information)
                            End If

                            Locop = 1
                            Dime_Aque_Compania_FacturarleGlobal(glofac)
                            Graba_Factura_Digital_Global(glofac, CStr(locID_Compania_Mizart), CStr(locID_Sucursal_Mizart)) ', MiConexion

                            Try
                                FormPruebaDigital.Show()
                            Catch ex As Exception

                            End Try

                            Dim r As New Globalization.CultureInfo("es-MX")
                            r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
                            System.Threading.Thread.CurrentThread.CurrentCulture = r

                            'If LocImpresoraTickets = "" Then
                            '    MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                            'ElseIf LocImpresoraTickets <> "" Then
                            '    LiTipo = 7
                            '    FrmImprimir.Show()
                            '    'Me.ConfigureCrystalReportefacturaGlobal(bec_letra, bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura, ieps)
                            'End If
                            'GloReporte = 5
                            'My.Forms.FrmImprimirRepGral.Show()
                            Me.Close()
                            'End If
                        Else
                            MsgBox("La Sucursal no tiene Asignada una Serie Para sus Facturas Globales", MsgBoxStyle.Information)
                        End If
                    Else
                        MsgBox("El Importe debe ser Mayor que Cero", MsgBoxStyle.Information)
                    End If
                Else
                    MsgBox("Seleccione una Sucursal", MsgBoxStyle.Information)
                End If
            ElseIf glotipoFacGlo = 1 And (CDate(Me.DateTimePicker1.Text) > CDate(Me.DateTimePicker2.Text)) Then
                MsgBox("La Fecha de Inicio debe ser Mayor a la Fecha Final", MsgBoxStyle.Information)
            End If
            ElseIf Gloop = "C" Then
                Me.Close()
            End If

    End Sub
   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    'Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Me.ComboBox1.SelectedIndex = -1 Then
    '        Me.SerieTextBox.Clear()
    '        Me.NuevafacturaTextBox.Clear()
    '        Me.TextBox4.Clear()
    '        Me.TextBox5.Clear()
    '    End If
    'End Sub

    Private Sub desglosa_detalle()
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        ''Me.DameImporteSucursalTableAdapter.Connection = CON
        ''Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
        ''Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON
        ''Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
        'CON.Close()
        TextBox4.Text = FormatCurrency(USP_DameImportePorCompania(DateTimePicker1.Value.ToShortDateString, GloFacGlobalclvcompania, locSucursal), 2, TriState.True)
        Me.TextBox5.Text = GloUsuario
    End Sub

   



    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON13 As New SqlConnection(MiConexion)
        If Gloop = "N" Then
            'CON13.Open()
            'Me.DameImporteSucursalTableAdapter.Connection = CON13
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON13
            'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            'CON13.Close()
            desglosa_detalle()
            Me.TextBox5.Text = GloUsuario
            'ElseIf glotipoFacGlo = 1 Then
            'ValidaFechas()
        End If
    End Sub


   
    Private Sub RadioButton2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then locSucursal = 1 Else locSucursal = 2
        If Gloop = "N" Then
            'CON13.Open()
            'Me.DameImporteSucursalTableAdapter.Connection = CON13
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON13
            'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            'CON13.Close()
            desglosa_detalle()
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then locSucursal = 2 Else locSucursal = 1
        If Gloop = "N" Then
            'CON13.Open()
            'Me.DameImporteSucursalTableAdapter.Connection = CON13
            'Me.DameImporteSucursalTableAdapter.Fill(Me.DataSetLydia.DameImporteSucursal, Me.DateTimePicker1.Text, Me.DateTimePicker2.Text, CInt(Me.ComboBox1.SelectedValue), bec_tipo, glotipoFacGlo)
            'Me.Llena_Factura_Global_nuevoTableAdapter.Connection = CON13
            'Me.Llena_Factura_Global_nuevoTableAdapter.Fill(Me.Procedimientos_arnoldo.Llena_Factura_Global_nuevo, CInt(Me.ComboBox1.SelectedValue), locerror)
            'CON13.Close()
            desglosa_detalle()
        End If
    End Sub
End Class