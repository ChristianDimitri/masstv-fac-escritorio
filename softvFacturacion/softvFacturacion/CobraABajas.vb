﻿Public Class CobraABajas

#Region "Atributos"

    Dim _idSesion As Integer
    Dim _MontoPorMes As Double
    Dim _Proporcional As Double
    Dim _MontoPlanForzoso As Double
    Dim _MontoRentaDeCaja As Double
    Dim _MontoTotalDelAdeudo As Double
    Dim _MesesPlanForzoso As Integer
    Dim _MesesCliente As Integer

#End Region
#Region "Propiedades"
    Public Property idSesion() As Integer
        Get
            Return _idSesion
        End Get
        Set(ByVal Value As Integer)
            _idSesion = Value
        End Set
    End Property
    Public Property MontoPorMes() As Double
        Get
            Return _MontoPorMes
        End Get
        Set(ByVal Value As Double)
            _MontoPorMes = Value
        End Set
    End Property
    Public Property Proporcional() As Double
        Get
            Return _Proporcional
        End Get
        Set(ByVal Value As Double)
            _Proporcional = Value
        End Set
    End Property
    Public Property MontoPlanForzoso() As Double
        Get
            Return _MontoPlanForzoso
        End Get
        Set(ByVal Value As Double)
            _MontoPlanForzoso = Value
        End Set
    End Property
    Public Property MontoRentaDeCaja() As Double
        Get
            Return _MontoRentaDeCaja
        End Get
        Set(ByVal Value As Double)
            _MontoRentaDeCaja = Value
        End Set
    End Property
    Public Property MontoTotalDelAdeudo() As Double
        Get
            Return _MontoTotalDelAdeudo
        End Get
        Set(ByVal Value As Double)
            _MontoTotalDelAdeudo = Value
        End Set
    End Property
    Public Property MesesPlanForzoso() As Integer
        Get
            Return _MesesPlanForzoso
        End Get
        Set(ByVal Value As Integer)
            _MesesPlanForzoso = Value
        End Set
    End Property
    Public Property MesesCliente() As Integer
        Get
            Return _MesesCliente
        End Get
        Set(ByVal Value As Integer)
            _MesesCliente = Value
        End Set
    End Property
#End Region
#Region "Funciones Estáticas"

#End Region

End Class
