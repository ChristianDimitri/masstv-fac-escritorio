﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Collections

Public Class FrmSelAparatosDeEntrega

    'Globales
    Public diccionario As Dictionary(Of String, AparatosCliente) = New Dictionary(Of String, AparatosCliente)
    Dim listaI As New List(Of AparatosCliente)
    Dim listaD As New List(Of AparatosCliente)
    Dim listaAuxiliar As New List(Of AparatosCliente)
    Dim listaOriginal As New List(Of AparatosCliente)
    Dim Aparatos As AparatosCliente = Nothing
    Dim arreglo As New ArrayList
    Dim strXML As String = ""
    Public Shared Clv_Detalle As Integer = 0
    Private CajasABajaDeLaAntena As Integer = 0
    Private CajasActivasDeLaAntena As Integer = 0

    Private Sub MUESTRATecnicosActivos()
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC MuestraTecnicosActivos")

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            cbTecnico.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

    End Sub

#Region "Métodos de Interfaz de Usuario"

    'LOAD
    Private Sub FrmSelAparatosDeEntrega_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'GUI
        colorea(Me)
        Me.lblTitulo.ForeColor = Color.Black
        Me.lblMsj01.ForeColor = Color.Black
        Me.lblMsj02.ForeColor = Color.Black
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black

        'Enlazamos los Datos
        CargarAparatosClientes()
        MUESTRATecnicosActivos()

    End Sub

    'BOTÓN SALIR
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub

    'ENVIAR DERECHA
    Private Sub enviarDerecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enviarDerecha.Click

       
        If Me.ListBox1.SelectedValue <> Nothing Then

            'Validamos la devolución de la antena
            Dim Aparato As AparatosCliente
            Aparato = diccionario(ListBox1.SelectedValue.ToString())

            If Not habilitaDevolucion(Aparato) Then
                MsgBox("Antes de que se regrese esta antena " & Aparato.descripcionCompuesta & " se tienen que dar de baja todos sus aparatos.", MsgBoxStyle.Information)
                Exit Sub
            End If

            listaD.Add(diccionario(ListBox1.SelectedValue.ToString()))
            listaI.Remove(diccionario(ListBox1.SelectedValue.ToString()))
            refreshLists()

            'Solo aplica para decodificadores/cajas
            If Aparato.nodo = 1 Then
                CajasABajaDeLaAntena += 1
            End If

        End If

    End Sub

    'ENVIAR TODOS DERECHA
    Private Sub enviarTodosDerecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enviarTodosDerecha.Click
        If ListBox1.Items.Count > 0 Then
            For Each Aparatos In listaI
                listaD.Add(Aparatos)
            Next
            listaI.Clear()
            refreshLists()
        End If
    End Sub

    'ENVIAR IZQUIERDA
    Private Sub enviarIzquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enviarIzquierda.Click
        If Me.ListBox2.SelectedValue <> Nothing Then


            'Validamos la devolución de la antena
            Dim Aparato As AparatosCliente
            Aparato = diccionario(ListBox2.SelectedValue.ToString())

            'Solo aplica para decodificadores/cajas
            If Aparato.nodo = 1 Then
                CajasABajaDeLaAntena -= 1
            End If

            listaI.Add(diccionario(ListBox2.SelectedValue.ToString()))
            listaD.Remove(diccionario(ListBox2.SelectedValue.ToString()))

            arreglo.Clear()

            For Each Aparatos In listaI
                arreglo.Add(Aparatos.ident - 1)
                listaAuxiliar.Add(Aparatos)
            Next

            listaI.Clear()

            'Barremos los elementos del arreglo para regresarlos a la lista Izquierda ya Ordenados 
            Dim i As Integer
            Dim j As Integer
            j = 0

            For i = 1 To arreglo.Count
                Me.Minimo()
            Next

            listaAuxiliar.Clear()
            refreshLists()



        End If
    End Sub

    'ENVIAR TODOS IZQUIERDA
    Private Sub enviarTodosIzquierda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enviarTodosIzquierda.Click
        If ListBox2.Items.Count > 0 Then
            listaI.Clear()
            For Each Aparatos In listaOriginal
                listaI.Add(Aparatos)
            Next
            listaD.Clear()
            refreshLists()
        End If
    End Sub

    'BOTÓN ACEPTAR
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        'Verificamos que haya elementos
        If ListBox2.Items.Count > 0 Then

            'Generamos el XML
            strXML = generaXML()

            'Insertamos los aparatos que regresó el Cliente en la Tabla temporal
            AgregaAparatosDevolucionCliente(strXML)

            Me.Close()
            Me.Dispose()

        Else
            Me.Close()
            Me.Dispose()
            'MsgBox("No has seleccionado ningun aparato.", MsgBoxStyle.Information, "Debes seleccionar por lo menos un aparato.")
        End If
    End Sub

    'REFRESCAR LISTAS
    Public Sub refreshLists()

        ListBox2.DataSource = Nothing
        ListBox2.DataSource = listaD
        ListBox2.DisplayMember = "descripcionCompuesta"
        ListBox2.ValueMember = "ident"

        ListBox1.DataSource = Nothing
        ListBox1.DataSource = listaI
        ListBox1.DisplayMember = "descripcionCompuesta"
        ListBox1.ValueMember = "ident"
        ListBox1.Refresh()


    End Sub

    'OBTENER EL ITEM MÍNIMO 
    Public Sub Minimo()

        Dim num_minimo As Integer
        'Dim compara As Integer
        Dim i As Integer
        i = 0
        num_minimo = -1

        For Each Aparatos In listaAuxiliar

            If num_minimo = -1 Then
                num_minimo = Aparatos.ident
            End If

            If num_minimo > Aparatos.ident Then
                num_minimo = Aparatos.ident
            End If

            If listaAuxiliar.Count = 1 Then
                num_minimo = Aparatos.ident
            End If
        Next

        Dim IndexList As Integer
        'Veces = listaAuxiliar.Count - 1

        For Each Aparatos In listaAuxiliar

            If num_minimo = Aparatos.ident Then
                listaI.Add(Aparatos)
                IndexList = i
            End If
            i = i + 1
        Next

        listaAuxiliar.RemoveAt(IndexList)

    End Sub

    'Convierte los Items que estén en la lista Derecha a XML
    Private Function generaXML() As String

        strXML = ""

        Try

            Dim sw As New StringWriter()
            Dim w As New XmlTextWriter(sw)
            Dim i As Integer
            Dim AparatoXML As AparatosCliente

            w.Formatting = Formatting.Indented

            w.WriteStartElement("AparatosCliente")

            For Each AparatoXML In listaD

                w.WriteStartElement("AparatosCliente")

                w.WriteAttributeString("ident", AparatoXML.ident)
                w.WriteAttributeString("contratonet", AparatoXML.contratonet)
                w.WriteAttributeString("clv_Aparato", AparatoXML.clv_Aparato)
                w.WriteAttributeString("nombre", AparatoXML.nombre)
                w.WriteAttributeString("descripcion", AparatoXML.descripcion)
                w.WriteAttributeString("status", AparatoXML.status)
                w.WriteAttributeString("nodo", AparatoXML.nodo)

                w.WriteEndElement()

            Next

            w.WriteEndElement()
            w.Close()
            strXML = sw.ToString()

        Catch ex As Exception
            MsgBox(ex.Message)
            strXML = ""
        End Try

        Return strXML

    End Function

    'Valida si puede devolver la antena 
    'Solo si no ya no tiene ningún aparato activo o se están cancelando todas las cajas sí se permite devolver la Antena, de lo contrario se bloquea la opción.
    Private Function habilitaDevolucion(ByRef Aparato As AparatosCliente) As Boolean
        Dim _habilitaDevolucion As Boolean = True

        'Checamos si es antena
        If Aparato.nodo = 0 Then
            'Si a la Antena aún le quedan aparatos activos que cuelgan de ella no se permitirá que la regrese el cliente sin antes regresar los demás aparatos antes.
            If CajasABajaDeLaAntena < CajasActivasDeLaAntena Then
                _habilitaDevolucion = False
            End If
        End If

        Return _habilitaDevolucion
    End Function

#End Region

#Region "SQL"

    'Carga los Aparatos del Usuario en base al servicio que se estpe cancelando
    Public Sub CargarAparatosClientes()

        Dim I As Integer = 0
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        Try

            Dim cmd As New SqlCommand("CargaAparatosClienteCobroAdeudo", conexion)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Contrato", SqlDbType.Int)
            cmd.Parameters("@Contrato").Value = GloContrato

            cmd.Parameters.Add("@Clv_Detalle", SqlDbType.Int)
            cmd.Parameters("@Clv_Detalle").Value = Clv_Detalle

            cmd.Parameters.Add("@Clv_Sesion", SqlDbType.BigInt)
            cmd.Parameters("@Clv_Sesion").Direction = ParameterDirection.Input
            cmd.Parameters("@Clv_Sesion").Value = gloClv_Session

            cmd.Parameters.Add("@CajasABajaDeLaAntena", SqlDbType.TinyInt)
            cmd.Parameters("@CajasABajaDeLaAntena").Direction = ParameterDirection.Output
            cmd.Parameters("@CajasABajaDeLaAntena").Value = 0

            cmd.Parameters.Add("@CajasActivasDeLaAntena", SqlDbType.TinyInt)
            cmd.Parameters("@CajasActivasDeLaAntena").Direction = ParameterDirection.Output
            cmd.Parameters("@CajasActivasDeLaAntena").Value = 0

            'Declaramos la lista de la clase 
            Dim listaCargos As New List(Of AparatosCliente)

            Dim dr As SqlDataReader
            conexion.Open()
            dr = cmd.ExecuteReader()
            listaI.Clear()

            Using dr
                While dr.Read
                    If diccionario.ContainsKey(dr("ident").ToString().Trim()) Then
                        If Not listaD.Contains(diccionario(dr("ident").ToString().Trim())) Then
                            listaI.Add(diccionario(dr("ident").ToString().Trim()))
                        End If

                    Else

                        Aparatos = New AparatosCliente()
                        Aparatos.ident = dr("ident").ToString().Trim()
                        Aparatos.contratonet = dr("contratonet").ToString().Trim()
                        Aparatos.clv_Aparato = dr("clv_Aparato").ToString().Trim()
                        Aparatos.nombre = dr("nombre").ToString().Trim()
                        Aparatos.descripcion = dr("descripcion").ToString().Trim()
                        Aparatos.status = dr("status").ToString().Trim()
                        Aparatos.nodo = dr("nodo").ToString().Trim()
                        Aparatos.habilitado = dr("habilitado")

                        diccionario.Add(Aparatos.ident, Aparatos)
                        listaI.Add(Aparatos)

                    End If
                End While

                dr.Close()

                'Obtenemos el total de Decos que cuelgan de la Antena
                CajasABajaDeLaAntena = cmd.Parameters("@CajasABajaDeLaAntena").Value
                CajasActivasDeLaAntena = cmd.Parameters("@CajasActivasDeLaAntena").Value

            End Using

            Me.ListBox1.DataSource = Nothing
            Me.ListBox1.DataSource = listaI
            Me.ListBox1.DisplayMember = "descripcionCompuesta"
            Me.ListBox1.ValueMember = "ident"

            For Each Aparatos In listaI
                listaOriginal.Add(Aparatos)
            Next

        Catch ex As Exception

            MsgBox("Ocurrió un error.", MsgBoxStyle.Critical)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Exit Sub

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    'Insertamos los aparatos que regresó el Cliente en la Tabla temporal
    Private Sub AgregaAparatosDevolucionCliente(ByRef strXML As String)

        Dim conn As New SqlConnection(MiConexion)
        Dim Dataset As New DataSet

        Try

            Dim comando As New SqlClient.SqlCommand("AgregaAparatosDevolucionCliente", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 30

            comando.Parameters.Add("@Clv_Sesion", SqlDbType.BigInt)
            comando.Parameters("@Clv_Sesion").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Sesion").Value = gloClv_Session

            comando.Parameters.Add("@XML", SqlDbType.Xml)
            comando.Parameters("@XML").Direction = ParameterDirection.Input
            comando.Parameters("@XML").Value = strXML

            comando.Parameters.Add("@UsuarioDevolucion", SqlDbType.VarChar, 5)
            comando.Parameters("@UsuarioDevolucion").Direction = ParameterDirection.Input
            comando.Parameters("@UsuarioDevolucion").Value = GloUsuario

            comando.Parameters.Add("@clv_Tecnico", SqlDbType.Int)
            comando.Parameters("@clv_Tecnico").Direction = ParameterDirection.Input
            comando.Parameters("@clv_Tecnico").Value = cbTecnico.SelectedValue


            conn.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

    End Sub

    'Verifica si es que el Serivicio que se está seleccionando es Principal.
    Public Function ConsultaTipoPaquetePorClv_Detalle(ByRef Clv_Detalle As Integer) As Boolean

        Dim conn As New SqlConnection(MiConexion)
        Dim EsPrinicpal = False

        Try

            Dim comando As New SqlClient.SqlCommand("uspConsultaTipoPaquetePorClv_Detalle", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 20

            comando.Parameters.Add("@Clv_Detalle", SqlDbType.BigInt)
            comando.Parameters("@Clv_Detalle").Direction = ParameterDirection.Input
            comando.Parameters("@Clv_Detalle").Value = Clv_Detalle

            comando.Parameters.Add("@EsPrinicpal", SqlDbType.Bit)
            comando.Parameters("@EsPrinicpal").Direction = ParameterDirection.Output
            comando.Parameters("@EsPrinicpal").Value = 0

            conn.Open()
            comando.ExecuteNonQuery()

            EsPrinicpal = comando.Parameters("@EsPrinicpal").Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conn.Close()
            conn.Dispose()
        End Try

        Return EsPrinicpal
    End Function

#End Region

End Class

'CLASE QUE CONTIENE LOS APARATOS
Public Class AparatosCliente
    Dim _ident As Integer
    Dim _contratonet As Integer
    Dim _clv_Aparato As Integer
    Dim _nombre As String
    Dim _descripcion As String
    Dim _status As String
    Dim _nodo As Integer
    Dim _habilitado As Boolean

    Public Property ident() As Integer
        Get
            Return _ident
        End Get
        Set(ByVal Value As Integer)
            _ident = Value
        End Set
    End Property
    Public Property contratonet() As Integer
        Get
            Return _contratonet
        End Get
        Set(ByVal Value As Integer)
            _contratonet = Value
        End Set
    End Property
    Public Property clv_Aparato() As Integer
        Get
            Return _clv_Aparato
        End Get
        Set(ByVal Value As Integer)
            _clv_Aparato = Value
        End Set
    End Property
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal Value As String)
            _nombre = Value
        End Set
    End Property

    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property
    Public Property descripcionCompuesta() As String
        Get
            Return _nombre & " - [ " & _descripcion & " ]"
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property
    Public Property nodo() As Integer
        Get
            Return _nodo
        End Get
        Set(ByVal Value As Integer)
            _nodo = Value
        End Set
    End Property
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal Value As String)
            _status = Value
        End Set
    End Property
    Public Property habilitado() As Boolean
        Get
            Return _habilitado
        End Get
        Set(ByVal Value As Boolean)
            _habilitado = Value
        End Set
    End Property

    
End Class
