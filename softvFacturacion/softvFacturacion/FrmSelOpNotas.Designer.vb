<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelOpNotas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ComboVendedores = New System.Windows.Forms.ComboBox
        Me.MuestraUsuarioNotasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.MuestrasucursalesnotasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo2 = New softvFacturacion.ProcedimientosArnoldo2
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Muestra_Usuario_NotasTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Muestra_Usuario_NotasTableAdapter
        Me.Muestra_sucursales_notasTableAdapter = New softvFacturacion.ProcedimientosArnoldo2TableAdapters.Muestra_sucursales_notasTableAdapter
        CType(Me.MuestraUsuarioNotasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.MuestrasucursalesnotasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ComboVendedores
        '
        Me.ComboVendedores.DataSource = Me.MuestraUsuarioNotasBindingSource
        Me.ComboVendedores.DisplayMember = "Nombre"
        Me.ComboVendedores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboVendedores.ForeColor = System.Drawing.Color.Black
        Me.ComboVendedores.FormattingEnabled = True
        Me.ComboVendedores.Location = New System.Drawing.Point(230, 70)
        Me.ComboVendedores.Name = "ComboVendedores"
        Me.ComboVendedores.Size = New System.Drawing.Size(189, 23)
        Me.ComboVendedores.TabIndex = 14
        Me.ComboVendedores.ValueMember = "clave"
        Me.ComboVendedores.Visible = False
        '
        'MuestraUsuarioNotasBindingSource
        '
        Me.MuestraUsuarioNotasBindingSource.DataMember = "Muestra_Usuario_Notas"
        Me.MuestraUsuarioNotasBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel3.Location = New System.Drawing.Point(227, 51)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(85, 16)
        Me.CMBLabel3.TabIndex = 18
        Me.CMBLabel3.Text = "Usuario(a):"
        Me.CMBLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(16, 127)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(163, 21)
        Me.DateTimePicker2.TabIndex = 13
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(16, 58)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(163, 21)
        Me.DateTimePicker1.TabIndex = 12
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel2.Location = New System.Drawing.Point(13, 99)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(179, 16)
        Me.CMBLabel2.TabIndex = 17
        Me.CMBLabel2.Text = "Fecha Final del Reporte:"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 32)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(186, 16)
        Me.CMBLabel1.TabIndex = 16
        Me.CMBLabel1.Text = "Fecha Inicial del Reporte:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.ComboVendedores)
        Me.GroupBox1.Controls.Add(Me.CMBLabel1)
        Me.GroupBox1.Controls.Add(Me.CMBLabel2)
        Me.GroupBox1.Controls.Add(Me.CMBLabel3)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(29, 165)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(536, 196)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Adicionales"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MuestrasucursalesnotasBindingSource
        Me.ComboBox1.DisplayMember = "descripcion"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.ForeColor = System.Drawing.Color.Black
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(230, 70)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(189, 23)
        Me.ComboBox1.TabIndex = 19
        Me.ComboBox1.ValueMember = "clv_sucursal"
        Me.ComboBox1.Visible = False
        '
        'MuestrasucursalesnotasBindingSource
        '
        Me.MuestrasucursalesnotasBindingSource.DataMember = "Muestra_sucursales_notas"
        Me.MuestrasucursalesnotasBindingSource.DataSource = Me.ProcedimientosArnoldo2
        '
        'ProcedimientosArnoldo2
        '
        Me.ProcedimientosArnoldo2.DataSetName = "ProcedimientosArnoldo2"
        Me.ProcedimientosArnoldo2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox3)
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(29, 23)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(536, 96)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Seleccione el Status de la Nota de Crédito"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(333, 35)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(86, 20)
        Me.CheckBox3.TabIndex = 2
        Me.CheckBox3.Text = "Saldada"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(198, 70)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(102, 20)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Cancelada"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(85, 35)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(70, 20)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Activa"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(427, 376)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 22
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(248, 376)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Muestra_Usuario_NotasTableAdapter
        '
        Me.Muestra_Usuario_NotasTableAdapter.ClearBeforeFill = True
        '
        'Muestra_sucursales_notasTableAdapter
        '
        Me.Muestra_sucursales_notasTableAdapter.ClearBeforeFill = True
        '
        'FrmSelOpNotas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(610, 439)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmSelOpNotas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones de Busqueda de las Notas de Crédito"
        CType(Me.MuestraUsuarioNotasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.MuestrasucursalesnotasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ComboVendedores As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents MuestraUsuarioNotasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Usuario_NotasTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Muestra_Usuario_NotasTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ProcedimientosArnoldo2 As softvFacturacion.ProcedimientosArnoldo2
    Friend WithEvents MuestrasucursalesnotasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_sucursales_notasTableAdapter As softvFacturacion.ProcedimientosArnoldo2TableAdapters.Muestra_sucursales_notasTableAdapter
End Class
