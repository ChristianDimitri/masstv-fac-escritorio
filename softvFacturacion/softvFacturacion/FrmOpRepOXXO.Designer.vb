﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOpRepOXXO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblSeleccion = New System.Windows.Forms.Label()
        Me.pnlRepOXXO = New System.Windows.Forms.Panel()
        Me.cbxPendiente = New System.Windows.Forms.CheckBox()
        Me.cbxCorrecto = New System.Windows.Forms.CheckBox()
        Me.cbxIncorrecto = New System.Windows.Forms.CheckBox()
        Me.cbxCancelado = New System.Windows.Forms.CheckBox()
        Me.pnlRepOXXO.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(37, 114)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(119, 34)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(406, 114)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(119, 34)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblSeleccion
        '
        Me.lblSeleccion.AutoSize = True
        Me.lblSeleccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeleccion.ForeColor = System.Drawing.Color.Black
        Me.lblSeleccion.Location = New System.Drawing.Point(12, 15)
        Me.lblSeleccion.Name = "lblSeleccion"
        Me.lblSeleccion.Size = New System.Drawing.Size(272, 18)
        Me.lblSeleccion.TabIndex = 179
        Me.lblSeleccion.Text = "Seleccione el estado del resultado:"
        '
        'pnlRepOXXO
        '
        Me.pnlRepOXXO.BackColor = System.Drawing.SystemColors.Control
        Me.pnlRepOXXO.Controls.Add(Me.cbxCancelado)
        Me.pnlRepOXXO.Controls.Add(Me.cbxPendiente)
        Me.pnlRepOXXO.Controls.Add(Me.cbxCorrecto)
        Me.pnlRepOXXO.Controls.Add(Me.cbxIncorrecto)
        Me.pnlRepOXXO.Controls.Add(Me.lblSeleccion)
        Me.pnlRepOXXO.Controls.Add(Me.btnCancelar)
        Me.pnlRepOXXO.Controls.Add(Me.btnAceptar)
        Me.pnlRepOXXO.Location = New System.Drawing.Point(28, 17)
        Me.pnlRepOXXO.Name = "pnlRepOXXO"
        Me.pnlRepOXXO.Size = New System.Drawing.Size(564, 164)
        Me.pnlRepOXXO.TabIndex = 181
        '
        'cbxPendiente
        '
        Me.cbxPendiente.AutoSize = True
        Me.cbxPendiente.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPendiente.Location = New System.Drawing.Point(421, 55)
        Me.cbxPendiente.Name = "cbxPendiente"
        Me.cbxPendiente.Size = New System.Drawing.Size(101, 22)
        Me.cbxPendiente.TabIndex = 182
        Me.cbxPendiente.Text = "Pendiente"
        Me.cbxPendiente.UseVisualStyleBackColor = True
        '
        'cbxCorrecto
        '
        Me.cbxCorrecto.AutoSize = True
        Me.cbxCorrecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCorrecto.Location = New System.Drawing.Point(37, 55)
        Me.cbxCorrecto.Name = "cbxCorrecto"
        Me.cbxCorrecto.Size = New System.Drawing.Size(94, 22)
        Me.cbxCorrecto.TabIndex = 181
        Me.cbxCorrecto.Text = "Correcto"
        Me.cbxCorrecto.UseVisualStyleBackColor = True
        '
        'cbxIncorrecto
        '
        Me.cbxIncorrecto.AutoSize = True
        Me.cbxIncorrecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxIncorrecto.Location = New System.Drawing.Point(153, 55)
        Me.cbxIncorrecto.Name = "cbxIncorrecto"
        Me.cbxIncorrecto.Size = New System.Drawing.Size(104, 22)
        Me.cbxIncorrecto.TabIndex = 180
        Me.cbxIncorrecto.Text = "Incorrecto"
        Me.cbxIncorrecto.UseVisualStyleBackColor = True
        '
        'cbxCancelado
        '
        Me.cbxCancelado.AutoSize = True
        Me.cbxCancelado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCancelado.Location = New System.Drawing.Point(287, 55)
        Me.cbxCancelado.Name = "cbxCancelado"
        Me.cbxCancelado.Size = New System.Drawing.Size(107, 22)
        Me.cbxCancelado.TabIndex = 183
        Me.cbxCancelado.Text = "Cancelado"
        Me.cbxCancelado.UseVisualStyleBackColor = True
        '
        'FrmOpRepOXXO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 198)
        Me.Controls.Add(Me.pnlRepOXXO)
        Me.Name = "FrmOpRepOXXO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Opciones de reporte para OXXO"
        Me.pnlRepOXXO.ResumeLayout(False)
        Me.pnlRepOXXO.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblSeleccion As System.Windows.Forms.Label
    Friend WithEvents pnlRepOXXO As System.Windows.Forms.Panel
    Friend WithEvents cbxCorrecto As System.Windows.Forms.CheckBox
    Friend WithEvents cbxIncorrecto As System.Windows.Forms.CheckBox
    Friend WithEvents cbxPendiente As System.Windows.Forms.CheckBox
    Friend WithEvents cbxCancelado As System.Windows.Forms.CheckBox
End Class
