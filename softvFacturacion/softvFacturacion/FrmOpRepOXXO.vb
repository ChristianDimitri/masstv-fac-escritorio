﻿Public Class FrmOpRepOXXO

    Protected Friend Resul As Integer

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cbxCorrecto.Checked = True Or cbxIncorrecto.Checked = True Or cbxPendiente.Checked = True Or cbxCancelado.Checked = True Then
            If cbxCorrecto.Checked = True And cbxIncorrecto.Checked = True And cbxPendiente.Checked = True And cbxCancelado.Checked = True Then
                Resul = 7
            ElseIf cbxIncorrecto.Checked = True And cbxPendiente.Checked = True And cbxCancelado.Checked = True Then
                Resul = 8
            ElseIf cbxCorrecto.Checked = True And cbxPendiente.Checked = True And cbxCancelado.Checked = True Then
                Resul = 9
            ElseIf cbxCorrecto.Checked = True And cbxIncorrecto.Checked = True And cbxCancelado.Checked = True Then
                Resul = 10
            ElseIf cbxCorrecto.Checked = True And cbxCancelado.Checked = True Then
                Resul = 11
            ElseIf cbxIncorrecto.Checked = True And cbxCancelado.Checked = True Then
                Resul = 12
            ElseIf cbxPendiente.Checked = True And cbxCancelado.Checked = True Then
                Resul = 13
            ElseIf cbxIncorrecto.Checked = True And cbxPendiente.Checked = True Then
                Resul = 6
            ElseIf cbxCorrecto.Checked = True And cbxPendiente.Checked = True Then
                Resul = 5
            ElseIf cbxCorrecto.Checked = True And cbxIncorrecto.Checked = True Then
                Resul = 4
            ElseIf cbxPendiente.Checked = True Then
                Resul = 3
            ElseIf cbxIncorrecto.Checked = True Then
                Resul = 2
            ElseIf cbxCorrecto.Checked = True Then
                Resul = 1
            ElseIf cbxCancelado.Checked = True Then
                Resul = 0
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            MsgBox("Seleccione al menos una opción", MsgBoxStyle.Information)
        End If
    End Sub
End Class