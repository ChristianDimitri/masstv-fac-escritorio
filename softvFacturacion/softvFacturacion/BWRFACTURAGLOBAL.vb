Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class BWRFACTURAGLOBAL
    Dim Letra2 As String = Nothing
    Dim Importe As String = Nothing
    Private customersByCityReport As ReportDocument

    Private Sub Llena_Compa�ias()

        Dim oIdCompania As Long = 0
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
            Me.CmbCompamia.DataSource = BaseII.ConsultaDT("Usp_MUESTRAtblCompanias_Factura_global")
            CmbCompamia.ValueMember = "clvCompania"
            CmbCompamia.DisplayMember = "nombreCompania"
            'oIdCompania = Usp_DameIdCompaniaFacturaGlobal(Contrato)
            'If oIdCompania > 0 Then
            '    ComboBox1.SelectedValue = oIdCompania
            'End If
            If CmbCompamia.Items.Count > 0 Then
                CmbCompamia.SelectedIndex = 0
            End If
        Catch ex As Exception
            MsgBox(Err.Description , MsgBoxStyle.Exclamation, "Error")

        End Try
    End Sub


    Private Sub Llena_Grid(ByVal op As Integer)

        Dim oIdCompania As Long = 0
        Try
            BuscaFacturasGlobalesDataGridView.DataSource = Nothing
            If IsNumeric(CmbCompamia.SelectedValue) = False Then oIdCompania = 0 Else oIdCompania = CmbCompamia.SelectedValue

            If oIdCompania = 0 Then Exit Sub

            Select Case op
                Case 0
                    '@ClvCompania int,@Serie varchar(10), @factura varchar(50), @fecha DateTime, @sucursal  varchar(150),@Op int,@tipo varchar(1)
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, Me.TextBox1.Text)
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, Me.TextBox4.Text)
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")

                Case 1

                    ' Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, Me.TextBox2.Text)
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 2
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 3

                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                Case 4

                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    BaseII.limpiaParametros()
                    BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, oIdCompania)
                    BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@factura", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, "01/01/1900")
                    BaseII.CreateMyParameter("@sucursal", SqlDbType.VarChar, "")
                    BaseII.CreateMyParameter("@Op", SqlDbType.Int, op)
                    BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "")
                    Me.BuscaFacturasGlobalesDataGridView.DataSource = BaseII.ConsultaDT("usp_BuscaFacturasGlobales_New")
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales
            End Select
            BuscaFacturasGlobalesDataGridView.Columns(0).HeaderText = "Serie"
            BuscaFacturasGlobalesDataGridView.Columns(1).HeaderText = "Factura"
            BuscaFacturasGlobalesDataGridView.Columns(2).HeaderText = "Fecha"
            BuscaFacturasGlobalesDataGridView.Columns(3).HeaderText = "Cajera(o)"
            BuscaFacturasGlobalesDataGridView.Columns(4).HeaderText = "Importe"
            BuscaFacturasGlobalesDataGridView.Columns(5).HeaderText = "Cancelada"
            BuscaFacturasGlobalesDataGridView.Columns(6).Visible = False
            BuscaFacturasGlobalesDataGridView.Columns(7).Visible = False
            BuscaFacturasGlobalesDataGridView.Columns(8).HeaderText = "Sucursal"


        Catch ex As Exception
            ' MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Error")

        End Try
        Try

            LlenaVariablesFila()
        Catch ex As Exception

        End Try
    End Sub

    'Private Sub busqueda(ByVal op As Integer)
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Select Case op
    '            Case 0
    '                Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
    '                Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
    '            Case 1
    '                Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
    '                Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
    '            Case 2
    '                Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
    '                Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
    '            Case 3
    '                Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
    '                Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
    '            Case 4
    '                Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
    '                Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
    '                'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales
    '        End Select
    '        CON.Close()
    '        LlenaVariablesFila()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String, ByVal FacturaID As Integer)
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim txtsubtotal As String = Nothing
            Dim subtotal2 As Double
            Dim iva2 As Double
            Dim myString As String = iva2.ToString("00.00")
            Dim reportPath As String = Nothing

            Select Case Locclv_empresa
                Case "AG"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
                Case "TO"
                    reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
                Case "SA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
                Case "VA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
            End Select
            customersByCityReport.Load(reportPath)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then
                'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                'subtotal2 = CDec(importe2) / 1.15
                'txtsubtotal = subtotal2
                'txtsubtotal = subtotal2.ToString("##0.00")
                'iva2 = CDec(importe2) / 1.15 * 0.15
                'myString = iva2.ToString("##0.00")
                'ElseIf Locclv_empresa = "AG" Then
                '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                '    subtotal2 = CDec(importe2) / 1.15
                '    txtsubtotal = subtotal2
                '    txtsubtotal = subtotal2.ToString("##0.00")
                '    iva2 = CDec(importe2) / 1.15 * 0.15
                '    myString = iva2.ToString("##0.00")
            End If

            Dim ieps As Double = 0
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Factura", SqlDbType.Int, FacturaID)
                Dim tblImpuestos As DataTable = BaseII.ConsultaDT("uspTraeImpuestosFacGlobal")

                For Each Fila As DataRow In tblImpuestos.Rows
                    ieps = CDbl(Fila("IEPS"))
                    myString = Fila("IVA")
                    txtsubtotal = Fila("SubTotal")
                    importe2 = Fila("Total")
                Next
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try


            customersByCityReport.DataDefinition.FormulaFields("IEPS").Text = "'" & ieps.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
            customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

            Select Case Locclv_empresa
                Case "AG"
                    'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "TO"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "SA"
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
                Case "VA"
                    'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
                    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            End Select


            customersByCityReport.PrintToPrinter(1, True, 1, 1)

            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub oldConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim cliente2 As String = "P�blico en General"
        Dim concepto2 As String = "Ingreso por Pago de Servicios"
        Dim txtsubtotal As String = Nothing
        Dim subtotal2 As Double
        Dim iva2 As Double
        Dim myString As String = iva2.ToString("00.00")
        Dim reportPath As String = Nothing

        If Locclv_empresa <> "TO" Then
            reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
        Else
            reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
        End If

        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
        subtotal2 = CDec(importe2) / 1.15
        txtsubtotal = subtotal2
        txtsubtotal = subtotal2.ToString("##0.00")
        iva2 = CDec(importe2) / 1.15 * 0.15
        myString = iva2.ToString("##0.00")
        customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
        customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

        If Locclv_empresa = "SA" Then
            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        ElseIf Locclv_empresa <> "TO" Then
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        Else
            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        End If

        customersByCityReport.PrintToPrinter(1, True, 1, 1)

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing


    End Sub

    Private Sub BWRFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bandera = 1 Then
            bec_bandera = 0
            'busqueda(4)
            Llena_Grid(4)
        End If

        'Try
        '    If BuscaFacturasGlobalesDataGridView.RowCount > 0 Then
        '        '5 4 9 (Botones de Consultar, Cancelar y Reimprimir)
        '        Button4.Enabled = True
        '        Button5.Enabled = True
        '        Button9.Enabled = True
        '    Else
        '        Button4.Enabled = False
        '        Button5.Enabled = False
        '        Button9.Enabled = False
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        'End Try
        

    End Sub
    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Importe), 0, Letra2)
            miLetra2 = Letra2
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BWRFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.

        Me.DameFechadelServidorHoraTableAdapter.Connection = CON
        Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
        CON.Close()
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRATIPOFACTGLO' Puede moverla o quitarla seg�n sea necesario.
        'Me.MUESTRATIPOFACTGLOTableAdapter.Connection = CON
        'Me.MUESTRATIPOFACTGLOTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRATIPOFACTGLO)

        ' bec_tipo = Me.ComboBox2.SelectedValue
        Llena_Compa�ias()
        Me.ComboBox2.Visible = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Llena_Grid(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Me.TextBox2.Text = String.Format("dd/mm/yyyy")
        Llena_Grid(1)
    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 And Me.TextBox4.Text = "" Then
            MsgBox("Selecciona Primero la Factura")
        ElseIf (Asc(e.KeyChar) = 13) Then
            Llena_Grid(0)
        End If
    End Sub
    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If (Asc(e.KeyChar) = 13) And Me.TextBox1.Text = "" Then
            MsgBox("Selecciona Primero la Serie")
        ElseIf (Asc(e.KeyChar) = 13) Then
            Llena_Grid(0)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Llena_Grid(2)
        End If
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Llena_Grid(1)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If CmbCompamia.Items.Count > 0 Then
            If CmbCompamia.SelectedIndex >= 1 Then

                Gloop = "N"
                LiTipo = 4
                bnd = 1
                CAPTURAFACTURAGLOBAL.Show()
            Else
                MsgBox("Seleccione la compa��a ", vbInformation)
            End If
        Else
            MsgBox("No existen compa��as ", vbInformation)
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If Me.CMBCanceladaTextBox.Text = "CANCELADA" Then
            MsgBox("La Factura ya ha sido Cancelada con Anterioridad")
        Else 'If Me.DateTimePicker1.Text = Me.CMBFechaTextBox.Text Then
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            resp = MsgBox("� Esta Seguro de que Desea Cancelar la Factura Global con Serie: " + Me.CMBSerieTextBox.Text + " y Folio:" + Me.FacturaLabel1.Text + " ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                'Inicio Edgar 04/Feb/2012 Factura_Digital
                DameId_FacturaCDF(Me.CMBIdFacturaTextBox.Text, "G")
                If Me.CMBIdFacturaTextBox.Text > 0 And GloClv_FacturaCFD > 0 Then
                    Cancelacion_FacturaCFD(GloClv_FacturaCFD)
                End If
                ''Fin

                bec_bandera = 1
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
                Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, " hola")
                CON.Close()
                MsgBox("La Factura ha sido Cancelada")
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Llena_Grid(2)
    End Sub

    'Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
    '    bec_tipo = Me.ComboBox2.SelectedValue
    '    Llena_Grid(3)
    'End Sub




    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Serie As String = Nothing
        Dim fecha As String = Nothing
        Dim Cajera As String = Nothing
        Dim Factura As String = Nothing
        Dim FacturaID As Integer = 0
        If CMBIdFacturaTextBox.Text > 0 Then


            Serie = Me.CMBSerieTextBox.Text
            Factura = Me.FacturaLabel1.Text
            fecha = Me.CMBFechaTextBox.Text
            FacturaID = CInt(CMBIdFacturaTextBox.Text)
            Cajera = Me.CMBCajeraTextBox.Text
            Importe = Me.CMBImporteTextBox.Text
            'CantidadaLetra()
            ''FACTURA DIGITAL 4/FEB/2012        
            Cajera = Me.CMBCajeraTextBox.Text
            Importe = Me.CMBImporteTextBox.Text
            CantidadaLetra()
            Locop = 1
            DameId_FacturaCDF(CMBIdFacturaTextBox.Text, "G")
            Try
                FormPruebaDigital.Show()
            Catch ex As Exception

            End Try
            Dim r As New Globalization.CultureInfo("es-MX")
            r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
            System.Threading.Thread.CurrentThread.CurrentCulture = r
            'FIN
            'If LocImpresoraTickets = "" Then
            '    MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
            'ElseIf LocImpresoraTickets <> "" Then
            '    LiTipo = 7
            '    bec_factura = Factura
            '    FrmImprimir.Show()
            '    'Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura, Factura)
            'End If
        Else
            MsgBox("Seleccione la factura global", vbInformation)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.FacturaLabel1.Text <> "" Then
            LocSerieglo = Me.CMBSerieTextBox.Text '.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = Me.FacturaLabel1.Text 'CInt(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value)
            LocFechaGlo = Me.CMBFechaTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocCajeroGlo = Me.CMBCajeraTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            LocImporteGlo = Me.CMBImporteTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            Locclv_sucursalglo = Me.Label6.Text
            LocFechaGloFinal = Me.Label8.Text
            bec_factura = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
            Gloop = "C"
            CAPTURAFACTURAGLOBAL.Show()
        Else
            MsgBox("No Se Ha Seleccionado Alguna Factura para Mostrar", MsgBoxStyle.Information)
        End If
    End Sub

   
   
    Private Sub BuscaFacturasGlobalesDataGridView_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BuscaFacturasGlobalesDataGridView.CurrentCellChanged
        LlenaVariablesFila()
    End Sub

    Public Sub LlenaVariablesFila()
        Try

            LocSerieglo = BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value
            LocFechaGlo = FormatDateTime(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value, DateFormat.ShortDate)
            LocCajeroGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocImporteGlo = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(4).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            Locclv_sucursalglo = Me.CmbCompamia.Text
            LocFechaGloFinal = LocFechaGlo
            bec_factura = Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            CMBSerieTextBox.Text = LocSerieglo
            Me.FacturaLabel1.Text = LocFacturaGlo
            Me.CMBFechaTextBox.Text = LocFechaGlo.ToShortDateString
            Me.CMBCajeraTextBox.Text = LocCajeroGlo
            Me.CMBCanceladaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(5).Value
            Me.CMBIdFacturaTextBox.Text = BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            Me.CMBImporteTextBox.Text = FormatCurrency(LocImporteGlo, 2, TriState.True)
            'GloFacGlobalclvcompania = BuscaFacturasGlobalesDataGridView.SelectedCells(7).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CmbCompamia_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CmbCompamia.SelectedIndexChanged
      
    End Sub

    Private Sub CmbCompamia_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles CmbCompamia.SelectedValueChanged
        Try
            If IsNumeric(CmbCompamia.SelectedValue) = True Then
                GloFacGlobalclvcompania = CmbCompamia.SelectedValue
            Else
                GloFacGlobalclvcompania = 0
            End If

            Llena_Grid(4)
        Catch ex As Exception

        End Try
    End Sub
End Class
