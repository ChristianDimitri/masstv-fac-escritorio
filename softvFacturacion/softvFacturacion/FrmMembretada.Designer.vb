<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMembretada
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.TextBoxSerie = New System.Windows.Forms.TextBox
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.TextBoxFolio = New System.Windows.Forms.TextBox
        Me.ButtonAceptar = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Red
        Me.CMBLabel1.Location = New System.Drawing.Point(29, 23)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(315, 37)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Establece la Serie y el Folio de la Factura Membretada"
        '
        'TextBoxSerie
        '
        Me.TextBoxSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSerie.Location = New System.Drawing.Point(145, 92)
        Me.TextBoxSerie.MaxLength = 10
        Me.TextBoxSerie.Name = "TextBoxSerie"
        Me.TextBoxSerie.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxSerie.TabIndex = 1
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(94, 98)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(45, 15)
        Me.CMBLabel2.TabIndex = 2
        Me.CMBLabel2.Text = "Serie:"
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(96, 144)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(43, 15)
        Me.CMBLabel3.TabIndex = 4
        Me.CMBLabel3.Text = "Folio:"
        '
        'TextBoxFolio
        '
        Me.TextBoxFolio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxFolio.Location = New System.Drawing.Point(145, 138)
        Me.TextBoxFolio.Name = "TextBoxFolio"
        Me.TextBoxFolio.Size = New System.Drawing.Size(100, 21)
        Me.TextBoxFolio.TabIndex = 3
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.Location = New System.Drawing.Point(218, 225)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(136, 36)
        Me.ButtonAceptar.TabIndex = 5
        Me.ButtonAceptar.Text = "&ACEPTAR"
        Me.ButtonAceptar.UseVisualStyleBackColor = True
        '
        'FrmMembretada
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(366, 273)
        Me.ControlBox = False
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.TextBoxFolio)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.TextBoxSerie)
        Me.Controls.Add(Me.CMBLabel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmMembretada"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Factura Membretada"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxSerie As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxFolio As System.Windows.Forms.TextBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
End Class
