﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelAparatosDeEntrega
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.enviarDerecha = New System.Windows.Forms.Button()
        Me.enviarIzquierda = New System.Windows.Forms.Button()
        Me.enviarTodosDerecha = New System.Windows.Forms.Button()
        Me.enviarTodosIzquierda = New System.Windows.Forms.Button()
        Me.lblMsj01 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.lblMsj02 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.cbTecnico = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.Location = New System.Drawing.Point(185, 3)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(393, 24)
        Me.lblTitulo.TabIndex = 71
        Me.lblTitulo.Text = "Selecciona los aparatos que trae el Cliente"
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.ForeColor = System.Drawing.Color.Black
        Me.btnAceptar.Location = New System.Drawing.Point(601, 459)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(170, 45)
        Me.btnAceptar.TabIndex = 73
        Me.btnAceptar.Text = "Siguiente >>"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(447, 459)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(148, 45)
        Me.btnSalir.TabIndex = 72
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        Me.btnSalir.Visible = False
        '
        'enviarDerecha
        '
        Me.enviarDerecha.BackColor = System.Drawing.Color.DarkOrange
        Me.enviarDerecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.enviarDerecha.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.enviarDerecha.ForeColor = System.Drawing.Color.Black
        Me.enviarDerecha.Location = New System.Drawing.Point(361, 161)
        Me.enviarDerecha.Name = "enviarDerecha"
        Me.enviarDerecha.Size = New System.Drawing.Size(68, 54)
        Me.enviarDerecha.TabIndex = 76
        Me.enviarDerecha.Text = ">"
        Me.enviarDerecha.UseVisualStyleBackColor = False
        '
        'enviarIzquierda
        '
        Me.enviarIzquierda.BackColor = System.Drawing.Color.DarkOrange
        Me.enviarIzquierda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.enviarIzquierda.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.enviarIzquierda.ForeColor = System.Drawing.Color.Black
        Me.enviarIzquierda.Location = New System.Drawing.Point(361, 263)
        Me.enviarIzquierda.Name = "enviarIzquierda"
        Me.enviarIzquierda.Size = New System.Drawing.Size(68, 54)
        Me.enviarIzquierda.TabIndex = 77
        Me.enviarIzquierda.Text = "<"
        Me.enviarIzquierda.UseVisualStyleBackColor = False
        '
        'enviarTodosDerecha
        '
        Me.enviarTodosDerecha.BackColor = System.Drawing.Color.DarkOrange
        Me.enviarTodosDerecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.enviarTodosDerecha.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.enviarTodosDerecha.ForeColor = System.Drawing.Color.Black
        Me.enviarTodosDerecha.Location = New System.Drawing.Point(361, 179)
        Me.enviarTodosDerecha.Name = "enviarTodosDerecha"
        Me.enviarTodosDerecha.Size = New System.Drawing.Size(68, 36)
        Me.enviarTodosDerecha.TabIndex = 78
        Me.enviarTodosDerecha.Text = ">>"
        Me.enviarTodosDerecha.UseVisualStyleBackColor = False
        Me.enviarTodosDerecha.Visible = False
        '
        'enviarTodosIzquierda
        '
        Me.enviarTodosIzquierda.BackColor = System.Drawing.Color.DarkOrange
        Me.enviarTodosIzquierda.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.enviarTodosIzquierda.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.enviarTodosIzquierda.ForeColor = System.Drawing.Color.Black
        Me.enviarTodosIzquierda.Location = New System.Drawing.Point(361, 263)
        Me.enviarTodosIzquierda.Name = "enviarTodosIzquierda"
        Me.enviarTodosIzquierda.Size = New System.Drawing.Size(68, 36)
        Me.enviarTodosIzquierda.TabIndex = 79
        Me.enviarTodosIzquierda.Text = "<<"
        Me.enviarTodosIzquierda.UseVisualStyleBackColor = False
        Me.enviarTodosIzquierda.Visible = False
        '
        'lblMsj01
        '
        Me.lblMsj01.AutoSize = True
        Me.lblMsj01.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsj01.Location = New System.Drawing.Point(66, 70)
        Me.lblMsj01.Name = "lblMsj01"
        Me.lblMsj01.Size = New System.Drawing.Size(186, 18)
        Me.lblMsj01.TabIndex = 80
        Me.lblMsj01.Text = "Aparatos que tiene el Cliente"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(12, 91)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(324, 346)
        Me.ListBox1.TabIndex = 81
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Location = New System.Drawing.Point(447, 92)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(324, 346)
        Me.ListBox2.TabIndex = 82
        '
        'lblMsj02
        '
        Me.lblMsj02.AutoSize = True
        Me.lblMsj02.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMsj02.Location = New System.Drawing.Point(503, 70)
        Me.lblMsj02.Name = "lblMsj02"
        Me.lblMsj02.Size = New System.Drawing.Size(216, 18)
        Me.lblMsj02.TabIndex = 83
        Me.lblMsj02.Text = "Aparatos que se están regresando"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 440)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(326, 18)
        Me.Label1.TabIndex = 84
        Me.Label1.Text = "Pasa los Aparatos que trae el cliente a la lista derecha -->"
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'cbTecnico
        '
        Me.cbTecnico.DisplayMember = "Nombre"
        Me.cbTecnico.FormattingEnabled = True
        Me.cbTecnico.Location = New System.Drawing.Point(215, 471)
        Me.cbTecnico.Name = "cbTecnico"
        Me.cbTecnico.Size = New System.Drawing.Size(121, 24)
        Me.cbTecnico.TabIndex = 85
        Me.cbTecnico.ValueMember = "clv_Tecnico"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(73, 474)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 16)
        Me.Label2.TabIndex = 86
        Me.Label2.Text = "Seleccione el Tecnico:"
        '
        'FrmSelAparatosDeEntrega
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 513)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbTecnico)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMsj02)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblMsj01)
        Me.Controls.Add(Me.enviarIzquierda)
        Me.Controls.Add(Me.enviarDerecha)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.lblTitulo)
        Me.Controls.Add(Me.enviarTodosIzquierda)
        Me.Controls.Add(Me.enviarTodosDerecha)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "FrmSelAparatosDeEntrega"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección Aparatos De Entrega"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents enviarDerecha As System.Windows.Forms.Button
    Friend WithEvents enviarIzquierda As System.Windows.Forms.Button
    Friend WithEvents enviarTodosDerecha As System.Windows.Forms.Button
    Friend WithEvents enviarTodosIzquierda As System.Windows.Forms.Button
    Friend WithEvents lblMsj01 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents lblMsj02 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents cbTecnico As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
