﻿Public Class frmAdelantaParcialidades

    Public NumMaximo As Integer
    Public DetClaveFactura As Long

    Private Sub frmAdelantaParcialidades_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.nudAdelantados.Maximum = NumMaximo
        Me.nudAdelantados.Value = 1
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(Me.nudAdelantados.Value) = True Then
            If Me.nudAdelantados.Value > NumMaximo Then
                MsgBox("Los Pagos adelantados no pueden ser mayor al número de Pagos Restantes", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.nudAdelantados.Value > 0 Then
                'AQUI VA EL CÓDIGO
                bnd2pardep1 = True
                CobroParcialMaterial.spAdelantaPagosParcialesMaterial(0, 0, Me.nudAdelantados.Value, DetClaveFactura, 2)
                Me.Close()
            Else
                MsgBox("Capture una Cantidad Mayor a Cero", MsgBoxStyle.Information)
                Exit Sub
            End If
        Else
            MsgBox("Ingrese una Cantidad Válida", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub
End Class