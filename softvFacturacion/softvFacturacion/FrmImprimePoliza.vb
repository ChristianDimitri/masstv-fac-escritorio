Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient


Public Class FrmImprimePoliza
    'Private customersByCityReport As ReportDocument
    'Private op As String = Nothing
    'Private Titulo As String = Nothing

    Private Sub FrmImprimePoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dS As New DataSet
        dS = REPORTEPoliza(gLOoPrEPpOLIZA, LocGloClv_poliza, GloClvCompania)

        If gLOoPrEPpOLIZA = 1 Then
            rDocument.Load(RutaReportes + "\RepPolizaDetallado.rpt")
        Else
            rDocument.Load(RutaReportes + "\RepPoliza.rpt")
        End If
        rDocument.SetDataSource(dS)
        CrystalReportViewer1.ReportSource = rDocument
        CrystalReportViewer1.Zoom(75)

    End Sub

    Private Function REPORTEPoliza(ByVal Tipo As Integer, ByVal ClvLlavePoliza As Integer, ByVal ClvCompania As Integer) As DataSet
        Dim listaTablas As New List(Of String)
        listaTablas.Add("Softv_GetReportePolizasIngreso")
        listaTablas.Add("tblCompanias")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@TIPO", SqlDbType.Int, Tipo)
        BaseII.CreateMyParameter("@CLVLLAVEPOLIZA", SqlDbType.Int, ClvLlavePoliza)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, ClvCompania)
        REPORTEPOLIZA = BaseII.ConsultaDS("REPORTEPoliza", listaTablas)
    End Function

    'Private Sub ConfigureCrystalReports()
    '    customersByCityReport = New ReportDocument
    '    Dim connectionInfo As New ConnectionInfo


    '    Dim reportPath As String = Nothing
    '    Dim Titulo As String = Nothing
    '    Dim Sucursal As String = Nothing
    '    Dim Ciudades As String = Nothing

    '    Ciudades = " Ciudad(es): " + LocCiudades
    '    reportPath = RutaReportes + "\RepPoliza.rpt"
    '    Dim cnn As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand("GeneraReportePoliza", cnn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandTimeout = 0
    '    Dim parametro1 As New SqlParameter("@Clv_llave_Poliza", SqlDbType.BigInt)
    '    parametro1.Direction = ParameterDirection.Input
    '    parametro1.Value = LocGloClv_poliza
    '    cmd.Parameters.Add(parametro1)
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim ds As New DataSet()
    '    da.Fill(ds)
    '    ds.Tables(0).TableName = "Softv_GetReportePolizasIngreso"
    '    customersByCityReport.Load(reportPath)
    '    customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloCiudadEmpresa & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'"
    '    customersByCityReport.SetDataSource(ds)
    '    CrystalReportViewer1.ReportSource = customersByCityReport
    '    CrystalReportViewer1.Zoom(75)
    '    If GloOpFacturas = 3 Then
    '        CrystalReportViewer1.ShowExportButton = True
    '        CrystalReportViewer1.ShowPrintButton = True
    '        CrystalReportViewer1.ShowRefreshButton = False
    '    End If

    'End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
    '    'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '    Next
    'End Sub

    'Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
    '    customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
    '    'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

    '    Dim I As Integer = myReportDocument.Subreports.Count
    '    Dim X As Integer = 0
    '    For X = 0 To I - 1
    '        Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
    '        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '        For Each myTable In myTables
    '            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '            myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '            myTable.ApplyLogOnInfo(myTableLogonInfo)
    '            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
    '        Next
    '    Next X
    'End Sub

    'Private Sub ConfigureCrystalReportsDetallado()
    '    customersByCityReport = New ReportDocument
    '    Dim connectionInfo As New ConnectionInfo


    '    Dim reportPath As String = Nothing
    '    Dim Titulo As String = Nothing
    '    Dim Sucursal As String = Nothing
    '    Dim Ciudades As String = Nothing

    '    Ciudades = " Ciudad(es): " + LocCiudades
    '    reportPath = RutaReportes + "\RepPolizaDetallado.rpt"
    '    Dim cnn As New SqlConnection(MiConexion)
    '    Dim cmd As New SqlCommand("GeneraReportePoliza_Detallado", cnn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandTimeout = 0
    '    Dim parametro1 As New SqlParameter("@Clv_llave_Poliza", SqlDbType.BigInt)
    '    parametro1.Direction = ParameterDirection.Input
    '    parametro1.Value = LocGloClv_poliza
    '    cmd.Parameters.Add(parametro1)
    '    Dim da As New SqlDataAdapter(cmd)
    '    Dim ds As New DataSet()
    '    da.Fill(ds)
    '    ds.Tables(0).TableName = "Softv_GetReportePolizasIngreso"
    '    customersByCityReport.Load(reportPath)
    '    customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
    '    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloCiudadEmpresa & "'"
    '    'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'"
    '    customersByCityReport.SetDataSource(ds)
    '    CrystalReportViewer1.ReportSource = customersByCityReport
    '    CrystalReportViewer1.Zoom(75)
    '    If GloOpFacturas = 3 Then
    '        CrystalReportViewer1.ShowExportButton = True
    '        CrystalReportViewer1.ShowPrintButton = True
    '        CrystalReportViewer1.ShowRefreshButton = False
    '    End If

    'End Sub




End Class