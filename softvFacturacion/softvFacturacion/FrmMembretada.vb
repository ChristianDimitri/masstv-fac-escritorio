Imports System.Data.SqlClient
Public Class FrmMembretada

    Private Sub NueTmpRelFacturasMembretadas(ByVal ClvSession As Long, ByVal Serie As String, ByVal Folio As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueTmpRelFacturasMembretadas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Serie", SqlDbType.VarChar, 10)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Serie
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Folio", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Folio
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmMembretada_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click

        If TextBoxSerie.Text.Length = 0 Then
            MsgBox("Establece el Serie.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If TextBoxFolio.Text.Length = 0 Then
            MsgBox("Establece el Folio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueTmpRelFacturasMembretadas(eClv_Session, TextBoxSerie.Text, TextBoxFolio.Text)
        eBndMembretadas = True
        FrmFAC.BotonGrabar()
        Me.Close()
    End Sub
End Class