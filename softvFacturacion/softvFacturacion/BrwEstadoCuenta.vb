﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class BrwEstadoCuenta

    Private Sub MuestraEstadoCuentaPeriodo()
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuentaPeriodo")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgPeriodo.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub MuestraEstadoCuenta(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuenta " + Id.ToString())
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgEstado.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ReporteEstadoCuenta(ByVal Id As Integer, ByVal Contrato As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC ReporteEstadoCuenta " + Id.ToString() + ", " + Contrato.ToString() + ", " + Op.ToString())
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataSet)
            dataTable = MuestraGeneral()


            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"
            dataSet.Tables.Add(dataTable)
            dataSet.Tables(2).TableName = "General"

            reportDocument.Load(RutaReportes + "\ReporteEstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)

            FrmImprimir.CrystalReportViewer1.ReportSource = reportDocument
            FrmImprimir.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function MuestraGeneral() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraGeneral")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()

        Try

            dataAdapter.Fill(dataTable)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dataTable

    End Function

    Private Sub bnVerTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVerTodos.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un periodo.")
            Exit Sub
        End If
        ReporteEstadoCuenta(dgPeriodo.SelectedCells(0).Value, 0, 0)
    End Sub



    Private Sub bnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVer.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.")
            Exit Sub
        End If
        ReporteEstadoCuenta(dgPeriodo.SelectedCells(0).Value, dgEstado.SelectedCells(1).Value, 1)
    End Sub


    Private Sub BrwEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        MuestraEstadoCuentaPeriodo()
    End Sub




    Private Sub dgPeriodo_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgPeriodo.SelectionChanged
        Try
            If dgPeriodo.Rows.Count = 0 Then Exit Sub
            MuestraEstadoCuenta(dgPeriodo.SelectedCells(0).Value)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class