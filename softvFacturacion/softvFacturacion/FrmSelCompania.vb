﻿Public Class FrmSelCompania

    Private Sub FrmSelCompania_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        colorea(Me)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbCompania.Text.Length = 0 Then
            MessageBox.Show("Selecciona una Compañía.")
            Exit Sub
        End If
        If CInt(cbCompania.SelectedValue.ToString()) = 0 Then
            MessageBox.Show("Selecciona una Compañía.")
            Exit Sub
        End If
        GloClvCompania = cbCompania.SelectedValue
        LocbndPolizaCiudad = False
        LocBndrelingporconceptos = True
        FrmSelCiudad.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        LocbndPolizaCiudad = False
        LocBndrelingporconceptos = False
        Me.Close()
    End Sub
End Class