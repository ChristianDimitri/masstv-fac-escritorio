<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadoEntregasParciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.LLena_Tabla_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LLena_Tabla_UsuariosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.LLena_Tabla_UsuariosTableAdapter
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.CMBLabel2 = New System.Windows.Forms.Label
        CType(Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LLena_Tabla_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource
        Me.ListBox1.DisplayMember = "Nombre"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(15, 35)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(202, 225)
        Me.ListBox1.TabIndex = 0
        Me.ListBox1.TabStop = False
        Me.ListBox1.ValueMember = "clv_usuario"
        '
        'MUESTRAUSUARIOSConEntregasParcialesBindingSource
        '
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource.DataMember = "MUESTRAUSUARIOS_Con_Entregas_Parciales"
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(369, 35)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(209, 225)
        Me.ListBox2.TabIndex = 1
        Me.ListBox2.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(262, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(262, 64)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Enabled = False
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(262, 132)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Enabled = False
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(262, 161)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter
        '
        Me.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(475, 274)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&Cancelar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(296, 274)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 4
        Me.Button6.Text = "&Aceptar"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'MUESTRAUSUARIOSConEntregasParcialesBindingSource1
        '
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource1.DataMember = "MUESTRAUSUARIOS_Con_Entregas_Parciales"
        Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource1.DataSource = Me.Procedimientos_arnoldo
        '
        'ListBox3
        '
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.Location = New System.Drawing.Point(15, 267)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.ScrollAlwaysVisible = True
        Me.ListBox3.Size = New System.Drawing.Size(202, 43)
        Me.ListBox3.TabIndex = 18
        Me.ListBox3.TabStop = False
        Me.ListBox3.Visible = False
        '
        'LLena_Tabla_UsuariosBindingSource
        '
        Me.LLena_Tabla_UsuariosBindingSource.DataMember = "LLena_Tabla_Usuarios"
        Me.LLena_Tabla_UsuariosBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'LLena_Tabla_UsuariosTableAdapter
        '
        Me.LLena_Tabla_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 16)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(180, 16)
        Me.CMBLabel1.TabIndex = 19
        Me.CMBLabel1.Text = "Cajeras por Seleccionar:"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel2.Location = New System.Drawing.Point(366, 16)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(174, 16)
        Me.CMBLabel2.TabIndex = 20
        Me.CMBLabel2.Text = "Cajeras Seleccionadas:"
        '
        'FrmListadoEntregasParciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(628, 322)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Name = "FrmListadoEntregasParciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selección de Cajeros(as) con Entrega Parcial"
        CType(Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOSConEntregasParcialesBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LLena_Tabla_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents MUESTRAUSUARIOSConEntregasParcialesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAUSUARIOS_Con_Entregas_ParcialesTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents MUESTRAUSUARIOSConEntregasParcialesBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents LLena_Tabla_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LLena_Tabla_UsuariosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.LLena_Tabla_UsuariosTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
End Class
