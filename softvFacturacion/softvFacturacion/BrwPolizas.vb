Imports System.Data.SqlClient

Public Class BrwPolizas

    'Private Sub busca(ByVal op As Integer)
    '    Dim con As New SqlConnection(MiConexion)
    '    con.Open()
    '    Select Case op
    '        Case 0
    '            Me.Busca_PolizaTableAdapter.Connection = con
    '            Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", "")
    '        Case 1
    '            Me.Busca_PolizaTableAdapter.Connection = con
    '            Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, CLng(Me.TextBox1.Text), "", "")
    '        Case 2
    '            Me.Busca_PolizaTableAdapter.Connection = con
    '            Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, CStr(DateTimePicker1.Value), "")
    '            'Case 3
    '            '    Me.Busca_PolizaTableAdapter.Connection = con
    '            '   Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", Me.TextBox4.Text)
    '    End Select

    'End Sub

    Private Sub BrwPolizas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndactualizapoiza = True Then
            Locbndactualizapoiza = False
            MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue)
        End If
        'If Locbndactualizapoiza = True Then
        '    Locbndactualizapoiza = False
        '    busca(0)
        'End If
    End Sub



    Private Sub BrwPolizas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clvCompania", SqlDbType.Int, 0)
        cbCompania.DataSource = BaseII.ConsultaDT("MUESTRAtblCompanias")

        'Me.StartPosition = FormStartPosition.CenterScreen
        'colorea(Me)
        'gLOoPrEPpOLIZA = 0
        'busca(0)
        'Me.Text = "Polizas"
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If TextBox1.Text.Length = 0 Then
            MessageBox.Show("Captura una Clave P�liza.")
            Exit Sub
        End If
        If IsNumeric(TextBox1.Text) = False Then
            MessageBox.Show("Captura una Clave P�liza v�lida.")
            Exit Sub
        End If
        MUESTRAPoliza(1, TextBox1.Text, DateTime.Today, "", cbCompania.SelectedValue)
        'busca(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MUESTRAPoliza(2, 0, DateTimePicker1.Value, "", cbCompania.SelectedValue)
        'busca(2)

    End Sub

    

    'Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
    '    e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    '    If Asc(e.KeyChar) = 13 Then
    '        If TextBox1.Text.Length = 0 Then
    '            MessageBox.Show("Captura una Clave P�liza.")
    '            Exit Sub
    '        End If
    '        If IsNumeric(TextBox1.Text) = False Then
    '            MessageBox.Show("Captura una Clave P�liza v�lida.")
    '            Exit Sub
    '        End If
    '        MUESTRAPoliza(1, TextBox1.Text, DateTime.Today, "", cbCompania.SelectedValue)
    '    End If
    'End Sub



    'Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    If Asc(e.KeyChar) = 13 Then
    '        busca(2)
    '    End If
    'End Sub

    
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim COn As New SqlConnection(MiConexion)
        'COn.Open()
        'LocopPoliza = "N"
        'Me.DameClv_Session_ServiciosTableAdapter.Connection = COn
        'Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        'COn.Close()
        'LocbndPolizaCiudad = True
        'FrmSelCiudad.Show()
        LocopPoliza = "N"
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Dim comando As SqlClient.SqlCommand
        'comando = New SqlClient.SqlCommand
        'With comando
        '    .Connection = CON
        '    .CommandText = "DameClv_Session_Servicios "
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandTimeout = 0
        '    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        '    prm.Direction = ParameterDirection.Output
        '    prm.Value = LocClv_session
        '    .Parameters.Add(prm)
        '    Dim i As Integer = comando.ExecuteNonQuery()
        '    LocClv_session = prm.Value
        'End With
        'CON.Close()
        gloClv_Session = 0
        LocbndPolizaCiudad = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza para ser Consultada.")
            Exit Sub
        End If
        LocopPoliza = "C"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPoliza.Show()
        'LocopPoliza = "C"
        'LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        'FrmPoliza.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza para ser Modificada.")
            Exit Sub
        End If
        LocopPoliza = "M"
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        FrmPoliza.Show()
        'LocopPoliza = "M"
        'LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        'FrmPoliza.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If DataGridView1.Rows.Count = 0 Then
            MessageBox.Show("Selecciona una P�liza a Eliminar.")
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_POLIZA", SqlDbType.Int, DataGridView1.SelectedCells(0).Value)
        BaseII.Inserta("ELIMINAPoliza")

        MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue)

        'If IsNumeric(LocGloClv_poliza) = True Then
        '    Dim cONe As New SqlConnection(MiConexion)
        '    cONe.Open()
        '    Dim comando As SqlClient.SqlCommand
        '    Dim reader As SqlDataReader
        '    comando = New SqlClient.SqlCommand
        '    With comando
        '        .Connection = cONe
        '        .CommandText = "EXEC BORRA_Genera_Poliza " & LocGloClv_poliza
        '        .CommandType = CommandType.Text
        '        .CommandTimeout = 0
        '        reader = comando.ExecuteReader()
        '    End With
        '    cONe.Close()
        '    busca(0)
        'Else
        '    MsgBox("Seleccione la P�liza ", MsgBoxStyle.Information)
        'End If
    End Sub



    'Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
    '    If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
    '        LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
    '    End If
    'End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        LocGloClv_poliza = DataGridView1.SelectedCells(0).Value
        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 0
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        If IsNumeric(LocGloClv_poliza) = True Then
            gLOoPrEPpOLIZA = 1
            FrmImprimePoliza.Show()
            gLOoPrEPpOLIZA = 0
        End If
    End Sub

    Private Sub MUESTRAPoliza(ByVal Op As Integer, ByVal Clv_Llave_Poliza As Integer, ByVal Fecha As DateTime, ByVal Concepto As String, ByVal ClvCompania As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Llave_Poliza", SqlDbType.Int, Clv_Llave_Poliza)
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha)
        BaseII.CreateMyParameter("@Concepto", SqlDbType.VarChar, Concepto, 250)
        BaseII.CreateMyParameter("@ClvCompania", SqlDbType.Int, ClvCompania)
        DataGridView1.DataSource = BaseII.ConsultaDT("MUESTRAPoliza")
    End Sub

    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If

        GloClvCompania = cbCompania.SelectedValue

        MUESTRAPoliza(0, 0, DateTime.Today, "", cbCompania.SelectedValue)
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Try
            Clv_calleLabel2.Text = DataGridView1.SelectedCells(0).Value
            CMBNombreTextBox.Text = DataGridView1.SelectedCells(1).Value
            CMBTextBox5.Text = DataGridView1.SelectedCells(2).Value
        Catch ex As Exception

        End Try
    End Sub
End Class