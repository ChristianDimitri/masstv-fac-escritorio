Imports System.Data.SqlClient

Public Class FrmFechaIngresosConcepto

    Private Sub FrmFechaIngresosConcepto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.MaxDate = Today
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaInicial = Me.DateTimePicker1.Value
        eFechaFinal = Me.DateTimePicker2.Value
        If GloBnd_Des_Men = True Then
            FrmImprimirIngresosConcepto.Show()
        ElseIf LocbndPolizaCiudad = True Then
            PROCESOGeneraPoliza()
            'GeneraPoliza()
            Locbndactualizapoiza = True
        Else
            FrmImprimirIngresosConcepto.Show()
        End If
        Me.Close()
    End Sub

    Private Sub PROCESOGeneraPoliza()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha_Ini", SqlDbType.DateTime, eFechaInicial)
        BaseII.CreateMyParameter("@Fecha_Fin", SqlDbType.DateTime, eFechaFinal)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, "C", 1)
        BaseII.CreateMyParameter("@sucursal", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Caja", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Cajera", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, gloClv_Session)
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@Clv_llave_PolizaNew", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@NumError", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@MsjError", ParameterDirection.Output, SqlDbType.VarChar, 250)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        BaseII.ProcedimientoOutPut("PROCESOGeneraPoliza")
        LocGloClv_poliza = CInt(BaseII.dicoPar("@Clv_llave_PolizaNew").ToString())

        LocopPoliza = "N"
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se gener� P�liza", "Con Clave de P�liza de: " + CStr(LocGloClv_poliza), LocClv_Ciudad)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_llave_Poliza", SqlDbType.Int, LocGloClv_poliza)
        BaseII.CreateMyParameter("@opc", SqlDbType.VarChar, LocopPoliza, 5)
        BaseII.CreateMyParameter("@clave", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Dame_Clave_poliza")
        GloPoliza2 = CInt(BaseII.dicoPar("@clave").ToString())

        FrmPoliza.Show()
    End Sub

    Private Sub GeneraPoliza()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            '.CommandText = "GeneraPoliza "
            .CommandText = "Genera_Poliza_New_Saltillo"
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            '  varchar(10)
            Dim prm As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
            prm.Direction = ParameterDirection.Input
            prm.Value = eFechaInicial
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = eFechaFinal
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = "C"
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@sucursal", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = 0
            .Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = 0
            .Parameters.Add(prm4)

            Dim prm5 As New SqlParameter("@Cajera", SqlDbType.VarChar)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = GloUsuario
            .Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Op", SqlDbType.VarChar)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = 0
            .Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = gloClv_Session
            .Parameters.Add(prm7)

            Dim prm8 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 11)
            prm8.Direction = ParameterDirection.Input
            prm8.Value = GloUsuario
            .Parameters.Add(prm8)

            Dim prm9 As New SqlParameter("@Clv_llave_PolizaNew", SqlDbType.BigInt)
            prm9.Direction = ParameterDirection.InputOutput
            prm9.Value = 0
            .Parameters.Add(prm9)

            Dim prm10 As New SqlParameter("@NumError", SqlDbType.Int)
            prm10.Direction = ParameterDirection.InputOutput
            prm10.Value = 0
            .Parameters.Add(prm10)

            Dim prm11 As New SqlParameter("@MsjError", SqlDbType.VarChar, 250)
            prm11.Direction = ParameterDirection.InputOutput
            prm11.Value = 0
            .Parameters.Add(prm11)

            Dim i As Integer = comando.ExecuteNonQuery()
            LocGloClv_poliza = prm9.Value
            LocopPoliza = "N"
        End With
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Genero Poliza", "Con Clave de poliza de: " + CStr(LocGloClv_poliza), LocClv_Ciudad)
        Dim ComandoLidia As New SqlClient.SqlCommand
        With ComandoLidia
            .CommandText = "Dame_Clave_poliza"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON
            Dim Prm1 As New SqlParameter("@Clv_Llave_Poliza", SqlDbType.BigInt)
            Prm1.Direction = ParameterDirection.Input
            Prm1.Value = LocGloClv_poliza
            .Parameters.Add(Prm1)

            Dim Prm2 As New SqlParameter("@opc", SqlDbType.VarChar, 5)
            Prm2.Direction = ParameterDirection.Input
            Prm2.Value = LocopPoliza
            .Parameters.Add(Prm2)

            Dim Prm3 As New SqlParameter("@Clave", SqlDbType.BigInt)
            Prm3.Direction = ParameterDirection.Output
            Prm3.Value = 0
            .Parameters.Add(Prm3)

            Dim i As Integer = ComandoLidia.ExecuteNonQuery()
            GloPoliza2 = Prm3.Value

        End With
        CON.Close()
        FrmPoliza.Show()
    End Sub

End Class