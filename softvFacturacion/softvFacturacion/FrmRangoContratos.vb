Public Class FrmRangoContratos

    Private Sub FrmRangoContratos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Glocontratosel > 0 Then
            If bndcontrato = 1 Then
                bndcontrato = 0
                Me.TxtContrato1.Text = Glocontratosel
            ElseIf bndcontrato = 2 Then
                bndcontrato = 0
                Me.TxtContrato2.Text = Glocontratosel
            End If
            Glocontratosel = 0
        ElseIf Glocontratosel = 0 Then
            If bndcontrato = 1 Then
                bndcontrato = 0
                If Len(Me.TxtContrato1.Text) = 0 Then
                    Me.TxtContrato1.Text = 0
                End If
            ElseIf bndcontrato = 2 Then
                bndcontrato = 0
                If Len(Me.TxtContrato2.Text) = 0 Then
                    Me.TxtContrato2.Text = 0
                End If
            End If
        End If
    End Sub

    Private Sub Aceptar()
        If Len(Me.TxtContrato1.Text) = 0 Then
            MsgBox("Selecciona El Contrato Inicial", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Me.TxtContrato2.Text) = 0 Then
            MsgBox("Selecciona El Contrato Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CLng(Me.TxtContrato1.Text) > CLng(Me.TxtContrato2.Text) Then
            MsgBox("El Contrato Inicial Debe De Ser Menor Que El Contrato Final", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CLng(Me.TxtContrato1.Text) = 0 Then
            MsgBox("El Contrato Inicial Debe De Ser Mayor A Cero", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CLng(Me.TxtContrato2.Text) = 0 Then
            MsgBox("El Contrato Final Debe De Ser Mayor A Cero", MsgBoxStyle.Information)
            Exit Sub
        End If


        contratoini = Me.TxtContrato1.Text
        contratofin = Me.TxtContrato2.Text
        BndRepImporteClietnes = True
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub


    Private Sub FrmRangoContratos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
    End Sub

    Private Sub BtnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Me.Close()
    End Sub

    Private Sub BtnBusca1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBusca1.Click
        Glocontratosel = 0
        bndcontrato = 1
        FrmSelCliente.Show()
    End Sub

    Private Sub BtnBusca2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBusca2.Click
        Glocontratosel = 0
        bndcontrato = 2
        FrmSelCliente.Show()
    End Sub

    Private Sub TxtContrato1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContrato1.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtContrato1, Asc(e.KeyChar), "N"))
        If Asc(e.KeyChar) = 13 Then
            Aceptar()
        End If
    End Sub

    Private Sub TxtContrato2_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContrato2.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtContrato2, Asc(e.KeyChar), "N"))
        If Asc(e.KeyChar) = 13 Then
            Aceptar()
        End If
    End Sub

    Private Sub BtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click
        Aceptar()
    End Sub
End Class