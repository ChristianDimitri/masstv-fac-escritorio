'Imports System.Collections
'Imports System.Web.UI.WebControls
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text


Public Class FrmImprimir
    Private customersByCityReport As ReportDocument
    Dim bloqueado, identi As Integer
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable

        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()

        If IdSistema = "TO" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            ba = True
        ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReportesFacturasXsd.rpt"
            ba = True
        ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            'reportPath = RutaReportes + "\ReporteCajasCosmo.rpt"
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        Else
            If IdSistema = "VA" Then
                'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            Else
                If AuxImprimirTicket = 1 Then
                    ConfigureCrystalReports_tickets(Clv_Factura)
                    Me.Close()
                    Exit Sub
                Else
                    ConfigureCrystalReports_ticketsVer(Clv_Factura)
                    Exit Sub
                End If
                Exit Sub
            End If
        End If

        ReportesFacturasXsd(Clv_Factura, 0, 0, "01-01-1900", "01-01-1900", 0, reportPath)
        'End If

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '' End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If



        CrystalReportViewer1.ReportSource = customersByCityReport

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

   Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)

        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable
        Dim dSet As New DataSet
        Dim ruta As String
        Dim referenciaOxxo As String = SP_REFERENCIA_OXXOFactura(Clv_Factura)
        dSet = ReportesFacturas(Clv_Factura)
        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        '    Case "MASSTVREYNOSA"
        '        'ruta = RutaReportes + "\ReporteCajasTickets_2(AG).rpt"
        '        ruta = RutaReportes + "\Report1.rpt"
        '    Case Else
        '        ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        'End Select
        'ruta = RutaReportes + "\ReporteCajasTickets.rpt"

        rDocument.Load(ruta)
        rDocument.SetDataSource(dSet)

        eMsjTickets = ""
        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()


        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        '        rDocument.SetDataSource(dSet)
        '    Case "MASSTVREYNOSA"
        '        SetDBReport(dSet, rDocument)
        '        ''@Clv_Factura 
        '        'rDocument.SetParameterValue(0, Clv_Factura)
        '        ''@Clv_Factura_Ini
        '        'rDocument.SetParameterValue(1, "0")
        '        ''@Clv_Factura_Fin
        '        'rDocument.SetParameterValue(2, "0")
        '        ''@Fecha_Ini
        '        'rDocument.SetParameterValue(3, "01/01/1900")
        '        ''@Fecha_Fin
        '        'rDocument.SetParameterValue(4, "01/01/1900")
        '        ''@op
        '        'rDocument.SetParameterValue(5, "0")

        '        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        'End Select
        If eActTickets = True Then
            rDocument.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            rDocument.DataDefinition.FormulaFields("Roxxo").Text = "'" & referenciaOxxo & "'"
        End If

        rDocument.PrintOptions.PrinterName = LocImpresoraTickets
        rDocument.PrintToPrinter(1, True, 1, 1)
        rDocument.Dispose()

        rDocument = Nothing

        Me.Close()
        'Dim ba As Boolean = False
        'If path.Contains("reynosa") = True Then
        '    customersByCityReport = New ReporteCajasTickets_2AG
        'Else
        '    customersByCityReport = New ReporteCajasTickets_2SA

        'End If
        ''Select Case IdSistema
        ''    Case "VA"
        ''        customersByCityReport = New ReporteCajasTickets_2VA
        ''    Case "LO"
        ''        customersByCityReport = New ReporteCajasTickets_2Log
        ''    Case "AG"
        ''        customersByCityReport = New ReporteCajasTickets_2AG
        ''    Case "SA"
        ''        customersByCityReport = New ReporteCajasTickets_2SA
        ''    Case "TO"
        ''        customersByCityReport = New ReporteCajasTickets_2TOM
        ''    Case Else
        ''        customersByCityReport = New ReporteCajasTickets_2OLD
        ''End Select


        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        ''        If GloImprimeTickets = False Then
        ''reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        ''Else
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BusFacFiscalTableAdapter.Connection = CON
        'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        'CON.Close()
        'CON.Dispose()

        'eActTickets = False
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        'CON2.Close()
        'CON2.Dispose()

        ''If IdSistema = "VA" Then
        ''    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
        ''    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        ''Else
        ''    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        ''End If

        ''End If

        ''customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)


        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If

        ''If facticket = 1 Then
        ''    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


        'customersByCityReport.PrintToPrinter(1, True, 1, 1)





        ''CrystalReportViewer1.ReportSource = customersByCityReport

        ''If GloOpFacturas = 3 Then
        ''CrystalReportViewer1.ShowExportButton = False
        ''CrystalReportViewer1.ShowPrintButton = False
        ''CrystalReportViewer1.ShowRefreshButton = False
        ''End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport.Dispose()
    End Sub

    Private Sub ConfigureCrystalReports_ticketsVer(ByVal Clv_Factura As Long)
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable
        Dim dSet As New DataSet
        Dim ruta As String
        Dim referenciaOxxo As String = SP_REFERENCIA_OXXOFactura(Clv_Factura)
        dSet = ReportesFacturas(Clv_Factura)
        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        '    Case "MASSTVREYNOSA"
        ''ruta = RutaReportes + "\ReporteCajasTickets_2(AG).rpt"
        'ruta = RutaReportes + "\Report1.rpt"
        '    Case Else
        'ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        'End Select
        'ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        rDocument.Load(ruta)
        rDocument.SetDataSource(dSet)

        eMsjTickets = ""
        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()
        CON2.Dispose()

        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        '        rDocument.SetDataSource(dSet)
        '    Case "MASSTVREYNOSA"
        '        SetDBReport(dSet, rDocument)
        '        ''@Clv_Factura 
        '        'rDocument.SetParameterValue(0, Clv_Factura)
        '        ''@Clv_Factura_Ini
        '        'rDocument.SetParameterValue(1, "0")
        '        ''@Clv_Factura_Fin
        '        'rDocument.SetParameterValue(2, "0")
        '        ''@Fecha_Ini
        '        'rDocument.SetParameterValue(3, "01/01/1900")
        '        ''@Fecha_Fin
        '        'rDocument.SetParameterValue(4, "01/01/1900")
        '        ''@op
        '        'rDocument.SetParameterValue(5, "0")

        '        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        'End Select

        If eActTickets = True Then
            rDocument.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            rDocument.DataDefinition.FormulaFields("Roxxo").Text = "'" & referenciaOxxo & "'"
        End If

        'rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        rDocument = Nothing

        'Dim ba As Boolean = False
        'If path.Contains("reynosa") = True Then
        '    customersByCityReport = New ReporteCajasTickets_2AG
        'Else
        '    customersByCityReport = New ReporteCajasTickets_2SA

        'End If
        ''Select Case IdSistema
        ''    Case "VA"
        ''        customersByCityReport = New ReporteCajasTickets_2VA
        ''    Case "LO"
        ''        customersByCityReport = New ReporteCajasTickets_2Log
        ''    Case "AG"
        ''        customersByCityReport = New ReporteCajasTickets_2AG
        ''    Case "SA"
        ''        customersByCityReport = New ReporteCajasTickets_2SA
        ''    Case "TO"
        ''        customersByCityReport = New ReporteCajasTickets_2TOM
        ''    Case Else
        ''        customersByCityReport = New ReporteCajasTickets_2OLD
        ''End Select


        'Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        'Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        'Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        'Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable

        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        ''If GloImprimeTickets = False Then
        '' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        ''Else

        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'CON.Close()

        'eActTickets = False
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'DameGralMsjTickets.Connection = CON2
        'DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        'CON2.Close()

        ''If IdSistema = "TO" And facnormal = True And identi > 0 Then
        ''    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        ''    ba = True
        ''ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
        ''    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
        ''    ba = True
        ''ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
        ''    'reportPath = RutaReportes + "\ReporteCajasCosmo.rpt"
        ''    reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
        ''    ba = True
        ''Else
        ''    If IdSistema = "VA" Then
        ''        'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
        ''    Else
        ''        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        ''    End If
        ''End If


        ''End If

        ''customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '' End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        ''If GloImprimeTickets = True Then
        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If



        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub

    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub


    Private Sub FrmImprimir_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LiTipo = 2 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LiTipo = 3 Then
            ConfigureCrystalReportsNota(GLONOTA)
        ElseIf LiTipo = 4 Then
            ConfigureCrystalReportsOrden_NewXml(0, "")
        ElseIf LiTipo = 5 Then
            ConfigureCrystalReportsQueja_NewXml(0, "")
        ElseIf LiTipo = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        ElseIf LiTipo = 7 Then
            Me.ConfigureCrystalReportefacturaGlobal(bec_letra, bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura, 0)
        ElseIf LiTipo = 8 Then

        End If
    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String, ByVal ieps As Double)
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim txtsubtotal As String = Nothing
            Dim txtieps As String = Nothing
            Dim subtotal2 As Double
            Dim iva2 As Double
            Dim myString As String = iva2.ToString("00.00")
            Dim reportPath As String = Nothing

            Select Case Locclv_empresa
                Case "AG"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
                Case "TO"
                    reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
                Case "SA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
                Case "VA"
                    reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            End Select
            customersByCityReport.Load(reportPath)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then
                'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & miLetra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & LocSerieglo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & LocFechaGlo & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & LocCajeroGlo & "'"
                'subtotal2 = CDec(importe2) / 1.15
                'txtsubtotal = subtotal2
                'txtsubtotal = subtotal2.ToString("##0.00")
                'iva2 = CDec(importe2) / 1.15 * 0.15
                'myString = iva2.ToString("##0.00")
                'ElseIf Locclv_empresa = "AG" Then
                '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                '    customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                '    subtotal2 = CDec(importe2) / 1.15
                '    txtsubtotal = subtotal2
                '    txtsubtotal = subtotal2.ToString("##0.00")
                '    iva2 = CDec(importe2) / 1.15 * 0.15
                '    myString = iva2.ToString("##0.00")
            End If

            ieps = 0
            Try
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@Factura", SqlDbType.Int, bec_factura)
                Dim tblImpuestos As DataTable = BaseII.ConsultaDT("uspTraeImpuestosFacGlobal")

                For Each Fila As DataRow In tblImpuestos.Rows
                    txtieps = Format(Fila("IEPS"), "#,##0.00") ' FormatCurrency(Fila("IEPS"), 2, False)
                    myString = Format(Fila("IVA"), "#,##0.00") 'FormatCurrency(Fila("IVA"), 2, False)
                    txtsubtotal = Format(Fila("SubTotal"), "#,##0.00") 'FormatCurrency(Fila("SubTotal"), 2, False)
                    importe2 = Format(Fila("Total"), "#,##0.00") 'FormatCurrency(Fila("Total"), 2, False)
                    'txtieps = FormatCurrency(Fila("IEPS"), 2, False)
                    'myString = FormatCurrency(Fila("IVA"), 2, False)
                    'txtsubtotal = FormatCurrency(Fila("SubTotal"), 2, False)
                    'importe2 = FormatCurrency(Fila("Total"), 2, False)
                Next
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try

            customersByCityReport.DataDefinition.FormulaFields("IEPS").Text = "'" & txtieps.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
            customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"




            'Select Case Locclv_empresa
            '    Case "AG"
            '        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "TO"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "SA"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            '    Case "VA"
            '        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            'End Select


            'customersByCityReport.PrintToPrinter(1, True, 1, 1)

            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Try

        
            Dim ba As Boolean = False
            Select Case IdSistema
                Case "LO"
                    customersByCityReport = New ReporteCajasTickets_2Log

            End Select


            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.BusFacFiscalTableAdapter.Connection = CON
            'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            'CON.Close()
            'CON.Dispose()

            'eActTickets = False
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            'CON2.Close()
            'CON2.Dispose()

            'If IdSistema = "VA" Then
            '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'End If

            'End If

            'customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            ''@Clv_Factura_Ini
            'customersByCityReport.SetParameterValue(1, "0")
            ''@Clv_Factura_Fin
            'customersByCityReport.SetParameterValue(2, "0")
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(3, "01/01/1900")
            ''@Fecha_Fin
            'customersByCityReport.SetParameterValue(4, "01/01/1900")
            ''@op
            'customersByCityReport.SetParameterValue(5, "0")

            'If ba = False Then
            '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            '    If eActTickets = True Then
            '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            '    End If
            'End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


            'customersByCityReport.PrintToPrinter(1, True, 1, 1)





            CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True
            'End If
            'SetDBLogonForReport2(connectionInfo)
            'customersByCityReport.Dispose()
        Catch ex As Exception
            CrystalReportViewer1.Dispose()
            customersByCityReport.Dispose()
            Me.Close()
        End Try
    End Sub
    

    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport1(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, nota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        CrystalReportViewer1.ReportSource = customersByCityReport
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub ConfigureCrystalReportsOrden_NewXml(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            VerificaOrdRetiro()

            If IdSistema = "AG" Then

                If OrdRetiro = 1 Then
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServRetiro.rpt"
                Else
                    reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                End If

            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer1", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0


            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)


            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = 1
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(gloClave)
            cmd.Parameters.Add(parametro10)

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(gloClave)
            cmd.Parameters.Add(parametro11)

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)

            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = 0
            cmd.Parameters.Add(parametro22)

            Dim parametro23 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = 0
            cmd.Parameters.Add(parametro23)


            Dim parametro24 As New SqlParameter("@op6", SqlDbType.SmallInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = 0
            cmd.Parameters.Add(parametro24)

            Dim parametro25 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro25.Direction = ParameterDirection.Input
            parametro25.Value = 0
            cmd.Parameters.Add(parametro25)

            Dim parametro26 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.Int)
            parametro26.Direction = ParameterDirection.Input
            parametro26.Value = 0
            cmd.Parameters.Add(parametro26)

            Dim da As New SqlDataAdapter(cmd)

            Dim ds As New DataSet()
            da.Fill(ds)

            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer"
            ds.Tables(1).TableName = "DameDatosGenerales_2"
            ds.Tables(2).TableName = "Cosulta_OrdRetiro"
            ds.Tables(3).TableName = "TablaDatos"
            ds.Tables(4).TableName = "tabla5Queja"
            ds.Tables(5).TableName = "tblUltOrdQueja"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)
            CrystalReportViewer1.ReportSource = customersByCityReport

            BaseII.limpiaParametros()
            BaseII.Inserta("UspBorrarTabla")

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsOrden(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(gloClave))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(gloClave))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            'mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            'CON.Open()
            'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            'CON.Close()
            'If a = 1 Then
            '    MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
            '    Exit Sub
            'Else
            '    customersByCityReport.PrintOptions.PrinterName = Impresora
            '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'End If
            CrystalReportViewer1.ReportSource = customersByCityReport
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsQueja(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim a As Integer = 0



        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim Impresora_Ordenes As String = Nothing
        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"


        Dim reportPath As String = Nothing

        If IdSistema = "TO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
        ElseIf IdSistema = "AG" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
        ElseIf IdSistema = "SA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
        ElseIf IdSistema = "VA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
        ElseIf IdSistema = "LO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
        End If


        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Clv_TipSer int
        customersByCityReport.SetParameterValue(0, Glo_tipSer)
        ',@op1 smallint
        customersByCityReport.SetParameterValue(1, 1)
        ',@op2 smallint
        customersByCityReport.SetParameterValue(2, 0)
        ',@op3 smallint
        customersByCityReport.SetParameterValue(3, 0)
        ',@op4 smallint,
        customersByCityReport.SetParameterValue(4, 0)
        '@op5 smallint
        customersByCityReport.SetParameterValue(5, 0)
        '@op6 smallint
        customersByCityReport.SetParameterValue(6, 0)
        ',@StatusPen bit
        customersByCityReport.SetParameterValue(7, 0)
        ',@StatusEje bit
        customersByCityReport.SetParameterValue(8, 0)
        ',@StatusVis bit,
        customersByCityReport.SetParameterValue(9, 0)
        '@Clv_OrdenIni bigint
        customersByCityReport.SetParameterValue(10, CInt(gloClave))
        ',@Clv_OrdenFin bigint
        customersByCityReport.SetParameterValue(11, CInt(gloClave))
        ',@Fec1Ini Datetime
        customersByCityReport.SetParameterValue(12, "01/01/1900")
        ',@Fec1Fin Datetime,
        customersByCityReport.SetParameterValue(13, "01/01/1900")
        '@Fec2Ini Datetime
        customersByCityReport.SetParameterValue(14, "01/01/1900")
        ',@Fec2Fin Datetime
        customersByCityReport.SetParameterValue(15, "01/01/1900")
        ',@Clv_Trabajo int
        customersByCityReport.SetParameterValue(16, 0)
        ',@Clv_Colonia int
        customersByCityReport.SetParameterValue(17, 0)
        ',@OpOrden int
        customersByCityReport.SetParameterValue(18, OpOrdenar)
        '@Clv_Departamento
        customersByCityReport.SetParameterValue(19, 0)

        customersByCityReport.SetParameterValue(20, 0)
        '@Clv_Departamento
        customersByCityReport.SetParameterValue(21, 0)

        'Titulos de Reporte
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        mySelectFormula = "Quejas " ' & Me.TextBox2.Text
        customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        'If a = 1 Then
        '    MsgBox("No se ha asignado una Impresora de Quejas")
        '    Exit Sub
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
        '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'End If

        CrystalReportViewer1.ReportSource = customersByCityReport

        CON.Close()
        customersByCityReport = Nothing
        'Catch ex As System.Exception
        'System.Windows.Forms.MessageBox.Show(ex.Message)
        ' End Try
    End Sub
    Private Sub ConfigureCrystalReportsQueja_NewXml(ByVal op As Integer, ByVal Titulo As String)
        'Try
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        Dim Num1 As String = 0, Num2 As String = 0
        Dim nclv_trabajo As String = "0"
        Dim nClv_colonia As String = "0"
        Dim a As Integer = 0



        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim Impresora_Ordenes As String = Nothing
        Dim mySelectFormula As String = Titulo
        Dim OpOrdenar As String = "0"


        Dim reportPath As String = Nothing

        If IdSistema = "TO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
        ElseIf IdSistema = "AG" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
        ElseIf IdSistema = "SA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
        ElseIf IdSistema = "VA" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
        ElseIf IdSistema = "LO" Then
            reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
        End If


        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        Dim cnn As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("ReporteAreaTecnicaQuejas1", cnn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = Glo_tipSer
        cmd.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 1
        cmd.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op2
        cmd.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Op3
        cmd.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Op4
        cmd.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Op5
        cmd.Parameters.Add(parametro6)

        Dim parametro22 As New SqlParameter("@op6", SqlDbType.SmallInt)
        parametro22.Direction = ParameterDirection.Input
        parametro22.Value = Op6
        cmd.Parameters.Add(parametro22)

        Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
        parametro7.Direction = ParameterDirection.Input
        If StatusPen = "1" Then
            parametro7.Value = True
        Else
            parametro7.Value = False
        End If
        cmd.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
        parametro8.Direction = ParameterDirection.Input
        If StatusEje = "1" Then
            parametro8.Value = True
        Else
            parametro8.Value = False
        End If
        cmd.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
        parametro9.Direction = ParameterDirection.Input
        If StatusVis = "1" Then
            parametro9.Value = True
        Else
            parametro9.Value = False
        End If
        cmd.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = CInt(gloClave)
        cmd.Parameters.Add(parametro10)

        Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
        parametro11.Direction = ParameterDirection.Input
        parametro11.Value = CInt(gloClave)
        cmd.Parameters.Add(parametro11)

        Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
        parametro12.Direction = ParameterDirection.Input
        parametro12.Value = Fec1Ini
        cmd.Parameters.Add(parametro12)

        Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
        parametro13.Direction = ParameterDirection.Input
        parametro13.Value = Fec1Fin
        cmd.Parameters.Add(parametro13)

        Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
        parametro14.Direction = ParameterDirection.Input
        parametro14.Value = Fec2Ini
        cmd.Parameters.Add(parametro14)

        Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
        parametro15.Direction = ParameterDirection.Input
        parametro15.Value = Fec2Fin
        cmd.Parameters.Add(parametro15)

        Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
        parametro16.Direction = ParameterDirection.Input
        parametro16.Value = nclv_trabajo
        cmd.Parameters.Add(parametro16)

        Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
        parametro17.Direction = ParameterDirection.Input
        parametro17.Value = nClv_colonia
        cmd.Parameters.Add(parametro17)

        Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
        parametro18.Direction = ParameterDirection.Input
        parametro18.Value = OpOrdenar
        cmd.Parameters.Add(parametro18)


        Dim parametro19 As New SqlParameter("@clv_Depto", SqlDbType.Char)
        parametro19.Direction = ParameterDirection.Input
        parametro19.Value = "0"
        cmd.Parameters.Add(parametro19)

        Dim parametro20 As New SqlParameter("@Op7", SqlDbType.SmallInt)
        parametro20.Direction = ParameterDirection.Input
        parametro20.Value = 0
        cmd.Parameters.Add(parametro20)


        Dim parametro21 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro21.Direction = ParameterDirection.Input
        parametro21.Value = 0
        cmd.Parameters.Add(parametro21)

        Dim da As New SqlDataAdapter(cmd)

        Dim data1 As New DataTable()

        da.Fill(data1)

        Dim ds As New DataSet()


        data1.TableName = "ReporteAreaTecnicaQuejas1"


        ds.Tables.Add(data1)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)


        CrystalReportViewer1.ReportSource = customersByCityReport

    End Sub

    Private Sub ReportesFacturasXsd(ByVal prmClvFactura As Long, ByVal prmClvFacturaIni As Long, ByVal prmClvFacturaFin As Long, ByVal prmFechaIni As Date, _
                                  ByVal prmFechaFin As Date, ByVal prmOp As Integer, ByVal RUTAREP As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("ReportesFacturasXsd", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Factura", prmClvFactura)
        CMD.Parameters.AddWithValue("@Clv_Factura_Ini", prmClvFacturaIni)
        CMD.Parameters.AddWithValue("@Clv_Factura_Fin", prmClvFacturaFin)
        CMD.Parameters.AddWithValue("@Fecha_Ini", prmFechaIni)
        CMD.Parameters.AddWithValue("@Fecha_Fin", prmFechaFin)
        CMD.Parameters.AddWithValue("@op", prmOp)
        Dim DA As New SqlDataAdapter(CMD)

        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "FacturaFiscal"

        customersByCityReport.Load(RUTAREP)
        customersByCityReport.SetDataSource(DS)
    End Sub

    Private Function ReportesFacturas(ByVal Clv_Factura As Long) As DataSet
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ReportesFacturas " + Clv_Factura.ToString() + ",0,0,'19000101','19000101',0")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet

        Try
            dAdapter.Fill(dSet)
            dSet.Tables(0).TableName = "CALLES"
            dSet.Tables(1).TableName = "CIUDADES"
            dSet.Tables(2).TableName = "CLIENTES"
            dSet.Tables(3).TableName = "COLONIAS"
            dSet.Tables(4).TableName = "CatalogoCajas"
            dSet.Tables(5).TableName = "DatosFiscales"
            dSet.Tables(6).TableName = "DetFacturas"
            dSet.Tables(7).TableName = "DetFacturasImpuestos"
            dSet.Tables(8).TableName = "Facturas"
            dSet.Tables(9).TableName = "GeneralDesconexion"
            dSet.Tables(10).TableName = "ReportesFacturas"
            dSet.Tables(11).TableName = "SUCURSALES"
            dSet.Tables(12).TableName = "Usuarios"
            dSet.Tables(13).TableName = "General"
            dSet.Tables(14).TableName = "tblRelSucursalDatosGenerales"
            dSet.Tables(15).TableName = "companias"
            Return dSet
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub VerificaOrdRetiro()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_ordser", SqlDbType.Int, gloClave)
        BaseII.CreateMyParameter("@Retiro", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("VerificaOrdRetiro")
        OrdRetiro = CInt(BaseII.dicoPar("@Retiro").ToString)
    End Sub

End Class
   

