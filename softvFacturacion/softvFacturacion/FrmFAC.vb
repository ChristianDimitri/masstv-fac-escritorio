Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmFAC
    Private customersByCityReport As ReportDocument
    Dim BndError As Integer = 0
    Dim Loc_Clv_Vendedor As Integer = 0
    Dim Loc_Folio As Long = 0
    Dim Loc_Serie As String = Nothing
    Dim Msg As String = Nothing
    Dim bloqueado, identi As Integer
    Dim Bnd As Integer = 0
    Dim CuantasTv As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Dim SiPagos As Integer = 0
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private ePideAparato As Integer = 0
    Private eClv_Detalle As Long = 0
    Dim eDFiscalGlo As Boolean
    Dim EsEcoMes As Boolean
    Dim EsInternet As Integer = 0

    Private Sub Guarda_Tipo_Tarjeta(ByVal Clv_factura As Long, ByVal Tipo As Integer, ByVal Monto As Decimal)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "Nuevo_Rel_Pago_Tarjeta_Factura"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Tipo", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Tipo
                .Parameters.Add(prm1)

                prm = New SqlParameter("@Monto", SqlDbType.Money)
                prm.Direction = ParameterDirection.Input
                prm.Value = Monto
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AgregarServicioAdicionales_COEXP(ByVal eClv_Session As Long, ByVal eClv_Txt As String, ByVal ePrecio As Double)
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "AgregarServicioAdicionales_COEXP"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = eClv_Session
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Clv_Txt", SqlDbType.VarChar, 25)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eClv_Txt
                .Parameters.Add(prm1)

                prm = New SqlParameter("@PRECIO", SqlDbType.Money)
                prm.Direction = ParameterDirection.Input
                prm.Value = ePrecio
                .Parameters.Add(prm)

                Dim ia As Integer = .ExecuteNonQuery()
            End With
            CON100.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Function DIME_SI_YA_GRABE_UNA_FACTURA(ByVal MiContrato As Long) As Integer
        Dim CON100 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()

        Try
            DIME_SI_YA_GRABE_UNA_FACTURA = 0
            SQL = New SqlCommand()
            CON100.Open()
            With SQL
                .CommandText = "DIME_SI_YA_GRABE_UNA_FACTURA"
                .Connection = CON100
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@Clv_factura bigint,@Tipo int,@Monto money
                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = MiContrato
                .Parameters.Add(prm)

                Dim prm2 As New SqlParameter("@Valida", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = MiContrato
                .Parameters.Add(prm2)
                Dim ia As Integer = .ExecuteNonQuery()
                DIME_SI_YA_GRABE_UNA_FACTURA = prm2.Value
            End With
            CON100.Close()
        Catch ex As Exception
            DIME_SI_YA_GRABE_UNA_FACTURA = 0
            If CON100.State <> ConnectionState.Closed Then CON100.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    ' Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '     For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '         myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '     Next
    ' End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        'ConfigureCrystalReports

        Dim ba As Boolean = False
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BusFacFiscalTableAdapter.Connection = CON
        Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        CON.Dispose()
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        If IdSistema = "SA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
            ba = True
        ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
            ba = True
        ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReportesFacturasXsd.rpt"
            ba = True
        ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
            reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
            ba = True
        Else
            If IdSistema = "VA" Then
                'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            Else
                ConfigureCrystalReports_tickets(Clv_Factura)
                Exit Sub
            End If
        End If

        ReportesFacturasXsd(Clv_Factura, 0, 0, "01-01-1900", "01-01-1900", 0, reportPath)

        ''End If

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)


        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

        'If facticket = 1 Then
        '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
        If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

            customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        Else
            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        End If

        If IdSistema = "AG" And facnormal = True And identi > 0 Then
            eCont = 1
            eRes = 0
            Do
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
                '6=Yes;7=No
                If eRes = 6 Then eCont = eCont + 1

            Loop While eCont <= 3
        Else
            If IdSistema = "SA" Then
                MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
                MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            Else
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

        End If

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing

    End Sub



    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dSet As New DataSet
        Dim ruta As String
        Dim referenciaOxxo As String = SP_REFERENCIA_OXXOFactura(Clv_Factura)
        dSet = ReportesFacturas(Clv_Factura)
        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        '    Case "MASSTVREYNOSA"
        '        'ruta = RutaReportes + "\ReporteCajasTickets_2(AG).rpt"
        '        ruta = RutaReportes + "\Report1.rpt"
        '    Case Else
        '        ruta = RutaReportes + "\ReporteCajasTickets.rpt"
        'End Select
        'ruta = RutaReportes + "\ReporteCajasTickets.rpt"

        rDocument.Load(ruta)
        rDocument.SetDataSource(dSet)

        eMsjTickets = ""
        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()


        'Select Case GloCity
        '    Case "MASSTVMIGUELAL"
        '        rDocument.SetDataSource(dSet)
        '    Case "MASSTVREYNOSA"
        '        SetDBReport(dSet, rDocument)
        '        ''@Clv_Factura 
        '        'rDocument.SetParameterValue(0, Clv_Factura)
        '        ''@Clv_Factura_Ini
        '        'rDocument.SetParameterValue(1, "0")
        '        ''@Clv_Factura_Fin
        '        'rDocument.SetParameterValue(2, "0")
        '        ''@Fecha_Ini
        '        'rDocument.SetParameterValue(3, "01/01/1900")
        '        ''@Fecha_Fin
        '        'rDocument.SetParameterValue(4, "01/01/1900")
        '        ''@op
        '        'rDocument.SetParameterValue(5, "0")

        '        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '        rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        'End Select
        If eActTickets = True Then
            rDocument.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            rDocument.DataDefinition.FormulaFields("Roxxo").Text = "'" & referenciaOxxo & "'"
        End If

        rDocument.PrintOptions.PrinterName = LocImpresoraTickets
        rDocument.PrintToPrinter(1, True, 1, 1)
        rDocument.Dispose()

        'Dim ba As Boolean = False
        'If path.Contains("reynosa") = True Then
        '    customersByCityReport = New ReporteCajasTickets_2AG
        'Else
        '    customersByCityReport = New ReporteCajasTickets_2SA

        'End If
        ''Select Case IdSistema
        ''    Case "VA"
        ''        customersByCityReport = New ReporteCajasTickets_2VA
        ''    Case "LO"
        ''        customersByCityReport = New ReporteCajasTickets_2Log
        ''    Case "AG"
        ''        customersByCityReport = New ReporteCajasTickets_2AG
        ''    Case "SA"
        ''        customersByCityReport = New ReporteCajasTickets_2SA
        ''    Case "TO"
        ''        customersByCityReport = New ReporteCajasTickets_2TOM
        ''    Case Else
        ''        customersByCityReport = New ReporteCajasTickets_2OLD
        ''End Select


        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        ''        If GloImprimeTickets = False Then
        ''reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        ''Else
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BusFacFiscalTableAdapter.Connection = CON
        'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
        'CON.Close()
        'CON.Dispose()

        'eActTickets = False
        'Dim CON2 As New SqlConnection(MiConexion)
        'CON2.Open()
        'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        'CON2.Close()
        'CON2.Dispose()

        ''If IdSistema = "VA" Then
        ''    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
        ''    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        ''Else
        ''    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        ''End If

        ''End If

        ''customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ''End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)


        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, Clv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")

        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If

        ''If facticket = 1 Then
        ''    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


        'customersByCityReport.PrintToPrinter(1, True, 1, 1)





        ''CrystalReportViewer1.ReportSource = customersByCityReport

        ''If GloOpFacturas = 3 Then
        ''CrystalReportViewer1.ShowExportButton = False
        ''CrystalReportViewer1.ShowPrintButton = False
        ''CrystalReportViewer1.ShowRefreshButton = False
        ''End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport.Dispose()
    End Sub

    Private Sub borracambiosdeposito(ByVal clv_session As Long, ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        If locbndborracabiosdep = True Then
            locbndborracabiosdep = False
            cmd = New SqlClient.SqlCommand
            con.Open()
            With cmd
                .Connection = con
                .CommandText = "Deshacerdepositocancela"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_session bigint,@contrato bigint
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input

                prm.Value = CLng(clv_session)
                prm1.Value = CLng(contrato)

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con.Close()
            con.Dispose()
        End If
    End Sub
    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim clv_Session1 As Long = 0
        Dim parcial As Integer
        Dim Liperiodo As String
        Try

            If ContratoTextBox.Text.Length > 0 And cbVendedor.Text.Length > 0 Then
                If IsNumeric(ContratoTextBox.Text) = True And IsNumeric(cbVendedor.SelectedValue) = True Then
                    If CInt(ContratoTextBox.Text) > 0 And CInt(cbVendedor.SelectedValue) > 0 Then
                        DAMEtblRelVendedoresSeries(ContratoTextBox.Text, cbVendedor.SelectedValue)
                    End If
                End If
            End If
         
            SiPagos = 0
            Bnd = 0
            CuantasTv = 0
            'gloClv_Session = 0
            'GloContrato = 0
            Me.Panel5.Visible = False
            Me.CMBPanel6.Visible = False
            BndError = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Session.Text) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
                'CON.Close()
            End If
            If IsNumeric(gloClv_Session) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
                'CON.Close()
            End If
            If IsNumeric(clv_Session1) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(clv_Session1, Long)))
                'CON.Close()
            End If

            gloClv_Session = 0
            clv_Session1 = 0
            Me.TextBoxClienteObs.Text = ""
            If IsNumeric(GloContrato) = True Then

                ConRelClienteObs(GloContrato)
                'GloContrato = Me.ContratoTextBox.Text
                'Glocontratosel = Me.ContratoTextBox.Text
                'Dim CON2 As New SqlConnection(MiConexion)
                'CON2.Open()
                Dim comando As SqlClient.SqlCommand

                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, GloContrato, 0)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.NewsoftvDataSet2.BuscaBloqueado, GloContrato, num, num2)
                TxtRefOxxo.Text = SP_REFERENCIA_OXXO(GloContrato)
                'Me.Dime_Si_DatosFiscalesTableAdapter.Connection = CON
                'Me.Dime_Si_DatosFiscalesTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Si_DatosFiscales, GloContrato, parcial)
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Dime_Si_DatosFiscales "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Periodo", SqlDbType.VarChar, 50)
                    Dim prm3 As New SqlParameter("@al_corriente", SqlDbType.VarChar, 100)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm.Value = GloContrato
                    prm1.Value = 0
                    prm2.Value = 0
                    prm3.Value = ""
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    'If IdSistema = "VA" Then
                    .Parameters.Add(prm3)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    parcial = prm1.Value
                    Liperiodo = prm2.Value

                    Me.REDLabel26.Text = prm3.Value
                    Dame_Saldo_Vencido(GloContrato)


                End With

                If parcial > 0 Then
                    eBndDatosFiscales = True
                    Me.REDLabel25.Visible = True
                    Me.REDLabel25.Text = " EL CLIENTE CUENTA CON DATOS FISCALES "
                Else
                    eBndDatosFiscales = False
                    Me.REDLabel25.Visible = False
                End If
                If Liperiodo <> "" Then
                    Me.Label25.Text = Liperiodo
                Else
                    Me.Label25.Text = ""
                End If
                If IdSistema = "VA" And Me.REDLabel26.Text.Trim <> "SN" Then
                    Me.REDLabel26.Visible = True
                ElseIf IdSistema <> "VA" Then
                    Me.REDLabel26.Visible = False
                End If
                'CON2.Close()
                If num = 0 Or num2 = 0 Then
                    CREAARBOL()
                Else
                    bloqueado = 1
                    clibloqueado()
                    MsgBox("�El Cliente " + Me.ContratoTextBox.Text + " ha sido Bloqueado!", MsgBoxStyle.Exclamation, "�Cliente Bloquedo!")
                    Me.ContratoTextBox.Text = 0
                    GloContrato = 0
                    Glocontratosel = 0
                    Me.Clv_Session.Text = 0
                    BUSCACLIENTES(0)
                    Bloque(False)
                    'Glocontratosel = 0
                End If
                If IdSistema = "SA" Then
                    If GloTipo = "V" Then
                        If IsDate(Fecha_Venta.Text) = True Then
                            'Dim CON3 As New SqlConnection(MiConexion)
                            'CON3.Open()
                            Me.Cobra_VentasTableAdapter.Connection = CON
                            Me.Cobra_VentasTableAdapter.Fill(Me.DataSetEdgar.Cobra_Ventas, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg, Fecha_Venta.Value, "V")
                            'CON3.Close()
                        End If
                    Else
                        'Dim CON4 As New SqlConnection(MiConexion)
                        'CON4.Open()
                        Me.CobraTableAdapter.Connection = CON
                        Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                        'CON4.Close()
                    End If
                Else
                    'Dim CON5 As New SqlConnection(MiConexion)
                    'CON5.Open()
                    Me.CobraTableAdapter.Connection = CON
                    Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                    'CON5.Close()
                End If
                Me.Clv_Session.Text = clv_Session1
                gloClv_Session = clv_Session1
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                    Me.Button2.Enabled = False
                ElseIf bloqueado <> 1 Then
                    Me.Bloque(True)
                    bloqueado = 0
                End If
                'Dim CON6 As New SqlConnection(MiConexion)
                'CON6.Open()
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Connection = CON
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Fill(Me.DataSetEdgar.Dime_Si_ProcedePagoParcial, New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)), Bnd, CuantasTv)
                'CON6.Close()
                If Bnd = 1 And CuantasTv > 0 Then
                    CMBPanel6.Visible = True
                End If

                If Loccontratopardep <> CLng(GloContrato) Then
                    Dim cmdarn As New SqlClient.SqlCommand
                    cmdarn = New SqlCommand
                    With cmdarn
                        .Connection = CON
                        .CommandText = "Dime_si_procedepagoparcialdeposito"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        '@clv_session bigint,@contrato bigint,@bnd1 int output,@bnd2 int output
                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@bnd1", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@bnd2", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Output
                        prm3.Direction = ParameterDirection.Output
                        prm.Value = CLng(Clv_Session.Text)
                        prm1.Value = CLng(GloContrato)
                        prm2.Value = 0
                        prm3.Value = 0
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        Dim i As Integer = cmdarn.ExecuteNonQuery()
                        bnd1pardep1 = prm2.Value
                        bnd1pardep2 = prm3.Value
                    End With
                    locclvsessionpardep = CLng(Clv_Session.Text)
                    Loccontratopardep = CLng(GloContrato)
                    If bnd1pardep1 = 1 Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                    If bnd1pardep2 = 1 And bnd2pardep = False Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                End If
            Else
                GloContrato = 0
                'Dim CON7 As New SqlConnection(MiConexion)
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 0)
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(0, Long)), "", 0, Me.Clv_Session.Text, BndError, Msg)
                'CON7.Close()
                Me.Clv_Session.Text = 0
                Me.TreeView1.Nodes.Clear()
                Me.Bloque(False)
                TxtRefOxxo.Text = SP_REFERENCIA_OXXO(GloContrato)
            End If
            CON.Dispose()
            CON.Close()
            DAMETIPOSCLIENTEDAME()
            bloqueado = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON15 As New SqlConnection(MiConexion)
            CON15.Open()

            If IsNumeric(GloContrato) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(GloContrato, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            'CON15.Dispose()
            'CON15.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON15.Dispose()
            CON15.Close()

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown

        Me.Clv_Session.Text = 0
        GloContrato = 0
        BUSCACLIENTES(0)
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
        If e.KeyValue = Keys.Enter Then

            StatusDesc = 0

            uspDimeStatusEco()

            If StatusDesc >= 1 Then
                Dim Resultado = MessageBox.Show("Desea Pasar a EconoPack", "EconoPack", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If Resultado = DialogResult.Yes Then
                    BndEco = True
                    MsgBox("El cliente Pasara a Econopack", MsgBoxStyle.Information, "EconoPack")
                ElseIf Resultado = DialogResult.No Then
                    BndEco = False
                    MsgBox("El cliente Rechazo el servicio Econopack", MsgBoxStyle.Information, "EconoPack")
                End If

            ElseIf StatusDesc = 0 Then
                BndEco = False

            End If

            If bloqueado <> 1 Then

                If (Me.ContratoTextBox.Text) = "" Then
                    LiContrato = 0
                    Me.Label25.Text = ""
                    Me.REDLabel26.Visible = False
                ElseIf IsNumeric(Me.ContratoTextBox.Text) = True Then
                    LiContrato = Me.ContratoTextBox.Text
                    GloContrato = CLng(Me.ContratoTextBox.Text)
                    Dim TABLE As New DataTable
                    TABLE = UspValidarMostrarBoton(Me.ContratoTextBox.Text)
                    If TABLE.Rows(0)(0).ToString = "1" Then
                        ButtonPagoAbono.Enabled = True
                    End If
                    Dim DT As New DataTable
                    DT = UspValidarAbonoACuenta(GloContrato)

                    BUSCACLIENTES(0)

                    If DT.Rows(0)(0).ToString = "1" Then
                        GloBndPagAbnCobBaj = True
                        eBotonGuardar = False
                        Me.Enabled = False
                        FrmDetalleCargoACuenta.ShowDialog()
                        Me.EsEcoMes = True
                        Me.Button12.Enabled = False
                        Dame_Saldo_Vencido(GloContrato)
                    End If
                End If

                Me.Bloque(True)

                If BndEco = True Or Recon >= 1 Then
                    GloBndPagAbnCobBaj = True
                    eBotonGuardar = False
                    GloClvSessionAbono = 0
                    GloImporteTotalEstadoDeCuenta = SumaDetalleDataGridView(4, 2).Value
                    Me.Enabled = False
                    FrmPagoAbonoACuenta.ShowDialog()
                    Me.Button12.Enabled = False

                    Dame_Saldo_Vencido(GloContrato)

                End If

            End If
        End If

        Button13.Enabled = True

    End Sub

    Private Function UspValidarMostrarBoton(ByVal PRMCONTRATO As Long) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, PRMCONTRATO)
            UspValidarMostrarBoton = BaseII.ConsultaDT("UspValidarMostrarBoton")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function UspValidarAbonoACuenta(ByVal PRMCONTRATO As Long) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, PRMCONTRATO)
            UspValidarAbonoACuenta = BaseII.ConsultaDT("UspValidarAbonoACuenta")
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub FrmFacturaci�n_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim valida As Integer = 0
        'Dim MSG As String
        Dim bndtodos As Integer = 0
        'Dim coneLidia As New SqlClient.SqlConnection(MiConexion)
        'Dim Cmd As New SqlClient.SqlCommand
        Try

            If ContratoTextBox.Tag = "1" Then
                ContratoTextBox.Tag = "0"
                Dim CON3 As New SqlConnection(MiConexion)
                CON3.Open()
                Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                CON3.Dispose()
                CON3.Close()
                GLOSIPAGO = 0
                eBotonGuardar = True
                FrmPago.Show()
            ElseIf ContratoTextBox.Tag = "2" Then
                ContratoTextBox.Tag = "0"
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
            End If
            If bnd2pardep1 = True Then
                bnd2pardep1 = False
                Dim CON20 As New SqlConnection(MiConexion)
                CON20.Open()
                Me.DameDetalleTableAdapter.Connection = CON20
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, locclvsessionpardep, 0)
                '  , Loccontratopardep
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON20
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, locclvsessionpardep, False, 0)
                CON20.Dispose()
                CON20.Close()
            End If
            If Glocontratosel > 0 Then
                Me.ContratoTextBox.Text = 0
                Me.ContratoTextBox.Text = Glocontratosel
                GloContrato = Glocontratosel
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                Me.BUSCACLIENTES(0)
            End If
            If GloBnd = True Then
                GloBnd = False
                If Glo_Apli_Pnt_Ade = True Then
                    bndtodos = 1
                    Glo_Apli_Pnt_Ade = False
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.PagosAdelantadosTableAdapter.Connection = CON
                Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Servicio, Long)), New System.Nullable(Of Long)(CType(gloClv_llave, Long)), New System.Nullable(Of Long)(CType(gloClv_UnicaNet, Long)), New System.Nullable(Of Long)(CType(gloClave, Long)), IdSistema, New System.Nullable(Of Integer)(CType(GloAdelantados, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, bndtodos, Me.ContratoTextBox.Text, BndError, Msg)
                CON.Dispose()
                CON.Close()
                Me.Clv_Session.Text = 0
                Me.Clv_Session.Text = gloClv_Session
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBndExt = True Then
                GloBndExt = False
                If GloClv_Txt = "CEXTV" Then
                    gloClv_Session = Me.Clv_Session.Text
                    Dim Error_1 As Long = 0
                    Dim CON8 As New SqlConnection(MiConexion)
                    CON8.Open()
                    Me.DimesiahiConexTableAdapter.Connection = CON8
                    Me.DimesiahiConexTableAdapter.Fill(Me.DataSetEdgar.DimesiahiConex, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), Error_1)
                    If Error_1 = 0 Then
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON8
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(1, Long)), IdSistema, New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(GloExt, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                        bitsist(GloUsuario, LiContrato, GloSistema, Me.Name, "Agregar Servicios", "Se Agrego un Servicio Adicional", "Servicio Agregado: " + CStr(GloClv_Txt) + " Tv Adicionales: " + CStr(GloExt), LocClv_Ciudad)
                    Else
                        MsgBox("Primero cobre la contrataci�n tvs. adicional que ya esta en la lista")
                    End If
                    CON8.Dispose()
                    CON8.Close()
                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = gloClv_Session
                Else
                    gloClv_Session = Me.Clv_Session.Text
                    Dim CON9 As New SqlConnection(MiConexion)
                    CON9.Open()
                    If eBndPPE = True Then
                        eBndPPE = False
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Connection = CON9
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Fill(Me.DataSetEdgar.AgregarServicioAdicionales_PPE, Me.Clv_Session.Text, eClv_Progra, eClv_Txt)
                    ElseIf GloClv_Txt = "COEXP" Or GloClv_Txt = "COEX6" Then
                        AgregarServicioAdicionales_COEXP(Me.Clv_Session.Text, GloClv_Txt, gloImpCoexp)
                    Else
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON9
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(1, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    End If
                    CON9.Dispose()
                    CON9.Close()

                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = gloClv_Session
                End If
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBonif = 1 Then
                GloBonif = 0
                Dim CON10 As New SqlConnection(MiConexion)
                CON10.Open()
                Me.DameDetalleTableAdapter.Connection = CON10
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, gloClv_Session, 0)
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON10
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, gloClv_Session, False, 0)
                CON10.Dispose()
                CON10.Close()
            End If
            'If bndcontt = True Then
            '    bndcontt = False
            '    ConfigureCrystalReportsContratoTomatlan("TO")
            '    valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
            '    If valida = 6 Then
            '        ConfigureCrystalReportsContratoTomatlan2("TO")
            '    ElseIf valida = 7 Then
            '        MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
            '    End If
            '    Me.ContratoTextBox.Text = 0
            'End If

            If GLOSIPAGO = 1 Then 'SI YA PAGO PASA Y SI NO AL CHORIZO
                GLOSIPAGO = 0
                GloClv_Factura = 0


                'CON11.Open()
                'Me.GrabaFacturasTableAdapter.Connection = CON11
                'Me.GrabaFacturasTableAdapter.Fill(Me.NewsoftvDataSet.GrabaFacturas, Me.ContratoTextBox.Text, Me.Clv_Session.Text, GloUsuario, GloSucursal, GloCaja, GloTipo, Loc_Serie, Loc_Folio, Loc_Clv_Vendedor, BndError, Msg, GloClv_Factura)
                ', , , , , 
                ''Inicia
                Try


                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "GrabaFacturas_2"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.ContratoTextBox.Text
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = Me.Clv_Session.Text
                        .Parameters.Add(prm1)

                        Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = GloUsuario
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                        prm3.Direction = ParameterDirection.Input
                        prm3.Value = GloSucursal
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Input
                        prm4.Value = GloCaja
                        .Parameters.Add(prm4)

                        Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                        prm5.Direction = ParameterDirection.Input
                        If Me.SplitContainer1.Panel1Collapsed = False Then
                            GloTipo = "V"
                            prm5.Value = "V"
                        Else
                            GloTipo = "C"
                            prm5.Value = "C"
                        End If

                        .Parameters.Add(prm5)

                        Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                        prm6.Direction = ParameterDirection.Input
                        If Len(Loc_Serie) = 0 Then
                            Loc_Serie = ""
                        End If
                        prm6.Value = Loc_Serie
                        .Parameters.Add(prm6)


                        Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                        prm7.Direction = ParameterDirection.Input
                        If IsNumeric(Loc_Folio) = False Then
                            Loc_Folio = 0
                        End If
                        prm7.Value = Loc_Folio
                        .Parameters.Add(prm7)

                        Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                        prm8.Direction = ParameterDirection.Input
                        prm8.Value = Loc_Clv_Vendedor
                        .Parameters.Add(prm8)

                        Dim prm9 As New SqlParameter("@BndError", SqlDbType.Int)
                        prm9.Direction = ParameterDirection.Input
                        prm9.Value = BndError
                        .Parameters.Add(prm9)

                        Dim prm10 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                        prm10.Direction = ParameterDirection.Input
                        prm10.Value = Msg
                        .Parameters.Add(prm10)

                        Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                        prm17.Direction = ParameterDirection.Output
                        prm17.Value = 0
                        .Parameters.Add(prm17)
                        '--1                    
                        Dim prm18 As New SqlParameter("@1Tipo", SqlDbType.Int)
                        prm18.Direction = ParameterDirection.Input
                        If IsNumeric(GloTipoTarjeta) = True Then
                            prm18.Value = GloTipoTarjeta
                        Else
                            prm18.Value = 0
                        End If
                        .Parameters.Add(prm18)

                        Dim prm19 As New SqlParameter("@1Monto", SqlDbType.Money)
                        prm19.Direction = ParameterDirection.Input
                        If IsNumeric(GLOTARJETA) = True Then
                            prm19.Value = GLOTARJETA
                        Else
                            prm19.Value = 0
                        End If
                        .Parameters.Add(prm19)
                        '--2
                        Dim prm22 As New SqlParameter("@2GLOEFECTIVO", SqlDbType.Money)
                        prm22.Direction = ParameterDirection.Input
                        If IsNumeric(GLOEFECTIVO) = True Then
                            prm22.Value = GLOEFECTIVO
                        Else
                            prm22.Value = 0
                        End If
                        .Parameters.Add(prm22)

                        Dim prm23 As New SqlParameter("@2GLOCHEQUE", SqlDbType.Money)
                        prm23.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCHEQUE) = True Then
                            prm23.Value = GLOCHEQUE
                        Else
                            prm23.Value = 0
                        End If

                        .Parameters.Add(prm23)

                        Dim prm24 As New SqlParameter("@2GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                        prm24.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCLV_BANCOCHEQUE) = True Then
                            prm24.Value = GLOCLV_BANCOCHEQUE
                        Else
                            prm24.Value = 0
                        End If
                        .Parameters.Add(prm24)

                        Dim prm25 As New SqlParameter("@2NUMEROCHEQUE", SqlDbType.VarChar, 50)
                        prm25.Direction = ParameterDirection.Input
                        If Len(NUMEROCHEQUE) > 0 Then
                            prm25.Value = NUMEROCHEQUE
                        Else
                            prm25.Value = ""
                        End If
                        .Parameters.Add(prm25)

                        Dim prm26 As New SqlParameter("@2GLOTARJETA", SqlDbType.Money)
                        prm26.Direction = ParameterDirection.Input
                        If IsNumeric(GLOTARJETA) = True Then
                            prm26.Value = GLOTARJETA
                        Else
                            prm26.Value = 0
                        End If
                        .Parameters.Add(prm26)

                        Dim prm27 As New SqlParameter("@2GLOCLV_BANCOTARJETA", SqlDbType.Int)
                        prm27.Direction = ParameterDirection.Input
                        If IsNumeric(GLOCLV_BANCOTARJETA) = True Then
                            prm27.Value = GLOCLV_BANCOTARJETA
                        Else
                            prm27.Value = 0
                        End If
                        .Parameters.Add(prm27)

                        Dim prm28 As New SqlParameter("@2NUMEROTARJETA", SqlDbType.VarChar, 50)
                        prm28.Direction = ParameterDirection.Input
                        If Len(NUMEROTARJETA) > 0 Then
                            prm28.Value = NUMEROTARJETA
                        Else
                            prm28.Value = 0
                        End If
                        .Parameters.Add(prm28)

                        Dim prm29 As New SqlParameter("@2TARJETAAUTORIZACION", SqlDbType.VarChar, 50)
                        prm29.Direction = ParameterDirection.Input
                        prm29.Value = TARJETAAUTORIZACION
                        .Parameters.Add(prm29)
                        '--3
                        Dim prm32 As New SqlParameter("@3CLV_Nota", SqlDbType.BigInt)
                        prm32.Direction = ParameterDirection.Input
                        prm32.Value = GLOCLV_NOTA
                        .Parameters.Add(prm32)

                        Dim prm33 As New SqlParameter("@3GLONOTA", SqlDbType.Money)
                        prm33.Direction = ParameterDirection.Input
                        prm33.Value = GLONOTA
                        .Parameters.Add(prm33)


                        Dim i As Integer = comando.ExecuteNonQuery()
                        GloClv_Factura = prm17.Value

                    End With
                    CON.Close()
                Catch ex As Exception
                    GloClv_Factura = 0
                    MsgBox(ex.Message)
                Finally
                    Button2.Enabled = True
                    Me.Clv_Session.Text = 0
                End Try

                If GLOTRANSFERENCIA > 0 Then
                    GUARDAPagoTransferencia(GloClv_Factura, GLOTRANSFERENCIA, GLOBANCO, GLONUMEROTRANSFERENCIA, GLOAUTORIZACION)
                End If

                If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
                If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
                If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
                If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
                If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
                If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
                If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
                If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""


                If Me.EsEcoMes = True Or Recon = 1 Or BndEco = True Then
                    UspGuardaTblPagoAbonoACuenta()
                End If
                'Dim CON2 As New SqlConnection(MiConexion)

                'Guarda_Tipo_Tarjeta(GloClv_Factura, GloTipoTarjeta, GLOTARJETA)

                'CON.Open()

                'Dim comando2 As SqlClient.SqlCommand
                'comando2 = New SqlClient.SqlCommand
                'With comando2
                '    .Connection = CON
                '    .CommandText = "GUARDATIPOPAGO "
                '    .CommandType = CommandType.StoredProcedure
                '    .CommandTimeout = 0

                '    Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '    prm.Direction = ParameterDirection.Input
                '    prm.Value = GloClv_Factura
                '    .Parameters.Add(prm)

                '    Dim prm2 As New SqlParameter("@GLOEFECTIVO", SqlDbType.Money)
                '    prm2.Direction = ParameterDirection.Input
                '    prm2.Value = GLOEFECTIVO
                '    .Parameters.Add(prm2)

                '    Dim prm3 As New SqlParameter("@GLOCHEQUE", SqlDbType.Money)
                '    prm3.Direction = ParameterDirection.Input
                '    prm3.Value = GLOCHEQUE
                '    .Parameters.Add(prm3)

                '    Dim prm4 As New SqlParameter("@GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                '    prm4.Direction = ParameterDirection.Input
                '    prm4.Value = GLOCLV_BANCOCHEQUE
                '    .Parameters.Add(prm4)

                '    Dim prm5 As New SqlParameter("@NUMEROCHEQUE", SqlDbType.VarChar)
                '    prm5.Direction = ParameterDirection.Input
                '    prm5.Value = NUMEROCHEQUE
                '    .Parameters.Add(prm5)

                '    Dim prm6 As New SqlParameter("@GLOTARJETA", SqlDbType.Money)
                '    prm6.Direction = ParameterDirection.Input
                '    prm6.Value = GLOTARJETA
                '    .Parameters.Add(prm6)

                '    Dim prm7 As New SqlParameter("@GLOCLV_BANCOTARJETA", SqlDbType.Int)
                '    prm7.Direction = ParameterDirection.Input
                '    prm7.Value = GLOCLV_BANCOTARJETA
                '    .Parameters.Add(prm7)

                '    Dim prm8 As New SqlParameter("@NUMEROTARJETA", SqlDbType.VarChar)
                '    prm8.Direction = ParameterDirection.Input
                '    prm8.Value = NUMEROTARJETA
                '    .Parameters.Add(prm8)

                '    Dim prm9 As New SqlParameter("@TARJETAAUTORIZACION", SqlDbType.VarChar)
                '    prm9.Direction = ParameterDirection.Input
                '    prm9.Value = TARJETAAUTORIZACION
                '    .Parameters.Add(prm9)

                '    Dim j As Integer = comando2.ExecuteNonQuery()

                'End With
                'CON.Close()
                ''Inica Guarda Si el Tipo de Pago fue con una Nota de Credito
                'Dim CON3 As New SqlConnection(MiConexion)
                'If GLOCLV_NOTA > 0 And GloClv_Factura > 0 And GLONOTA > 0 Then
                '    CON.Open()

                '    Dim comando3 As SqlClient.SqlCommand
                '    comando3 = New SqlClient.SqlCommand
                '    With comando3
                '        .Connection = CON
                '        .CommandText = "GUARDATIPOPAGO_Nota_Credito "
                '        .CommandType = CommandType.StoredProcedure
                '        .CommandTimeout = 0

                '        Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                '        prm.Direction = ParameterDirection.Input
                '        prm.Value = GloClv_Factura
                '        .Parameters.Add(prm)

                '        Dim prm2 As New SqlParameter("@CLV_Nota", SqlDbType.BigInt)
                '        prm2.Direction = ParameterDirection.Input
                '        prm2.Value = GLOCLV_NOTA
                '        .Parameters.Add(prm2)

                '        Dim prm3 As New SqlParameter("@GLONOTA", SqlDbType.Money)
                '        prm3.Direction = ParameterDirection.Input
                '        prm3.Value = GLONOTA
                '        .Parameters.Add(prm3)

                '        Dim j As Integer = comando3.ExecuteNonQuery()

                '    End With
                '    CON.Close()
                'End If

                'Fin Guarda si el tipo de Pago es con Nota de Credito


                ''Termina
                'Me.GUARDATIPOPAGOTableAdapter.Connection = CON11
                'Me.GUARDATIPOPAGOTableAdapter.Fill(Me.NewsoftvDataSet1.GUARDATIPOPAGO, GloClv_Factura, GLOEFECTIVO, GLOCHEQUE, GLOCLV_BANCOCHEQUE, NUMEROCHEQUE, GLOTARJETA, GLOCLV_BANCOTARJETA, NUMEROTARJETA, TARJETAAUTORIZACION)

                '--------FACTUTAS MEMBRETADAS
                If eBndMembretadas = True Then
                    eBndMembretadas = False
                    NueRelFacturasMembretadas(gloClv_Session, GloClv_Factura)
                End If


                Dim CON11 As New SqlConnection(MiConexion)
                CON11.Open()
                Me.Dime_ContratacionTableAdapter.Connection = CON11
                Me.Dime_ContratacionTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Contratacion, GloClv_Factura, res)

                'If eMotivoBonificacion.Length > 0 Then
                '    Me.GuardaMotivosBonificacionTableAdapter.Connection = CON11
                '    Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, GloClv_Factura, eMotivoBonificacion)
                'End If




                'graba si hay bonificacion
                If locBndBon1 = True Then
                    If GloClv_Factura > 0 Then
                        Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON11
                        Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
                        bitsist(GloUsuario, Me.ContratoTextBox.Text, GloSistema, Me.Name, "Se Realizo Una Bonificaci�n", "", "Factura:" + CStr(GloClv_Factura), SubCiudad)
                    End If
                End If

                'CON11.Dispose()
                CON11.Close()
                'MsgBox(Msg)


                '   INICIO FACTURACION DIGITAL EDGAR 4/FEB/2012

                identi = 0
                identi = BusFacFiscalOledb(GloClv_Factura)
                'fin Facturacion Digital
                locID_Compania_Mizart = ""
                locID_Sucursal_Mizart = ""
                'Fin Guarda si el tipo de Pago es con Nota de Credito
                ''FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                '   FIN FACTURACION DIGITAL EDGAR 4/FEB/2012

                If CInt(identi) > 0 Then
                    ''SAUL IMPRIMIR FACTURA FISCAL
                    ''LiTipo = 2
                    ''GloOpFacturas = 1
                    ''FrmImprimir.ShowDialog()
                    ''SAUL IMPRIMIR FACTURA FISCAL(FIN)

                    ' ''Inicio Facturacion Digital
                    'Locop = 0
                    'Dime_Aque_Compania_Facturarle(GloClv_Factura)
                    'Graba_Factura_Digital(GloClv_Factura, identi)
                    ' ''FrmImprimirFacturaDigital.Show()
                    ''FormPruebaDigital.Show()
                    'locID_Compania_Mizart = ""
                    'locID_Sucursal_Mizart = ""
                    ' ''fin Facturacion Digital
                    Locop = 0
                    Dime_Aque_Compania_Facturarle(GloClv_Factura)
                    Graba_Factura_Digital(GloClv_Factura, identi) ', MiConexion
                    'FrmImprimirFacturaDigital.Show()
                    Try
                        FormPruebaDigital.Show()
                    Catch ex As Exception

                    End Try

                    Dim r As New Globalization.CultureInfo("es-MX")
                    r.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy"
                    System.Threading.Thread.CurrentThread.CurrentCulture = r

                    locID_Compania_Mizart = ""
                    locID_Sucursal_Mizart = ""
                Else
                    If LocImpresoraTickets = "" Then
                        MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                        GLOSIPAGO = 0
                        Me.ContratoTextBox.Text = 0
                        GloContrato = 0
                        Glocontratosel = 0
                        Me.Clv_Session.Text = 0


                        BUSCACLIENTES(0)
                        ButtonPagoAbono.Enabled = False
                    Else
                        'Genera_Factura_Digital(GloClv_Factura)
                        'GeneraDocumentoTxt_DIGITAL(GloClv_Factura, "\\192.168.123.11\exes\softv\FacturaDig")
                        ConfigureCrystalReports(GloClv_Factura)
                    End If
                End If

                If checaSiImprimeOrden() = True Then
                    Mana_ImprimirOrdenes(GloClv_Factura)
                End If
                'If GloTipo = "V" Then
                '    Me.Dame_UltimoFolio()
                'End If

                'FrmImprimir.Show()
                If res = 1 Then
                    If LocNomImpresora_Contratos = "" Then
                        MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
                        Me.ContratoTextBox.Text = 0
                        GloContrato = 0
                        Glocontratosel = 0
                        Me.Clv_Session.Text = 0
                        BUSCACLIENTES(0)
                    Else
                        If IdSistema = "TO" Then
                            '' FrmHorasInst.Show()
                            horaini = "0"
                            horafin = "0"
                            bndcontt = True
                            If bndcontt = True Then
                                bndcontt = False
                                ConfigureCrystalReportsContratoTomatlan("TO")
                                valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
                                If valida = 6 Then
                                    ConfigureCrystalReportsContratoTomatlan2("TO")
                                ElseIf valida = 7 Then
                                    MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
                                End If
                                Me.ContratoTextBox.Text = 0
                                GloContrato = 0
                                Glocontratosel = 0
                                Me.Clv_Session.Text = 0
                                BUSCACLIENTES(0)
                                ButtonPagoAbono.Enabled = False
                            End If
                        End If
                    End If
                End If
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                If GloTipo = "V" Then
                    cbVendedor.SelectedIndex = 0
                End If
                SerieTextBox.Text = ""
                FolioDisponible(String.Empty)
                cmbFolio.Text = ""
                BUSCACLIENTES(0)

                'Proceso para Checar el Monto de la Cajera ===========
                'coneLidia.Open()
                'With Cmd
                '    .CommandText = "Verifica_Monto"
                '    .CommandTimeout = 0
                '    .CommandType = CommandType.StoredProcedure
                '    .Connection = coneLidia
                '    Dim prm As New SqlParameter("@Cajera", SqlDbType.VarChar)
                '    Dim prm2 As New SqlParameter("@Alerta", SqlDbType.Int)
                '    prm.Direction = ParameterDirection.Input
                '    prm2.Direction = ParameterDirection.Output
                '    prm.Value = GloUsuario
                '    prm2.Value = 0
                '    .Parameters.Add(prm)
                '    .Parameters.Add(prm2)
                '    Dim i As Integer = Cmd.ExecuteNonQuery
                '    Verifica = prm2.Value
                'End With
                'If Verifica = 5 Then
                '    BndAlerta = True
                'Else
                '    BndAlerta = False
                'End If
                'Me.Enabled = True




            ElseIf GLOSIPAGO = 2 Then 'CANCELO EL PAGO
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
                ButtonPagoAbono.Enabled = False
                'Me.Enabled = True
            End If

            If GloBndPagAbnCobBaj = True Then
                GloBndPagAbnCobBaj = False
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DameDetalleTableAdapter.Connection = CON
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, locclvsessionpardep, 0)
                '  , Loccontratopardep
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, locclvsessionpardep, False, 0)
                ButtonPagoAbono.Enabled = False
                Button7.Enabled = False
                If Me.Enabled = False Then
                    Me.Enabled = True
                End If
                CON.Dispose()
                CON.Close()
            End If

            If BNDLIMPIAR = True Then
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
                BNDLIMPIAR = False
                ButtonPagoAbono.Enabled = False
                If Me.Enabled = False Then
                    Me.Enabled = True
                End If
            End If

            'If GloBndPagAbnCobBaj = True Then
            '    Me.Button12.Enabled = False
            'End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            GLOSIPAGO = 0
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
            ButtonPagoAbono.Enabled = False
        End Try
        If eBotonGuardar = True Then
            Bloque(False)
        End If
    End Sub

    Private Sub UspGuardaTblPagoAbonoACuenta()
        If BndEco = True Then
            TipoDoB = "D"
        ElseIf Recon = 1 Then
            TipoDoB = "B"
        Else
            TipoDoB = ""
        End If

        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, gloClv_Session)
            BaseII.CreateMyParameter("@CLVSESSIONABONO", SqlDbType.BigInt, GloClvSessionAbono)
            BaseII.CreateMyParameter("@IMPORTE", SqlDbType.Decimal, CDec(0)) 'SumaDetalleDataGridView(4, 2).Value
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Me.ContratoTextBox.Text)
            BaseII.CreateMyParameter("@CLVFACTURA", SqlDbType.BigInt, GloClv_Factura)
            BaseII.CreateMyParameter("@Meses", SqlDbType.Int, EcoMeses)
            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, TipoDoB)
            BaseII.Inserta("UspGuardaTblPagoAbonoACuenta")

            GloClvSessionAbono = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            'Dim horaini As String = nothing
            'Dim horafin As String = nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalaci�n Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            ' customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Hora_insTableAdapter.Connection = CON
            Me.Hora_insTableAdapter.Fill(Me.NewsoftvDataSet2.Hora_ins, horaini, horafin, GloContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.NewsoftvDataSet2.Inserta_Comentario2, GloContrato)
            CON.Dispose()
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)

            'customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub FrmFacturaci�n_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If IsNumeric(Me.Clv_Session.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            CON.Dispose()
            CON.Close()
        End If
    End Sub



    Private Sub FrmFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)
        CNOLblSaldoVencido.Text = ""
        CNOLblSaldoVencido.Visible = False
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEdgar.MUESTRAVENDEDORES_2' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.Ultimo_SERIEYFOLIO' Puede moverla o quitarla seg�n sea necesario.
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraVendedores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraPromotores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.DameDatosGenerales' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = CON
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)
        CON.Dispose()
        CON.Close()
        Me.REDLabel25.Visible = False
        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString
        If GloTipo = "V" Then
            Me.SplitContainer1.Panel1Collapsed = False
            'Dim CON1 As New SqlConnection(MiConexion)
            'CON1.Open()
            'Me.MUESTRAVENDEDORES_2TableAdapter.Connection = CON1
            'Me.MUESTRAVENDEDORES_2TableAdapter.Fill(Me.DataSetEdgar.MUESTRAVENDEDORES_2)
            'CON1.Dispose()
            'CON1.Close()
            'Me.ComboBox1.Text = ""
            'Me.ComboBox1.SelectedValue = 0
            MUESTRAVendedoresSeries()

        Else
            Me.SplitContainer1.Panel1Collapsed = True
            'Me.ComboBox1.TabStop = False
            'Me.ComboBox2.TabStop = False
            cbVendedor.TabStop = False
        End If
        Label22.Visible = False
        Fecha_Venta.Visible = False
        If IdSistema = "SA" Then
            Me.Button12.Visible = True
            Me.Button9.Visible = False
            If GloTipo = "V" Then
                Label22.Visible = True
                Fecha_Venta.Visible = True
            End If
        End If
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON2
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.NewsoftvDataSet2.Selecciona_Impresora_Sucursal, GloSucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        CON2.Dispose()
        CON2.Close()
        Bloque(False)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Glocontratosel = 0
        GloContrato = 0
        eBotonGuardar = False
        'If IdSistema = "VA" Or IdSistema = "LO" Then
        '    FrmSelCliente2.Show()
        'ElseIf IdSistema <> "VA" And IdSistema <> "LO" Then
        FrmSelCliente.Show()
        'End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If locbndborracabiosdep = True Then
        '    'locbndborracabiosdep = False
        '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
        'End If
        Me.Close()
    End Sub


    Private Sub Clv_Session_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Session.TextChanged
        If IsNumeric(Me.Clv_Session.Text) = True Then
            Me.Panel5.Visible = False
            locBndBon1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
            CON.Dispose()
            CON.Close()
            ''CREAARBOL1()
        End If
    End Sub


    Public Sub Dame_Saldo_Vencido(ByVal eContrato As Long)

        Dim CONE As New SqlConnection(MiConexion)
        Try
            'GloProcesa = 3
            CNOLblSaldoVencido.Text = ""
            If eContrato > 0 Then
                CNOLblSaldoVencido.Text = ""
                CNOLblSaldoVencido.Visible = True
                Dim I As Integer = 0
                Dim m1 As String = Nothing
                Dim m2 As Double
                Dim m3 As String = Nothing
                Dim m4 As String = Nothing
                Dim m5 As String = Nothing

                Dim a1 As String = Nothing
                Dim a2 As Double
                Dim a3 As String = Nothing
                Dim a4 As String = Nothing
                Dim a5 As String = Nothing

                '
                Dim Cont As Integer
                Cont = 0
                CONE.Open()
                Dim comando As SqlClient.SqlCommand
                Dim reader As SqlDataReader
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC Dame_SaldoVencido_SaldoActual " & eContrato
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            m1 = CStr(reader.GetValue(0))
                            m2 = reader.GetValue(1)
                            m3 = CStr(reader.GetValue(2))
                            m4 = CStr(reader.GetValue(3))
                            m5 = reader.GetValue(4)
                            a1 = CStr(reader.GetValue(5))
                            a2 = reader.GetValue(6)
                            a3 = CStr(reader.GetValue(7))
                            a4 = CStr(reader.GetValue(8))
                            a5 = reader.GetValue(9)
                        End While
                    End Using
                End With
                CNOLblSaldoVencido.Text = m1 + CStr(FormatCurrency(m2, 2, TriState.True)) + m3 + m4 + CStr(m5) + " " + a1 + CStr(FormatCurrency(a2, 2, TriState.True)) + a3 + a4 + CStr(a5)
            Else
                CNOLblSaldoVencido.Text = ""
            End If


        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & Referencia_Servicio & " son Invalidos")
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            CONE.Close()
        End Try

    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 7 Then
            If SiPagos = 1 Then
                MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
                Exit Sub
            End If
            If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
                If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Servicio = DataGridView1.SelectedCells(1).Value
                    gloClv_llave = DataGridView1.SelectedCells(2).Value
                    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                    gloClave = DataGridView1.SelectedCells(4).Value
                    Dim ERROR_1 As Integer = 0
                    Dim MSGERROR_1 As String = Nothing
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Connection = CON
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
                    CON.Dispose()
                    CON.Close()
                    If ERROR_1 = 0 Then
                        My.Forms.FrmPagosAdelantados.Show()
                    ElseIf ERROR_1 = 2 Then
                        MsgBox(MSGERROR_1)
                    End If
                ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
                    GloClv_Txt = "CEXTV"
                    My.Forms.FrmExtecionesTv.Show()
                End If
            ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmDetCobroDesc.Show()
            ElseIf DataGridView1.SelectedCells(7).Value = "Ver el Detalle" Then
                gloClv_Session = DataGridView1.SelectedCells(0).Value
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmVerDetalledelCobro.Show()
            ElseIf DataGridView1.SelectedCells(7).Value = "Adelantar Parcialidades" Then 'ESTO LO PUSO JUANJO PARA ADELANTAR PARCIALIDADES
                frmAdelantaParcialidades.NumMaximo = CobroParcialMaterial.spDamePagosRestantes(CInt(DataGridView1.SelectedCells(29).Value))
                frmAdelantaParcialidades.DetClaveFactura = CInt(DataGridView1.SelectedCells(29).Value)
                frmAdelantaParcialidades.ShowDialog()
                'ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                '    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                '    FrmDetCobroDesc.Show()
                'Else
                '    MsgBox("No se pueden Adelantar pagos de Telefon�a. Esto hasta que se genere su primer estado de Cuenta", MsgBoxStyle.Information)
                'ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                '    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                '    FrmDetCobroDesc.Show()
                'Else
                '    MsgBox("No se pueden Adelantar pagos de Telefon�a. Esto hasta que se genere su primer estado de Cuenta", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub clibloqueado()
        Me.Button7.Enabled = False
        Me.Button8.Enabled = False
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button5.Enabled = False
        Me.NOMBRELabel1.Text = ""
        Me.CALLELabel1.Text = ""
        Me.NUMEROLabel1.Text = ""
        Me.COLONIALabel1.Text = ""
        Me.CIUDADLabel1.Text = ""
    End Sub

    Private Sub Bloque(ByVal bnd As Boolean)
        Me.Button5.Enabled = bnd
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                Me.Button5.Enabled = True
            End If
        End If
        'Me.Button6.Enabled = bnd
        Me.Button7.Enabled = bnd
        Me.Button8.Enabled = bnd
        Me.Button2.Enabled = bnd
        Me.Button6.Enabled = bnd

        If Me.DataGridView1.RowCount = 0 Then
            Me.Button7.Enabled = False
            Me.Button6.Enabled = False
            Me.Button2.Enabled = False
            Me.Button8.Enabled = False
            Me.Button5.Enabled = False
        Else
            Me.Button7.Enabled = True
            Me.Button6.Enabled = True
            Me.Button2.Enabled = True
            Me.Button8.Enabled = True
            Me.Button5.Enabled = True
        End If


        'If Me.ContratoTextBox.Text.Length = 0 Then
        '    Me.ContratoTextBox.Text = 0
        'End If
1:
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameServicioAsignadoTableAdapter.Connection = CON2
        Me.DameServicioAsignadoTableAdapter.Fill(Me.EricDataSet.DameServicioAsignado, GloContrato, eRes)
        CON2.Dispose()
        CON2.Close()
        If eRes = 1 Then
            eRes = 0
            Me.Button9.Enabled = True
        Else
            eRes = 0
            Me.Button9.Enabled = False
        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        gloClv_Session = Me.Clv_Session.Text
        eBotonGuardar = False
        FrmServicios.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ContratoTextBox.Text = 0
        GloContrato = 0
        Glocontratosel = 0
        Me.Clv_Session.Text = 0
        BUSCACLIENTES(0)
        Bloque(False)
        SerieTextBox.Text = ""
        FolioDisponible(String.Empty)
        cmbFolio.Text = ""
        If GloTipo = "V" Then
            cbVendedor.SelectedIndex = 0
        End If

    End Sub


    Public Sub BotonGrabar()
        Try
            If IsNumeric(Me.ContratoTextBox.Text) = True And IsNumeric(Me.Clv_Session.Text) = True Then
                If Me.ContratoTextBox.Text > 0 And Me.Clv_Session.Text > 0 Then
                    If GloTipo = "V" Then

                        If cbVendedor.Text.Length = 0 Then
                            MessageBox.Show("Selecciona el Vendedor.")
                            Button2.Enabled = True
                            Exit Sub
                        End If

                        If CInt(cbVendedor.SelectedValue) = 0 Then
                            MessageBox.Show("Selecciona el Vendedor.")
                            Exit Sub
                        End If

                        If eMsj.Length > 0 Then
                            MessageBox.Show(eMsj)
                            Button2.Enabled = True
                            Exit Sub
                        End If

                        Loc_Clv_Vendedor = cbVendedor.SelectedValue
                        Loc_Serie = SerieTextBox.Text
                        Loc_Folio = FolioTextBox.Text

                    ElseIf GloTipo = "C" Then
                        VALIDASucursalesSeries(ContratoTextBox.Text, GloSucursal)

                        If eMsj.Length > 0 Then
                            MessageBox.Show(eMsj)
                            Button2.Enabled = True
                            Exit Sub
                        End If

                    End If

                    If Me.DIME_SI_YA_GRABE_UNA_FACTURA(Me.ContratoTextBox.Text) = 0 Then
                        Dim CON3 As New SqlConnection(MiConexion)
                        CON3.Open()
                        Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                        Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                        CON3.Dispose()
                        CON3.Close()
                        GLOSIPAGO = 0
                        eBotonGuardar = True
                        FrmPago.Show()
                    Else
                        Me.ContratoTextBox.Tag = "0"
                        DialogTiene_Facturas.Show()
                    End If
                    'Me.Enabled = False
                Else
                    MsgBox("Seleccione un Cliente para Facturar ", MsgBoxStyle.Information)
                    Button2.Enabled = True
                    eBotonGuardar = False
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("No hay Servicios o Conceptos que Facturar")
            Button2.Enabled = True
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
            eBotonGuardar = True
        End Try
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BUSCACLIENTES(0)
    End Sub

    'Private Sub Clv_Vendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    'If IsNumeric(Me.Clv_Vendedor.SelectedValue) = True And Len(Trim(Me.Clv_Vendedor.Text)) > 0 Then
    ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, Me.Clv_Vendedor.SelectedValue)
    ' Me.ComboBox2.Text = ""
    ' Else
    ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, 0)
    'End If
    'End Sub



    'Private Sub ComboBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(ComboBox2.Text)) Then
    '        Dame_UltimoFolio()
    '    Else
    '        Me.FolioTextBox.Text = ""
    '    End If
    'End Sub

    'Private Sub Dame_UltimoFolio()
    '    Try
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.FolioTextBox.Text = 0
    '        Me.DAMEUltimo_FOLIOTableAdapter.Connection = CON
    '        Me.DAMEUltimo_FOLIOTableAdapter.Fill(Me.DataSetEdgar.DAMEUltimo_FOLIO, Me.ComboBox1.SelectedValue, Me.ComboBox2.Text, Me.FolioTextBox.Text)
    '        CON.Dispose()
    '        CON.Close()
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    'Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
    '    If e.KeyCode = Keys.F10 Then
    '        If eBndDatosFiscales = True Then
    '            eClv_Session = Clv_Session.Text
    '            FrmMembretada.Show()
    '        Else
    '            BotonGrabar()
    '        End If
    '    End If
    'End Sub
    'Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
    '    Dim CON As New SqlConnection(MiConexion)
    '    CON.Open()
    '    If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
    '        Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
    '        Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, Me.ComboBox1.SelectedValue)
    '        Me.ComboBox2.Text = ""
    '    Else
    '        Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
    '        Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, 0)
    '    End If
    '    CON.Dispose()
    '    CON.Close()
    'End Sub

    Private Sub cbVendedor_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbVendedor.KeyDown
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
    End Sub

    Private Sub cbVendedor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbVendedor.TextChanged
        'If ContratoTextBox.Text.Length > 0 And cbVendedor.Text.Length > 0 Then
        '    If IsNumeric(ContratoTextBox.Text) = True And IsNumeric(cbVendedor.SelectedValue) = True Then
        '        If CInt(ContratoTextBox.Text) > 0 And CInt(cbVendedor.SelectedValue) > 0 Then
        DAMEtblRelVendedoresSeries(ContratoTextBox.Text, cbVendedor.SelectedValue)
        If Len(SerieTextBox.Text) > 0 Then
            FolioDisponible(SerieTextBox.Text)
        End If

        '        End If
        '    End If
        'End If
    End Sub

    Private Sub FolioDisponible(ByVal PRMSERIE As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@SERIE", SqlDbType.VarChar, PRMSERIE, 8)
            cmbFolio.DataSource = BaseII.ConsultaDT("Folio_Disponible")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub ComboBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox2.KeyDown
    '    If e.KeyCode = Keys.F10 Then
    '        If eBndDatosFiscales = True Then
    '            eClv_Session = Clv_Session.Text
    '            FrmMembretada.Show()
    '        Else
    '            BotonGrabar()
    '        End If
    '    End If
    'End Sub

    'Private Sub ComboBox2_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
    '    Me.FolioTextBox.Text = ""
    '    If Len(Trim(ComboBox2.Text)) Then
    '        Dame_UltimoFolio()
    '    End If
    'End Sub

    'Private Sub ComboBox2_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.TextChanged
    '    Me.FolioTextBox.Text = ""
    'End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'If IsNumeric(GloContrato) = True And GloContrato > 0 Then
        If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
            'GloOpFacturas = 3
            'eBotonGuardar = False
            'BrwFacturas_Cancelar.Show()
            FrmSeleccionaTipo.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim LOCCLAVE As Integer = 0
            'MsgBox(DataGridView1.SelectedCells(29).Value)
            If DataGridView1.SelectedRows.Count > 0 Then
                If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                    If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value
                    If (LOCCLAVE = 1 Or LOCCLAVE = 3) And DataGridView1.SelectedCells(7).Value <> "Ext. Adicionales" Then
                        If LOCCLAVE = 1 Then MsgBox("No se puede quitar la Contrataci�n", MsgBoxStyle.Information)
                        If LOCCLAVE = 2 Then MsgBox("No se puede quitar la Reconexi�n", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    '--MsgBox(DataGridView1.SelectedCells(0).Value & "," & DataGridView1.SelectedCells(29).Value & "," & IdSistema & "," & Me.CLV_TIPOCLIENTELabel1.Text)
                    Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                    Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, gloClv_Session)
                    Me.QUITARDELDETALLETableAdapter.Connection = CON

                    Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, DataGridView1.SelectedCells(29).Value, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)
                    If BndError = 1 Then
                        Me.LABEL19.Text = Msg
                        Me.Panel5.Visible = True
                        Me.Bloque(False)
                    ElseIf BndError = 2 Then
                        MsgBox(Msg)
                    Else
                        Me.Bloque(True)
                    End If
                    Me.DameDetalleTableAdapter.Connection = CON
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
                    Me.SumaDetalleTableAdapter.Connection = CON
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
                    ''gloClv_Session = DataGridView1.SelectedCells(0).Value        
                    ''gloClave = DataGridView1.SelectedCells(4).Value
                End If
            End If
            CON.Dispose()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        eBotonGuardar = False

        'If Me.DataGridView1.RowCount = 0 Then
        ' Me.Button7.Enabled = False
        'Me.Button6.Enabled = False
        'Me.Button2.Enabled = False
        'End If
    End Sub

    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            If IsNumeric(GloContrato) = True Then
                If GloContrato > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                    CON.Dispose()
                    CON.Close()

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If BndError = 1 Then
            If Me.LABEL19.BackColor = Color.Yellow Then
                Me.LABEL19.BackColor = Me.Panel5.BackColor
            Else
                Me.LABEL19.BackColor = Color.Yellow
            End If
        End If
    End Sub

    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            If DataGridView1.RowCount > 0 Then
                If DataGridView1.SelectedCells(6).Value.ToString.Contains("Mensualidad") = True Or DataGridView1.SelectedCells(6).Value.ToString.Contains("Cobro") = True Or DataGridView1.SelectedCells(6).Value.ToString.Contains("Contrata") = True Or DataGridView1.SelectedCells(6).Value.ToString.Contains("Cargo") = True Then
                    If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) Then
                        GloDes_Ser = DataGridView1.SelectedCells(6).Value
                        gloClv_Session = DataGridView1.SelectedCells(0).Value
                        gloClv_Detalle = DataGridView1.SelectedCells(29).Value
                        loctitulo = "Solo el Supervisor puede Bonificar"
                        locband_pant = 3
                        locBndBon1 = True
                        If IdSistema = "SA" And GloTipoUsuario = 1 Then
                            eAccesoAdmin = False
                        End If
                        FrmSupervisor.Show()
                    Else
                        MsgBox("Seleccione el Concepto que deseas Bonificar ", MsgBoxStyle.Information)
                    End If
                Else
                    MsgBox("Solo se puede Bonificar las Mensualidades ", MsgBoxStyle.Information)
                End If

            End If
            eBotonGuardar = False
        Catch ex As System.Exception
            Exit Sub
        End Try
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub


    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), DescripcionToolStripTextBox.Text)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button2.Enabled = False
        'Cuando se pague una Factura Fiscal, se va a capturar la Serie y el Folio de la Factura Membretada
        'If eBndDatosFiscales = True Then
        '    eClv_Session = Clv_Session.Text
        '    eDFiscalGlo = False
        '    EsDatosFiscales()
        '    If eDFiscalGlo = False Then
        '        FrmMembretada.Show()
        '    Else
        '        ' MsgBox("Cliente - NO MEMNRETADA Serie Global")
        '        eBndMembretadas = True
        '        BotonGrabar()
        '    End If
        'Else
        BotonGrabar()
        'End If

        'If (VerificaSerieFolioFacturas()) Then
        '    MessageBox.Show("Ya se  ha utilizado ese folio y serie en otra factura favor de verificar")
        'Else
        '    If (VerificaDisponibilidadFolio()) Then
        '        MessageBox.Show("El folio no se encuentra dentro del rango de la serie")
        '    Else
        '        'Cuando se pague una Factura Fiscal, se va a capturar la Serie y el Folio de la Factura Membretada
        '        If eBndDatosFiscales = True Then
        '            eClv_Session = Clv_Session.Text
        '            FrmMembretada.Show()
        '        Else
        '            BotonGrabar()
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub EsDatosFiscales()
        Dim conexion As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("uspValidaDFiscalGlobal", conexion)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 0

        'GloContrato = CLng(Me.ContratoTextBox.Text)
        Dim par1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = CLng(Me.ContratoTextBox.Text)
        cmd.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@CLVSESSION", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = eClv_Session
        cmd.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@BndG", SqlDbType.Bit)
        par3.Direction = ParameterDirection.Output
        par3.Value = eDFiscalGlo
        cmd.Parameters.Add(par3)

        Try
            conexion.Open()
            cmd.ExecuteNonQuery()
            eDFiscalGlo = par3.Value
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub

    Private Sub TreeView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
    End Sub



    'Private Sub FolioTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FolioTextBox.KeyDown
    '    If e.KeyCode = Keys.F10 Then
    '        If eBndDatosFiscales = True Then
    '            eClv_Session = Clv_Session.Text
    '            FrmMembretada.Show()
    '        Else
    '            BotonGrabar()
    '        End If
    '    End If
    'End Sub

    'Private Sub FolioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FolioTextBox.TextChanged
    '    If (Not FolioTextBox.Text.Equals("") And IsNumeric(FolioTextBox.Text)) Then

    '        If (VerificaSerieFolioFacturas()) Then
    '            MessageBox.Show("Ya se  ha utilizado ese folio y serie en otra factura favor de verificar")
    '        End If

    '    End If
    'End Sub

    'Private Function VerificaSerieFolioFacturas() As Boolean
    '    If GloTipo = "V" Then
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, Me.ComboBox2.Text, 50)
    '        BaseII.CreateMyParameter("@Folio", SqlDbType.BigInt, Me.FolioTextBox.Text)
    '        BaseII.CreateMyParameter("@Existe", ParameterDirection.Output, SqlDbType.Bit)
    '        Return BaseII.ProcedimientoOutPut("USP_ValidaExitenciaSerieFolio")("@Existe")
    '    Else
    '        Return False
    '    End If


    'End Function

    'Private Function VerificaDisponibilidadFolio() As Boolean
    '    If GloTipo = "V" Then
    '        BaseII.limpiaParametros()
    '        BaseII.CreateMyParameter("@Serie", SqlDbType.VarChar, Me.ComboBox2.Text, 50)
    '        BaseII.CreateMyParameter("@Folio", SqlDbType.BigInt, Me.FolioTextBox.Text)
    '        BaseII.CreateMyParameter("@Existe", ParameterDirection.Output, SqlDbType.Bit)
    '        Return BaseII.ProcedimientoOutPut("uspValidaDisponibilidadFolio")("@Existe")
    '    Else
    '        Return False
    '    End If


    'End Function

    Private Sub Button1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button1.KeyDown
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
    End Sub

    Private Sub Button8_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button8.KeyDown
        If e.KeyCode = Keys.F10 Then
            If eBndDatosFiscales = True Then
                eClv_Session = Clv_Session.Text
                FrmMembretada.Show()
            Else
                BotonGrabar()
            End If
        End If
    End Sub

    Private Sub ConfigureCrystalReportsOrdenes(ByVal op As String, ByVal Titulo As String, ByVal Clv_Orden1 As Long, ByVal Clv_TipSer1 As Integer, ByVal GloNom_TipSer As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing



            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(Clv_Orden1))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(Clv_Orden1))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            mySelectFormula = "Orden De Servicio: "
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            Dim CON12 As New SqlConnection(MiConexion)
            CON12.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            CON12.Dispose()
            CON12.Close()

            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)




            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsOrdenes_NewXml(ByVal op As String, ByVal Titulo As String, ByVal Clv_Orden1 As Long, ByVal Clv_TipSer1 As Integer, ByVal GloNom_TipSer As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing


            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "AG" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            Dim cnn As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("ReporteAreaTecnicaOrdSer1", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = 0
            cmd.Parameters.Add(parametro1)

            Dim parametro2 As New SqlParameter("@op1", SqlDbType.SmallInt)
            parametro2.Direction = ParameterDirection.Input
            parametro2.Value = 1
            cmd.Parameters.Add(parametro2)

            Dim parametro3 As New SqlParameter("@op2", SqlDbType.SmallInt)
            parametro3.Direction = ParameterDirection.Input
            parametro3.Value = Op2
            cmd.Parameters.Add(parametro3)

            Dim parametro4 As New SqlParameter("@op3", SqlDbType.SmallInt)
            parametro4.Direction = ParameterDirection.Input
            parametro4.Value = Op3
            cmd.Parameters.Add(parametro4)

            Dim parametro5 As New SqlParameter("@op4", SqlDbType.SmallInt)
            parametro5.Direction = ParameterDirection.Input
            parametro5.Value = Op4
            cmd.Parameters.Add(parametro5)

            Dim parametro6 As New SqlParameter("@op5", SqlDbType.SmallInt)
            parametro6.Direction = ParameterDirection.Input
            parametro6.Value = Op5
            cmd.Parameters.Add(parametro6)

            Dim parametro7 As New SqlParameter("@StatusPen", SqlDbType.Bit)
            parametro7.Direction = ParameterDirection.Input
            If StatusPen = "1" Then
                parametro7.Value = True
            Else
                parametro7.Value = False
            End If
            cmd.Parameters.Add(parametro7)

            Dim parametro8 As New SqlParameter("@StatusEje", SqlDbType.Bit)
            parametro8.Direction = ParameterDirection.Input
            If StatusEje = "1" Then
                parametro8.Value = True
            Else
                parametro8.Value = False
            End If
            cmd.Parameters.Add(parametro8)

            Dim parametro9 As New SqlParameter("@StatusVis", SqlDbType.Bit)
            parametro9.Direction = ParameterDirection.Input
            If StatusVis = "1" Then
                parametro9.Value = True
            Else
                parametro9.Value = False
            End If
            cmd.Parameters.Add(parametro9)

            Dim parametro10 As New SqlParameter("@Clv_OrdenIni", SqlDbType.BigInt)
            parametro10.Direction = ParameterDirection.Input
            parametro10.Value = CLng(Clv_Orden1)
            cmd.Parameters.Add(parametro10)

            Dim parametro11 As New SqlParameter("@Clv_OrdenFin", SqlDbType.BigInt)
            parametro11.Direction = ParameterDirection.Input
            parametro11.Value = CLng(Clv_Orden1)
            cmd.Parameters.Add(parametro11)

            Dim parametro12 As New SqlParameter("@Fec1Ini", SqlDbType.DateTime)
            parametro12.Direction = ParameterDirection.Input
            parametro12.Value = Fec1Ini
            cmd.Parameters.Add(parametro12)

            Dim parametro13 As New SqlParameter("@Fec1Fin", SqlDbType.DateTime)
            parametro13.Direction = ParameterDirection.Input
            parametro13.Value = Fec1Fin
            cmd.Parameters.Add(parametro13)

            Dim parametro14 As New SqlParameter("@Fec2Ini", SqlDbType.DateTime)
            parametro14.Direction = ParameterDirection.Input
            parametro14.Value = Fec2Ini
            cmd.Parameters.Add(parametro14)

            Dim parametro15 As New SqlParameter("@Fec2Fin", SqlDbType.DateTime)
            parametro15.Direction = ParameterDirection.Input
            parametro15.Value = Fec2Fin
            cmd.Parameters.Add(parametro15)

            Dim parametro16 As New SqlParameter("@Clv_Trabajo", SqlDbType.Int)
            parametro16.Direction = ParameterDirection.Input
            parametro16.Value = nclv_trabajo
            cmd.Parameters.Add(parametro16)

            Dim parametro17 As New SqlParameter("@Clv_Colonia", SqlDbType.Int)
            parametro17.Direction = ParameterDirection.Input
            parametro17.Value = nClv_colonia
            cmd.Parameters.Add(parametro17)

            Dim parametro18 As New SqlParameter("@OpOrden", SqlDbType.Int)
            parametro18.Direction = ParameterDirection.Input
            parametro18.Value = OpOrdenar
            cmd.Parameters.Add(parametro18)
            Dim parametro22 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            parametro22.Direction = ParameterDirection.Input
            parametro22.Value = 0
            cmd.Parameters.Add(parametro22)
            Dim parametro23 As New SqlParameter("@ResVisita", SqlDbType.Int)
            parametro23.Direction = ParameterDirection.Input
            parametro23.Value = 0
            cmd.Parameters.Add(parametro23)

            Dim parametro24 As New SqlParameter("@op6", SqlDbType.SmallInt)
            parametro24.Direction = ParameterDirection.Input
            parametro24.Value = 0
            cmd.Parameters.Add(parametro24)

            Dim parametro25 As New SqlParameter("@clvDepto", SqlDbType.BigInt)
            parametro25.Direction = ParameterDirection.Input
            parametro25.Value = 0
            cmd.Parameters.Add(parametro25)

            Dim parametro26 As New SqlParameter("@clvTipoServicioDigital", SqlDbType.Int)
            parametro26.Direction = ParameterDirection.Input
            parametro26.Value = 0
            cmd.Parameters.Add(parametro26)

            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet()

            da.Fill(ds)
            ds.Tables(0).TableName = "ReporteAreaTecnicaOrdSer"
            ds.Tables(1).TableName = "DameDatosGenerales_2"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            Dim CON12 As New SqlConnection(MiConexion)
            CON12.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            CON12.Dispose()
            CON12.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Mana_ImprimirOrdenes(ByVal Clv_Factura As Long)
        Try
            'Eric 10Dic2008 LAS ORDENES DE SERVICIO QUE SE GENERAN DESDE FACTURACION
            'NO SE IMPRIMIRAN DE FORMA AUTOM�TICA

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Connection = CON
            Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Fill(Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura, New System.Nullable(Of Long)(CType(Clv_Factura, Long)))

            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""

            For Each FilaRow In Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura.Rows
                If IsNumeric(FilaRow("Clv_Orden").ToString()) = True Then
                    ConfigureCrystalReportsOrdenes_NewXml(0, "", FilaRow("Clv_Orden").ToString(), FilaRow("Clv_TipSer").ToString(), FilaRow("Concepto").ToString())
                End If
            Next
            CON.Dispose()
            CON.Close()


        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        FrmServiciosPPE.Show()
    End Sub

    Private Sub CMBPanel6_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles CMBPanel6.Paint

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Me.CMBPanel6.Visible = False
        SiPagos = 0
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            SiPagos = 1
            Me.Cobra_PagosTableAdapter.Connection = CON
            Me.Cobra_PagosTableAdapter.Fill(Me.DataSetEdgar.Cobra_Pagos, New System.Nullable(Of Long)(CType(GloContrato, Long)), New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Integer)(CType(Bnd, Integer)), New System.Nullable(Of Integer)(CType(CuantasTv, Integer)))
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
            CON.Dispose()
            CON.Close()
            Me.CMBPanel6.Visible = False
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub GuardaTbl_RespuestaCancelaServicios(ByVal eClv_Session As Long, ByVal ebNDREGRESAtODO As Boolean)
        Dim Con1 As New SqlConnection(MiConexion)
        Try
            Dim cmd As New SqlClient.SqlCommand()
            Con1.Open()
            With cmd
                .CommandText = "USPCANCELATODOSSERVICIOS"
                .Connection = Con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@Clave bigint output,@NomArchivo varchar(20) output
                Dim prm1 As New SqlParameter("@CLV_SESSION", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eClv_Session
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@bNDREGRESAtODO", SqlDbType.Bit)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = ebNDREGRESAtODO
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            Con1.Close()
        Catch ex As Exception
            If Con1.State <> ConnectionState.Closed Then Con1.Close()
        End Try
    End Sub

    Private Sub AgregaMotCan(ByVal oClv_Session As Long, ByVal oClv_MotCan As Integer)
        '@Clv_Session bigint,@Clv_Txt varchar(10),@op int,@Clv_Telefono int,@Contrato bigint
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, oClv_Session)
        BaseII.CreateMyParameter("@Clv_MotCan", SqlDbType.Int, oClv_MotCan)
        BaseII.EjecutaDT("Usp_InsertaMotCanFac")
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click

        Dim LOCCLAVE As Integer = 0
        Dim BndCancelaServicios As Boolean = False
        Dim bNDREGRESAtODO As Boolean = False
        Try
            Dim CON As New SqlConnection(MiConexion)
            If IsNumeric(Me.Clv_Session.Text) = True And IsNumeric(Me.CLV_DETALLETextBox.Text) = True Then
                If Me.Clv_Session.Text > 0 And Me.CLV_DETALLETextBox.Text > 0 Then
                    ePideAparato = 0
                    Dim ERROR_1 As Integer = 0
                    Dim msgERROR_1 As String = Nothing




                    'Solamente lanzamos la pregunta a los conceptos que sean de Mensualidad
                    If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value
                    EsInternet = 0
                    usp_CobroAdeudoInt(New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))


                    If (LOCCLAVE = 2) Then
                        If EsInternet = 0 Then
                            If (MsgBox("�Deseas Cancelar Todos los Servicios?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) And bNDREGRESAtODO = False Then
                                bNDREGRESAtODO = True
                                BndCancelaServicios = True
                                Dim i1 As Integer = 0
                                For i1 = 0 To DataGridView1.RowCount - 1
                                    'MsgBox(DataGridView1.Item(0, i1).Value.ToString ) 'Clave _Session
                                    If DataGridView1.Item(4, i1).Value.ToString() = "2" Then
                                        Me.CLV_DETALLETextBox.Text = DataGridView1.Item(29, i1).Value.ToString()
                                        If (FrmSelAparatosDeEntrega.ConsultaTipoPaquetePorClv_Detalle(Me.CLV_DETALLETextBox.Text)) Then
                                            If (MsgBox("�El Cliente trae consigo el aparato : " & DataGridView1.Item(5, i1).Value.ToString() & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                                                'Se verifica qu� aparatos trae el Cliente
                                                FrmSelAparatosDeEntrega.Clv_Detalle = Me.CLV_DETALLETextBox.Text
                                                FrmSelAparatosDeEntrega.ShowDialog()
                                                FrmSelAparatosDeEntrega.Clv_Detalle = 0
                                            End If
                                        End If

                                        Me.CobraAdeudoTableAdapter.Connection = CON
                                        Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)

                                    End If

                                Next
                                Dim selMotCan As New FrmMotivoCancelacion()
                                selMotCan.ShowDialog()

                            End If
                        ElseIf EsInternet = 1 Then
                            bNDREGRESAtODO = True
                            BndCancelaServicios = True
                            Dim i1 As Integer = 0
                            For i1 = 0 To DataGridView1.RowCount - 1
                                'MsgBox(DataGridView1.Item(0, i1).Value.ToString ) 'Clave _Session
                                If DataGridView1.Item(4, i1).Value.ToString() = "2" Then
                                    Me.CLV_DETALLETextBox.Text = DataGridView1.Item(29, i1).Value.ToString()
                                    If (FrmSelAparatosDeEntrega.ConsultaTipoPaquetePorClv_Detalle(Me.CLV_DETALLETextBox.Text)) Then
                                        If (MsgBox("�El Cliente trae consigo el aparato : " & DataGridView1.Item(5, i1).Value.ToString() & "?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                                            'Se verifica qu� aparatos trae el Cliente
                                            FrmSelAparatosDeEntrega.Clv_Detalle = Me.CLV_DETALLETextBox.Text
                                            FrmSelAparatosDeEntrega.ShowDialog()
                                            FrmSelAparatosDeEntrega.Clv_Detalle = 0
                                        End If
                                    End If



                                    Me.CobraAdeudoTableAdapter.Connection = CON
                                    Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)

                                End If

                            Next
                            Dim selMotCan As New FrmMotivoCancelacion()
                            selMotCan.ShowDialog()
                        End If
                        'Edgar Agrego 24/junio /2011 para Pregunta para cancelar todos los Servicios
                        'If (MsgBox("�Deseas Cancelar Todos los Servicios ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                        '    ' GuardaTbl_RespuestaCancelaServicios(Me.Clv_Session.Text)
                        '    BndCancelaServicios = True
                        'End If
                        'Verifica si es que el Serivicio que se est� seleccionando es Principal.
                        If BndCancelaServicios = False Then
                            If (FrmSelAparatosDeEntrega.ConsultaTipoPaquetePorClv_Detalle(Me.CLV_DETALLETextBox.Text)) Then
                                If (MsgBox("�El Cliente trae alg�n aparato consigo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes) Then
                                    'Se verifica qu� aparatos trae el Cliente
                                    FrmSelAparatosDeEntrega.Clv_Detalle = Me.CLV_DETALLETextBox.Text
                                    FrmSelAparatosDeEntrega.ShowDialog()
                                    FrmSelAparatosDeEntrega.Clv_Detalle = 0
                                End If
                            End If
                        End If


                        CON.Open()
                        If BndCancelaServicios = False Then

                            Me.CobraAdeudoTableAdapter.Connection = CON
                            Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)
                            Dim selMotCanII As New FrmMotivoCancelacion()
                            selMotCanII.ShowDialog()
                        End If
                        Me.DameDetalleTableAdapter.Connection = CON
                        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
                        Me.SumaDetalleTableAdapter.Connection = CON
                        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
                        CON.Dispose()
                        CON.Close()
                        If ERROR_1 = 1 Then
                            Me.LABEL19.Text = msgERROR_1
                            Me.Panel5.Visible = True
                            Me.Bloque(False)
                        ElseIf ERROR_1 = 2 Then
                            MsgBox(msgERROR_1)
                        End If
                        'VALIDACION PARA VALLARTA. PREGUNTA SI EL CLIENTE LLEVA CONSIGO EL APARATO, SI NO, SE GENERA UNA ORDEN DE RETIRO DE APARATO O CABLEMODEM
                        If ePideAparato = 1 Then
                            eRes = MsgBox("�El Cliente trae con sigo el Aparato?", MsgBoxStyle.YesNo, "Atenci�n")
                            If eRes = 6 Then
                                Dim CON2 As New SqlConnection(MiConexion)
                                CON2.Open()
                                Me.EntregaAparatoTableAdapter.Connection = CON2
                                Me.EntregaAparatoTableAdapter.Fill(Me.EricDataSet2.EntregaAparato, CLng(Me.Clv_Session.Text), eClv_Detalle)
                                CON2.Close()
                            End If
                        End If

                    End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function usp_CobroAdeudoInt(ByVal prmClv_session As Long) As Integer
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, prmClv_session)
        BaseII.CreateMyParameter("@Esinternet", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("usp_CobroAdeudoInt")

        EsInternet = CInt(BaseII.dicoPar("@Esinternet").ToString())
    End Function

    Private Sub CLV_DETALLETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_DETALLETextBox.TextChanged

    End Sub

    Private Sub Label14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label14.Click

    End Sub


    Private Sub LblFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblFecha.Click

    End Sub

    Private Sub LblFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblFecha.TextChanged
        If IsDate(LblFecha.Text) = True Then
            Me.Fecha_Venta.Value = LblFecha.Text
        End If
    End Sub

    Private Sub Fecha_Venta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Venta.ValueChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                If bloqueado <> 1 Then
                    BUSCACLIENTES(0)
                    Me.Bloque(True)
                End If
            End If
        End If
    End Sub




    Private Sub ClvSessionDataGridViewTextBoxColumn1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClvSessionDataGridViewTextBoxColumn1.Disposed

    End Sub

    Private Sub ConRelClienteObs(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelClienteObs", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Obs", SqlDbType.VarChar, 500)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
            Me.TextBoxClienteObs.Text = parametro2.Value
            If Me.TextBoxClienteObs.Text.Length > 0 Then Me.TextBoxClienteObs.Text = "Observaci�n: " + Me.TextBoxClienteObs.Text
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub NueRelFacturasMembretadas(ByVal ClvSession As Long, ByVal Clv_Factura As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelFacturasMembretadas", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@ClvSession", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = ClvSession
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Factura
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub LABEL19_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LABEL19.TextChanged

    End Sub
    Private Sub ReportesFacturasXsd(ByVal prmClvFactura As Long, ByVal prmClvFacturaIni As Long, ByVal prmClvFacturaFin As Long, ByVal prmFechaIni As Date, _
                                   ByVal prmFechaFin As Date, ByVal prmOp As Integer, ByVal RUTAREP As String)
        Dim CON As New SqlConnection(MiConexion)

        Dim CMD As New SqlCommand("ReportesFacturasXsd", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Clv_Factura", prmClvFactura)
        CMD.Parameters.AddWithValue("@Clv_Factura_Ini", prmClvFacturaIni)
        CMD.Parameters.AddWithValue("@Clv_Factura_Fin", prmClvFacturaFin)
        CMD.Parameters.AddWithValue("@Fecha_Ini", prmFechaIni)
        CMD.Parameters.AddWithValue("@Fecha_Fin", prmFechaFin)
        CMD.Parameters.AddWithValue("@op", prmOp)
        Dim DA As New SqlDataAdapter(CMD)

        Dim DS As New DataSet()

        DA.Fill(DS)

        DS.Tables(0).TableName = "FacturaFiscal"

        customersByCityReport.Load(RUTAREP)
        customersByCityReport.SetDataSource(DS)
    End Sub

    Private Sub DAMEtblRelVendedoresSeries(ByVal Contrato As Integer, ByVal Clv_Vendedor As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DAMEtblRelVendedoresSeries", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Vendedor
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Folio", SqlDbType.Int)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            SerieTextBox.Text = par3.Value.ToString()
            FolioTextBox.Text = cmbFolio.Text
            'FolioTextBox.Text = par4.Value.ToString()
            eMsj = ""
            eMsj = par5.Value.ToString()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MUESTRAVendedoresSeries()
        Dim conexion As New SqlConnection(MiConexion)
        Dim dAdapter As New SqlDataAdapter("EXEC MUESTRAVendedoresSeries", conexion)
        Dim dTable As New DataTable

        Try
            dAdapter.Fill(dTable)
            cbVendedor.DataSource = dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub VALIDASucursalesSeries(ByVal Contrato As Integer, ByVal Clv_Sucursal As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("VALIDASucursalesSeries", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Contrato", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Contrato
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Sucursal
        comando.Parameters.Add(par2)

        Dim par5 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par5.Direction = ParameterDirection.Output
        comando.Parameters.Add(par5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par5.Value.ToString()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Function ReportesFacturas(ByVal Clv_Factura As Long) As DataSet
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ReportesFacturas " + Clv_Factura.ToString() + ",0,0,'19000101','19000101',0")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet

        Try
            dAdapter.Fill(dSet)
            dSet.Tables(0).TableName = "CALLES"
            dSet.Tables(1).TableName = "CIUDADES"
            dSet.Tables(2).TableName = "CLIENTES"
            dSet.Tables(3).TableName = "COLONIAS"
            dSet.Tables(4).TableName = "CatalogoCajas"
            dSet.Tables(5).TableName = "DatosFiscales"
            dSet.Tables(6).TableName = "DetFacturas"
            dSet.Tables(7).TableName = "DetFacturasImpuestos"
            dSet.Tables(8).TableName = "Facturas"
            dSet.Tables(9).TableName = "GeneralDesconexion"
            dSet.Tables(10).TableName = "ReportesFacturas"
            dSet.Tables(11).TableName = "SUCURSALES"
            dSet.Tables(12).TableName = "Usuarios"
            dSet.Tables(13).TableName = "General"
            dSet.Tables(14).TableName = "tblRelSucursalDatosGenerales"
            dSet.Tables(15).TableName = "companias"
            Return dSet
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub GUARDAPagoTransferencia(ByVal Clv_Factura As Integer, ByVal Importe As Double, ByVal Banco As Integer, ByVal NumeroTransferencia As String, ByVal Autorizacion As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Factura", SqlDbType.Int, Clv_Factura)
        BaseII.CreateMyParameter("@Importe", SqlDbType.Decimal, Importe)
        BaseII.CreateMyParameter("@Banco", SqlDbType.Int, Banco)
        BaseII.CreateMyParameter("@NumeroTransferencia", SqlDbType.VarChar, NumeroTransferencia, 50)
        BaseII.CreateMyParameter("@Autorizacion", SqlDbType.VarChar, Autorizacion, 50)
        BaseII.Inserta("GUARDAPagoTransferencia")
    End Sub

    Private Sub cmbFolio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFolio.SelectedIndexChanged
        If Len(cmbFolio.Text) > 0 Then
            FolioTextBox.Text = cmbFolio.Text
        End If
    End Sub

#Region "VALIDA SI IMPRIME QUEJA"
    Private Function checaSiImprimeOrden() As Boolean
        Dim imprimeQueja As New classValidaciones

        imprimeQueja.sistema = "FAC"
        imprimeQueja.ordenQueja = "ORDEN"
        checaSiImprimeOrden = imprimeQueja.uspChecaSiImprimeOrdenQueja()
    End Function
#End Region

    Private Sub ButtonPagoAbono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPagoAbono.Click
        GloBndPagAbnCobBaj = True
        eBotonGuardar = False
        GloClvSessionAbono = 0
        GloImporteTotalEstadoDeCuenta = SumaDetalleDataGridView(4, 2).Value
        Me.Enabled = False
        FrmPagoAbonoACuenta.Show()
    End Sub

    Private Sub uspDimeStatus()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@Desco", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Recon", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspDimeStatus")
        StatusDesc = CInt(BaseII.dicoPar("@Desco").ToString)
        Recon = CInt(BaseII.dicoPar("@Recon").ToString)
    End Sub

    Private Sub uspDimeStatusEco()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(Me.ContratoTextBox.Text))
        BaseII.CreateMyParameter("@Desco", ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Recon", ParameterDirection.Output, SqlDbType.Int)
        'ByVal prmNombre As String, ByRef prmTipo As SqlDbType, ByRef prmTamanio As Integer, ByRef prmDireccion As ParameterDirection, ByRef prmValor As Object
        BaseII.ProcedimientoOutPut("uspDimeStatusEco")
        StatusDesc = CInt(BaseII.dicoPar("@Desco").ToString)
        Recon = CInt(BaseII.dicoPar("@Recon").ToString)
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress

    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        GloContratoSaldo = Me.ContratoTextBox.Text
        FrmSaldoCliente.Show()
    End Sub
End Class
