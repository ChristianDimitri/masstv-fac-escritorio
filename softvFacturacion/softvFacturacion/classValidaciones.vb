﻿Public Class classValidaciones
#Region "PROPIEDADES"
    Private _sistema As String
    Public Property sistema As String
        Get
            Return _sistema
        End Get
        Set(value As String)
            _sistema = value
        End Set
    End Property

    Private _ordenQueja As String
    Public Property ordenQueja As String
        Get
            Return _ordenQueja
        End Get
        Set(value As String)
            _ordenQueja = value
        End Set
    End Property
#End Region

#Region "CONSTRUCTORES"
    Public Sub New()
        _sistema = String.Empty
        _ordenQueja = String.Empty
    End Sub
#End Region

#Region "MÉTODOS"
    Public Function uspChecaSiImprimeOrdenQueja() As Boolean
        Try
            Dim ordenQueja As New BaseIII

            ordenQueja.CreateMyParameter("@sistema", SqlDbType.VarChar, _sistema, 5)
            ordenQueja.CreateMyParameter("@ordenQueja", SqlDbType.VarChar, _ordenQueja, 5)
            ordenQueja.CreateMyParameter("@bndImprimeOrdenQueja", ParameterDirection.Output, SqlDbType.Bit)
            ordenQueja.ProcedimientoOutPut("uspChecaSiImprimeOrdenQueja")

            uspChecaSiImprimeOrdenQueja = CBool(ordenQueja.dicoPar("@bndImprimeOrdenQueja").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
