﻿Imports System.Net.NetworkInformation
Imports System.Data.SqlClient
Imports System.Net
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO
Imports System
Imports Microsoft.VisualBasic
Imports System.Text

Module ModuleDigital


    Dim Archivo As String
    Dim space As String
    Dim intro As String
    Dim indice As Integer
    Dim indice2 As Integer
    Dim Fecha As String
    Dim CantidadTotal As String
    Dim NoClientes As String
    Dim Contrato As String
    Dim NoCuenta As String
    Dim Cantidadcte As String
    Dim Ruta As String

    Public Sub GeneraDocumentoTxt_DIGITALNota(ByVal clv_factura As Long, ByVal Ruta As String)
        Dim Referencia_Servicio As String = Nothing
        Try
            'GloProcesa = 3
            Dim CONE As New SqlConnection(MiConexion)
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Txt As String = Nothing
            Dim GLOBND As Boolean = True
            Dim Fila1_01 As String = Nothing
            Dim Fila1_02 As String = Nothing
            Dim Fila1_03 As String = Nothing
            Dim Fila1_04 As String = Nothing
            Dim Fila1_05 As String = Nothing
            Dim Fila1_06 As String = Nothing
            Dim Fila1_07 As String = Nothing
            Dim Fila1_08 As String = Nothing
            Dim Fila1_09 As String = Nothing
            Dim Fila1_10 As String = Nothing
            Dim Fila1_11 As String = Nothing
            Dim Fila1_12 As String = Nothing
            Dim Fila1_13 As String = Nothing
            Dim Fila1_14 As String = Nothing
            Dim Fila1_15 As String = Nothing
            Dim Fila1_16 As String = Nothing
            '
            Dim Fila2_01 As String = Nothing
            Dim Fila2_02 As String = Nothing
            Dim Fila2_03 As String = Nothing
            Dim Fila2_04 As String = Nothing
            Dim Fila2_05 As String = Nothing
            Dim Fila2_06 As String = Nothing
            Dim Fila2_07 As String = Nothing
            Dim Fila2_08 As String = Nothing
            '
            Dim Fila3_01 As String = Nothing
            Dim Fila3_02 As String = Nothing
            Dim Fila3_03 As String = Nothing
            Dim Fila3_04 As String = Nothing
            Dim Fila3_05 As String = Nothing
            Dim Fila3_06 As String = Nothing
            Dim Fila3_07 As String = Nothing
            Dim Fila3_08 As String = Nothing
            Dim Fila3_09 As String = Nothing
            Dim Fila3_10 As String = Nothing
            Dim Fila3_11 As String = Nothing
            Dim Fila3_12 As String = Nothing
            Dim Fila3_13 As String = Nothing
            Dim Fila3_14 As String = Nothing
            '
            Dim Fila5_01 As String = Nothing
            Dim Fila5_02 As String = Nothing
            Dim Fila5_03 As String = Nothing
            Dim Fila5_04 As String = Nothing
            Dim Fila5_05 As String = Nothing
            Dim Fila5_06 As String = Nothing
            Dim Fila5_07 As String = Nothing
            Dim Fila5_08 As String = Nothing
            Dim Fila5_09 As String = Nothing
            '
            Dim Fila6_01 As String = Nothing
            Dim Fila6_02 As String = Nothing
            Dim Fila6_03 As String = Nothing
            Dim Fila6_04 As String = Nothing

            Dim ImporteTotal As String = Nothing
            Dim Leyenda_tmp As String = Nothing
            Dim Tot_Reg As String = Nothing
            Dim Ano As String = Nothing
            Dim Mes As String = Nothing
            Dim DIA As String = Nothing
            Dim Fila3 As String = Nothing
            Dim Fila4 As String = Nothing
            Dim Fila5 As String = Nothing
            Dim Fila6 As String = Nothing
            Dim Fila7 As String = Nothing


            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.DameGeneralesBancosTableAdapter.Connection = CON
            'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "SA")
            'CON.Close()
            '
            Dim Cont As Integer
            Cont = 0
            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC NTEncabezado_Dig " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila1_01 = CStr(reader.GetValue(1))
                        Fila1_02 = CStr(reader.GetValue(2))
                        Fila1_03 = CStr(reader.GetValue(3))
                        Fila1_04 = CStr(reader.GetValue(4))
                        Fila1_05 = CStr(reader.GetValue(5))
                        Fila1_06 = CStr(reader.GetValue(6))
                        Fila1_07 = CStr(reader.GetValue(7))
                        Fila1_08 = CStr(reader.GetValue(8))
                        Fila1_09 = CStr(reader.GetValue(9))
                        Fila1_10 = CStr(reader.GetValue(10))
                        Fila1_11 = CStr(reader.GetValue(11))
                        Fila1_12 = CStr(reader.GetValue(12))
                        Fila1_13 = CStr(reader.GetValue(13))
                        Fila1_14 = CStr(reader.GetValue(14))
                        Fila1_15 = CStr(reader.GetValue(15))
                    End While
                End Using
            End With
            CONE.Close()
            CONE.Open()
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC NTDATOS_DEL_PAGO_DEL_DIGITAL " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila2_01 = CStr(reader.GetValue(1))
                        Fila2_02 = CStr(reader.GetValue(2))
                        Fila2_03 = CStr(reader.GetValue(3))
                        Fila2_04 = CStr(reader.GetValue(4))
                        Fila2_05 = CStr(reader.GetValue(5))
                        Fila2_06 = CStr(reader.GetValue(6))
                        Fila2_07 = CStr(reader.GetValue(7))
                        Fila2_08 = CStr(reader.GetValue(8))
                    End While
                End Using
            End With
            CONE.Close()
            CONE.Open()
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC NTDATOS_DEL_RECEPTOR_DEL_DIGITAL " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila3_01 = CStr(reader.GetValue(1))
                        Fila3_02 = CStr(reader.GetValue(2))
                        Fila3_03 = CStr(reader.GetValue(3))
                        Fila3_04 = CStr(reader.GetValue(4))
                        Fila3_05 = CStr(reader.GetValue(5))
                        Fila3_06 = CStr(reader.GetValue(6))
                        Fila3_07 = CStr(reader.GetValue(7))
                        Fila3_08 = CStr(reader.GetValue(8))
                        Fila3_09 = CStr(reader.GetValue(9))
                        Fila3_10 = CStr(reader.GetValue(10))
                        Fila3_11 = CStr(reader.GetValue(11))
                        Fila3_12 = CStr(reader.GetValue(12))
                        Fila3_13 = CStr(reader.GetValue(13))
                        Fila3_14 = CStr(reader.GetValue(14))
                    End While
                End Using
            End With
            CONE.Close()


            '            
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim Fila2 As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing
            Dim nomArchi As String = Nothing
            'Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()

            'Me.FolderBrowserDialog1.ShowDialog()


            'If (result = DialogResult.OK) Then
            Rutatxt = Ruta 'FolderBrowserDialog1.SelectedPath.ToString
            nomArchi = "NTD" & "_" & clv_factura & ".txt"
            Nom_Archivo = Rutatxt + "\" + "NTD" & "_" & clv_factura & ".txt"

            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
            If fileExists = True Then
                File.Delete(Nom_Archivo)
            End If
            Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                'Encabezado = Me.DIATextBox.Text & Me.MESTextBox.Text & Me.ANOTextBox.Text & Me.HORATextBox.Text & Me.MITextBox.Text & "00" & Microsoft.VisualBasic.Strings.Space(6 - Len(Me.ContadorTextBox.Text)) & Me.ContadorTextBox.Text & Microsoft.VisualBasic.Strings.Space(16 - Len(imp1)) & imp1 & Microsoft.VisualBasic.Strings.Space(31)

                'GloEmpresa = "Gigacable de Aguascalientes S.A. de C.V."
                If Len(Trim(Fila1_01)) = 0 Or Fila1_01 = "0.00" Then Fila1_01 = "|" Else Fila1_01 = Fila1_01 & "|"
                If Len(Trim(Fila1_02)) = 0 Or Fila1_02 = "0.00" Then Fila1_02 = "|" Else Fila1_02 = Fila1_02 & "|"
                If Len(Trim(Fila1_03)) = 0 Or Fila1_03 = "0.00" Then Fila1_03 = "|" Else Fila1_03 = Fila1_03 & "|"
                If Len(Trim(Fila1_04)) = 0 Or Fila1_04 = "0.00" Then Fila1_04 = "|" Else Fila1_04 = Fila1_04 & "|"
                If Len(Trim(Fila1_05)) = 0 Or Fila1_05 = "0.00" Then Fila1_05 = "|" Else Fila1_05 = Fila1_05 & "|"
                If Len(Trim(Fila1_06)) = 0 Or Fila1_06 = "0.00" Then Fila1_06 = "|" Else Fila1_06 = Fila1_06 & "|"
                If Len(Trim(Fila1_07)) = 0 Or Fila1_07 = "0.00" Then Fila1_07 = "|" Else Fila1_07 = Fila1_07 & "|"
                If Len(Trim(Fila1_08)) = 0 Or Fila1_08 = "0.00" Then Fila1_08 = "|" Else Fila1_08 = Fila1_08 & "|"
                If Len(Trim(Fila1_09)) = 0 Or Fila1_09 = "0.00" Then Fila1_09 = "|" Else Fila1_09 = Fila1_09 & "|"
                If Len(Trim(Fila1_10)) = 0 Or Fila1_10 = "0.00" Then Fila1_10 = "|" Else Fila1_10 = Fila1_10 & "|"
                If Len(Trim(Fila1_11)) = 0 Or Fila1_11 = "0.00" Then Fila1_11 = "|" Else Fila1_11 = Fila1_11 & "|"
                If Len(Trim(Fila1_12)) = 0 Or Fila1_12 = "0.00" Then Fila1_12 = "|" Else Fila1_12 = Fila1_12 & "|"
                If Len(Trim(Fila1_13)) = 0 Or Fila1_13 = "0.00" Then Fila1_13 = "|" Else Fila1_13 = Fila1_13 & "|"
                If Len(Trim(Fila1_14)) = 0 Or Fila1_14 = "0.00" Then Fila1_14 = "|" Else Fila1_14 = Fila1_14 & "|"
                If Len(Trim(Fila1_15)) = 0 Or Fila1_15 = "0.00" Then Fila1_15 = "|" Else Fila1_15 = Fila1_15 & "|"

                '
                If Len(Trim(Fila2_01)) = 0 Or Fila2_01 = "0.00" Then Fila2_01 = "|" Else Fila2_01 = Fila2_01 & "|"
                If Len(Trim(Fila2_02)) = 0 Or Fila2_02 = "0.00" Then Fila2_02 = "|" Else Fila2_02 = Fila2_02 & "|"
                If Len(Trim(Fila2_03)) = 0 Or Fila2_03 = "0.00" Then Fila2_03 = "|" Else Fila2_03 = Fila2_03 & "|"
                If Len(Trim(Fila2_04)) = 0 Or Fila2_04 = "0.00" Then Fila2_04 = "|" Else Fila2_04 = Fila2_04 & "|"
                If Len(Trim(Fila2_05)) = 0 Or Fila2_05 = "0.00" Then Fila2_05 = "|" Else Fila2_05 = Fila2_05 & "|"
                If Len(Trim(Fila2_06)) = 0 Or Fila2_06 = "0.00" Then Fila2_06 = "|" Else Fila2_06 = Fila2_06 & "|"
                If Len(Trim(Fila2_07)) = 0 Or Fila2_07 = "0.00" Then Fila2_07 = "|" Else Fila2_07 = Fila2_07 & "|"
                If Len(Trim(Fila2_08)) = 0 Or Fila2_08 = "0.00" Then Fila2_08 = "|" Else Fila2_08 = Fila2_08 & "|"
                '
                If Len(Trim(Fila3_01)) = 0 Or Fila3_01 = "0.00" Then Fila3_01 = "|" Else Fila3_01 = Fila3_01 & "|"
                If Len(Trim(Fila3_02)) = 0 Or Fila3_02 = "0.00" Then Fila3_02 = "|" Else Fila3_02 = Fila3_02 & "|"
                If Len(Trim(Fila3_03)) = 0 Or Fila3_03 = "0.00" Then Fila3_03 = "|" Else Fila3_03 = Fila3_03 & "|"
                If Len(Trim(Fila3_04)) = 0 Or Fila3_04 = "0.00" Then Fila3_04 = "|" Else Fila3_04 = Fila3_04 & "|"
                If Len(Trim(Fila3_05)) = 0 Or Fila3_05 = "0.00" Then Fila3_05 = "|" Else Fila3_05 = Fila3_05 & "|"
                If Len(Trim(Fila3_06)) = 0 Or Fila3_06 = "0.00" Then Fila3_06 = "|" Else Fila3_06 = Fila3_06 & "|"
                If Len(Trim(Fila3_07)) = 0 Or Fila3_07 = "0.00" Then Fila3_07 = "|" Else Fila3_07 = Fila3_07 & "|"
                If Len(Trim(Fila3_08)) = 0 Or Fila3_08 = "0.00" Then Fila3_08 = "|" Else Fila3_08 = Fila3_08 & "|"
                If Len(Trim(Fila3_09)) = 0 Or Fila3_09 = "0.00" Then Fila3_09 = "|" Else Fila3_09 = Fila3_09 & "|"
                If Len(Trim(Fila3_10)) = 0 Or Fila3_10 = "0.00" Then Fila3_10 = "|" Else Fila3_10 = Fila3_10 & "|"
                If Len(Trim(Fila3_11)) = 0 Or Fila3_11 = "0.00" Then Fila3_11 = "|" Else Fila3_11 = Fila3_11 & "|"
                If Len(Trim(Fila3_12)) = 0 Or Fila3_12 = "0.00" Then Fila3_12 = "|" Else Fila3_12 = Fila3_12 & "|"
                If Len(Trim(Fila3_13)) = 0 Or Fila3_13 = "0.00" Then Fila3_13 = "|" Else Fila3_13 = Fila3_13 & "|"
                If Len(Trim(Fila3_14)) = 0 Or Fila3_14 = "0.00" Then Fila3_14 = "|" Else Fila3_14 = Fila3_14 & "|"

                Encabezado = Fila1_01 & Fila1_02 & Fila1_03 & Fila1_04 & Fila1_05 & Fila1_06 & Fila1_07 & Fila1_08 & Fila1_09 & Fila1_10 & Fila1_11 & Fila1_12 & Fila1_13 & Fila1_14 & Fila1_15
                Fila2 = Fila2_01 & Fila2_02 & Fila2_03 & Fila2_04 & Fila2_05 & Fila2_06 & Fila2_07 & Fila2_08
                Fila3 = Fila3_01 & Fila3_02 & Fila3_03 & Fila3_04 & Fila3_05 & Fila3_06 & Fila3_07 & Fila3_08 & Fila3_09 & Fila3_10 & Fila3_11 & Fila3_12 & Fila3_13
                sw.Write(Encabezado & vbNewLine)
                sw.Write(Fila2 & vbNewLine)
                sw.Write(Fila3 & vbNewLine)
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC NTDATOS_DE_LOS_CONCEPTO_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila5_01 = CStr(reader.GetValue(1))
                            Fila5_02 = CStr(reader.GetValue(2))
                            Fila5_03 = CStr(reader.GetValue(3))
                            Fila5_04 = CStr(reader.GetValue(4))
                            Fila5_05 = CStr(reader.GetValue(5))
                            Fila5_06 = CStr(reader.GetValue(6))
                            Fila5_07 = CStr(reader.GetValue(7))
                            Fila5_08 = CStr(reader.GetValue(8))
                            Fila5_09 = CStr(reader.GetValue(9))
                            If Len(Trim(Fila5_01)) = 0 Or Fila5_01 = "0.00" Then Fila5_01 = "|" Else Fila5_01 = Fila5_01 & "|"
                            If Len(Trim(Fila5_02)) = 0 Or Fila5_02 = "0.00" Then Fila5_02 = "|" Else Fila5_02 = Fila5_02 & "|"
                            If Len(Trim(Fila5_03)) = 0 Or Fila5_03 = "0.00" Then Fila5_03 = "|" Else Fila5_03 = Fila5_03 & "|"
                            If Len(Trim(Fila5_04)) = 0 Or Fila5_04 = "0.00" Then Fila5_04 = "|" Else Fila5_04 = Fila5_04 & "|"
                            If Len(Trim(Fila5_05)) = 0 Or Fila5_05 = "0.00" Then Fila5_05 = "|" Else Fila5_05 = Fila5_05 & "|"
                            If Len(Trim(Fila5_06)) = 0 Or Fila5_06 = "0.00" Then Fila5_06 = "|" Else Fila5_06 = Fila5_06 & "|"
                            If Len(Trim(Fila5_07)) = 0 Or Fila5_07 = "0.00" Then Fila5_07 = "|" Else Fila5_07 = Fila5_07 & "|"
                            If Len(Trim(Fila5_08)) = 0 Then Fila5_08 = "|" Else Fila5_08 = Fila5_08 & "|"
                            If Len(Trim(Fila5_09)) = 0 Then Fila5_09 = "|" Else Fila5_09 = Fila5_09 & "|"
                            Fila5 = Fila5_01 & Fila5_02 & Fila5_03 & Fila5_04 & Fila5_05 & Fila5_06 & Fila5_07 & Fila5_08 & Fila5_09
                            sw.Write(Fila5 & vbNewLine)
                            Fila5_01 = ""
                            Fila5_02 = ""
                            Fila5_03 = ""
                            Fila5_04 = ""
                            Fila5_05 = ""
                            Fila5_06 = ""
                            Fila5_07 = ""
                            Fila5_08 = ""
                            Fila5_09 = ""
                        End While
                    End Using
                End With
                CONE.Close()
                '06
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC NTDATOS_ADICIONALES_CONCEPTOS " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()
                '07
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC NTDATOS_DE_LOS_IMPUESTOS_TRASLADADOS_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()

                '08
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "NTDATOS_DE_LOS_IMPUESTOS_RETENIDOS_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()

                '09
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC NTDATOS_ADICIONALES_COMPROBANTE " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))

                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"

                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""

                        End While
                    End Using
                End With
                CONE.Close()


                sw.Close()
            End Using
            My.Computer.Network.UploadFile(Nom_Archivo, "ftp://201.148.4.65/" & nomArchi, "usr_notacredito", "Cde34Rfv", True, 10000)
            ' MsgBox("El archivo se genero en la siguiente ruta : " & Nom_Archivo)
            'End If
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & Referencia_Servicio & " son Invalidos")
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub GeneraDocumentoTxt_DIGITAL(ByVal clv_factura As Long, ByVal Ruta As String)
        Dim Referencia_Servicio As String = Nothing
        Try
            'GloProcesa = 3
            Dim CONE As New SqlConnection(MiConexion)
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Txt As String = Nothing
            Dim GLOBND As Boolean = True
            Dim Fila1_01 As String = Nothing
            Dim Fila1_02 As String = Nothing
            Dim Fila1_03 As String = Nothing
            Dim Fila1_04 As String = Nothing
            Dim Fila1_05 As String = Nothing
            Dim Fila1_06 As String = Nothing
            Dim Fila1_07 As String = Nothing
            Dim Fila1_08 As String = Nothing
            Dim Fila1_09 As String = Nothing
            Dim Fila1_10 As String = Nothing
            Dim Fila1_11 As String = Nothing
            Dim Fila1_12 As String = Nothing
            Dim Fila1_13 As String = Nothing
            Dim Fila1_14 As String = Nothing
            Dim Fila1_15 As String = Nothing
            Dim Fila1_16 As String = Nothing
            '
            Dim Fila2_01 As String = Nothing
            Dim Fila2_02 As String = Nothing
            Dim Fila2_03 As String = Nothing
            Dim Fila2_04 As String = Nothing
            Dim Fila2_05 As String = Nothing
            Dim Fila2_06 As String = Nothing
            Dim Fila2_07 As String = Nothing
            Dim Fila2_08 As String = Nothing
            '
            Dim Fila3_01 As String = Nothing
            Dim Fila3_02 As String = Nothing
            Dim Fila3_03 As String = Nothing
            Dim Fila3_04 As String = Nothing
            Dim Fila3_05 As String = Nothing
            Dim Fila3_06 As String = Nothing
            Dim Fila3_07 As String = Nothing
            Dim Fila3_08 As String = Nothing
            Dim Fila3_09 As String = Nothing
            Dim Fila3_10 As String = Nothing
            Dim Fila3_11 As String = Nothing
            Dim Fila3_12 As String = Nothing
            Dim Fila3_13 As String = Nothing
            Dim Fila3_14 As String = Nothing
            '
            Dim Fila5_01 As String = Nothing
            Dim Fila5_02 As String = Nothing
            Dim Fila5_03 As String = Nothing
            Dim Fila5_04 As String = Nothing
            Dim Fila5_05 As String = Nothing
            Dim Fila5_06 As String = Nothing
            Dim Fila5_07 As String = Nothing
            Dim Fila5_08 As String = Nothing
            Dim Fila5_09 As String = Nothing
            '
            Dim Fila6_01 As String = Nothing
            Dim Fila6_02 As String = Nothing
            Dim Fila6_03 As String = Nothing
            Dim Fila6_04 As String = Nothing

            Dim ImporteTotal As String = Nothing
            Dim Leyenda_tmp As String = Nothing
            Dim Tot_Reg As String = Nothing
            Dim Ano As String = Nothing
            Dim Mes As String = Nothing
            Dim DIA As String = Nothing
            Dim Fila3 As String = Nothing
            Dim Fila4 As String = Nothing
            Dim Fila5 As String = Nothing
            Dim Fila6 As String = Nothing
            Dim Fila7 As String = Nothing


            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.DameGeneralesBancosTableAdapter.Connection = CON
            'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "SA")
            'CON.Close()
            '
            Dim Cont As Integer
            Cont = 0
            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC UPEncabezado_Dig " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila1_01 = CStr(reader.GetValue(1))
                        Fila1_02 = CStr(reader.GetValue(2))
                        Fila1_03 = CStr(reader.GetValue(3))
                        Fila1_04 = CStr(reader.GetValue(4))
                        Fila1_05 = CStr(reader.GetValue(5))
                        Fila1_06 = CStr(reader.GetValue(6))
                        Fila1_07 = CStr(reader.GetValue(7))
                        Fila1_08 = CStr(reader.GetValue(8))
                        Fila1_09 = CStr(reader.GetValue(9))
                        Fila1_10 = CStr(reader.GetValue(10))
                        Fila1_11 = CStr(reader.GetValue(11))
                        Fila1_12 = CStr(reader.GetValue(12))
                        Fila1_13 = CStr(reader.GetValue(13))
                        Fila1_14 = CStr(reader.GetValue(14))
                        Fila1_15 = CStr(reader.GetValue(15))
                    End While
                End Using
            End With
            CONE.Close()
            CONE.Open()
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC UPDATOS_DEL_PAGO_DEL_DIGITAL " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila2_01 = CStr(reader.GetValue(1))
                        Fila2_02 = CStr(reader.GetValue(2))
                        Fila2_03 = CStr(reader.GetValue(3))
                        Fila2_04 = CStr(reader.GetValue(4))
                        Fila2_05 = CStr(reader.GetValue(5))
                        Fila2_06 = CStr(reader.GetValue(6))
                        Fila2_07 = CStr(reader.GetValue(7))
                        Fila2_08 = CStr(reader.GetValue(8))
                    End While
                End Using
            End With
            CONE.Close()
            CONE.Open()
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC UPDATOS_DEL_RECEPTOR_DEL_DIGITAL " & clv_factura
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Fila3_01 = CStr(reader.GetValue(1))
                        Fila3_02 = CStr(reader.GetValue(2))
                        Fila3_03 = CStr(reader.GetValue(3))
                        Fila3_04 = CStr(reader.GetValue(4))
                        Fila3_05 = CStr(reader.GetValue(5))
                        Fila3_06 = CStr(reader.GetValue(6))
                        Fila3_07 = CStr(reader.GetValue(7))
                        Fila3_08 = CStr(reader.GetValue(8))
                        Fila3_09 = CStr(reader.GetValue(9))
                        Fila3_10 = CStr(reader.GetValue(10))
                        Fila3_11 = CStr(reader.GetValue(11))
                        Fila3_12 = CStr(reader.GetValue(12))
                        Fila3_13 = CStr(reader.GetValue(13))
                        Fila3_14 = CStr(reader.GetValue(14))
                    End While
                End Using
            End With
            CONE.Close()


            '            
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim Fila2 As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing
            Dim nomArchi As String = Nothing
            'Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()

            'Me.FolderBrowserDialog1.ShowDialog()


            'If (result = DialogResult.OK) Then
            Rutatxt = Ruta 'FolderBrowserDialog1.SelectedPath.ToString
            nomArchi = "CFD" & "_" & clv_factura & ".txt"
            Nom_Archivo = Rutatxt + "\" + "CFD" & "_" & clv_factura & ".txt"

            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
            If fileExists = True Then
                File.Delete(Nom_Archivo)
            End If
            Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                'Encabezado = Me.DIATextBox.Text & Me.MESTextBox.Text & Me.ANOTextBox.Text & Me.HORATextBox.Text & Me.MITextBox.Text & "00" & Microsoft.VisualBasic.Strings.Space(6 - Len(Me.ContadorTextBox.Text)) & Me.ContadorTextBox.Text & Microsoft.VisualBasic.Strings.Space(16 - Len(imp1)) & imp1 & Microsoft.VisualBasic.Strings.Space(31)

                'GloEmpresa = "Gigacable de Aguascalientes S.A. de C.V."
                If Len(Trim(Fila1_01)) = 0 Or Fila1_01 = "0.00" Then Fila1_01 = "|" Else Fila1_01 = Fila1_01 & "|"
                If Len(Trim(Fila1_02)) = 0 Or Fila1_02 = "0.00" Then Fila1_02 = "|" Else Fila1_02 = Fila1_02 & "|"
                If Len(Trim(Fila1_03)) = 0 Or Fila1_03 = "0.00" Then Fila1_03 = "|" Else Fila1_03 = Fila1_03 & "|"
                If Len(Trim(Fila1_04)) = 0 Or Fila1_04 = "0.00" Then Fila1_04 = "|" Else Fila1_04 = Fila1_04 & "|"
                If Len(Trim(Fila1_05)) = 0 Or Fila1_05 = "0.00" Then Fila1_05 = "|" Else Fila1_05 = Fila1_05 & "|"
                If Len(Trim(Fila1_06)) = 0 Or Fila1_06 = "0.00" Then Fila1_06 = "|" Else Fila1_06 = Fila1_06 & "|"
                If Len(Trim(Fila1_07)) = 0 Or Fila1_07 = "0.00" Then Fila1_07 = "|" Else Fila1_07 = Fila1_07 & "|"
                If Len(Trim(Fila1_08)) = 0 Or Fila1_08 = "0.00" Then Fila1_08 = "|" Else Fila1_08 = Fila1_08 & "|"
                If Len(Trim(Fila1_09)) = 0 Or Fila1_09 = "0.00" Then Fila1_09 = "|" Else Fila1_09 = Fila1_09 & "|"
                If Len(Trim(Fila1_10)) = 0 Or Fila1_10 = "0.00" Then Fila1_10 = "|" Else Fila1_10 = Fila1_10 & "|"
                If Len(Trim(Fila1_11)) = 0 Or Fila1_11 = "0.00" Then Fila1_11 = "|" Else Fila1_11 = Fila1_11 & "|"
                If Len(Trim(Fila1_12)) = 0 Or Fila1_12 = "0.00" Then Fila1_12 = "|" Else Fila1_12 = Fila1_12 & "|"
                If Len(Trim(Fila1_13)) = 0 Or Fila1_13 = "0.00" Then Fila1_13 = "|" Else Fila1_13 = Fila1_13 & "|"
                If Len(Trim(Fila1_14)) = 0 Or Fila1_14 = "0.00" Then Fila1_14 = "|" Else Fila1_14 = Fila1_14 & "|"
                If Len(Trim(Fila1_15)) = 0 Or Fila1_15 = "0.00" Then Fila1_15 = "|" Else Fila1_15 = Fila1_15 & "|"

                '
                If Len(Trim(Fila2_01)) = 0 Or Fila2_01 = "0.00" Then Fila2_01 = "|" Else Fila2_01 = Fila2_01 & "|"
                If Len(Trim(Fila2_02)) = 0 Or Fila2_02 = "0.00" Then Fila2_02 = "|" Else Fila2_02 = Fila2_02 & "|"
                If Len(Trim(Fila2_03)) = 0 Or Fila2_03 = "0.00" Then Fila2_03 = "|" Else Fila2_03 = Fila2_03 & "|"
                If Len(Trim(Fila2_04)) = 0 Or Fila2_04 = "0.00" Then Fila2_04 = "|" Else Fila2_04 = Fila2_04 & "|"
                If Len(Trim(Fila2_05)) = 0 Or Fila2_05 = "0.00" Then Fila2_05 = "|" Else Fila2_05 = Fila2_05 & "|"
                If Len(Trim(Fila2_06)) = 0 Or Fila2_06 = "0.00" Then Fila2_06 = "|" Else Fila2_06 = Fila2_06 & "|"
                If Len(Trim(Fila2_07)) = 0 Or Fila2_07 = "0.00" Then Fila2_07 = "|" Else Fila2_07 = Fila2_07 & "|"
                If Len(Trim(Fila2_08)) = 0 Or Fila2_08 = "0.00" Then Fila2_08 = "|" Else Fila2_08 = Fila2_08 & "|"
                '
                If Len(Trim(Fila3_01)) = 0 Or Fila3_01 = "0.00" Then Fila3_01 = "|" Else Fila3_01 = Fila3_01 & "|"
                If Len(Trim(Fila3_02)) = 0 Or Fila3_02 = "0.00" Then Fila3_02 = "|" Else Fila3_02 = Fila3_02 & "|"
                If Len(Trim(Fila3_03)) = 0 Or Fila3_03 = "0.00" Then Fila3_03 = "|" Else Fila3_03 = Fila3_03 & "|"
                If Len(Trim(Fila3_04)) = 0 Or Fila3_04 = "0.00" Then Fila3_04 = "|" Else Fila3_04 = Fila3_04 & "|"
                If Len(Trim(Fila3_05)) = 0 Or Fila3_05 = "0.00" Then Fila3_05 = "|" Else Fila3_05 = Fila3_05 & "|"
                If Len(Trim(Fila3_06)) = 0 Or Fila3_06 = "0.00" Then Fila3_06 = "|" Else Fila3_06 = Fila3_06 & "|"
                If Len(Trim(Fila3_07)) = 0 Or Fila3_07 = "0.00" Then Fila3_07 = "|" Else Fila3_07 = Fila3_07 & "|"
                If Len(Trim(Fila3_08)) = 0 Or Fila3_08 = "0.00" Then Fila3_08 = "|" Else Fila3_08 = Fila3_08 & "|"
                If Len(Trim(Fila3_09)) = 0 Or Fila3_09 = "0.00" Then Fila3_09 = "|" Else Fila3_09 = Fila3_09 & "|"
                If Len(Trim(Fila3_10)) = 0 Or Fila3_10 = "0.00" Then Fila3_10 = "|" Else Fila3_10 = Fila3_10 & "|"
                If Len(Trim(Fila3_11)) = 0 Or Fila3_11 = "0.00" Then Fila3_11 = "|" Else Fila3_11 = Fila3_11 & "|"
                If Len(Trim(Fila3_12)) = 0 Or Fila3_12 = "0.00" Then Fila3_12 = "|" Else Fila3_12 = Fila3_12 & "|"
                If Len(Trim(Fila3_13)) = 0 Or Fila3_13 = "0.00" Then Fila3_13 = "|" Else Fila3_13 = Fila3_13 & "|"
                If Len(Trim(Fila3_14)) = 0 Or Fila3_14 = "0.00" Then Fila3_14 = "|" Else Fila3_14 = Fila3_14 & "|"

                Encabezado = Fila1_01 & Fila1_02 & Fila1_03 & Fila1_04 & Fila1_05 & Fila1_06 & Fila1_07 & Fila1_08 & Fila1_09 & Fila1_10 & Fila1_11 & Fila1_12 & Fila1_13 & Fila1_14 & Fila1_15
                Fila2 = Fila2_01 & Fila2_02 & Fila2_03 & Fila2_04 & Fila2_05 & Fila2_06 & Fila2_07 & Fila2_08
                Fila3 = Fila3_01 & Fila3_02 & Fila3_03 & Fila3_04 & Fila3_05 & Fila3_06 & Fila3_07 & Fila3_08 & Fila3_09 & Fila3_10 & Fila3_11 & Fila3_12 & Fila3_13
                sw.Write(Encabezado & vbNewLine)
                sw.Write(Fila2 & vbNewLine)
                sw.Write(Fila3 & vbNewLine)
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC UPDATOS_DE_LOS_CONCEPTO_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila5_01 = CStr(reader.GetValue(1))
                            Fila5_02 = CStr(reader.GetValue(2))
                            Fila5_03 = CStr(reader.GetValue(3))
                            Fila5_04 = CStr(reader.GetValue(4))
                            Fila5_05 = CStr(reader.GetValue(5))
                            Fila5_06 = CStr(reader.GetValue(6))
                            Fila5_07 = CStr(reader.GetValue(7))
                            Fila5_08 = CStr(reader.GetValue(8))
                            Fila5_09 = CStr(reader.GetValue(9))
                            If Len(Trim(Fila5_01)) = 0 Or Fila5_01 = "0.00" Then Fila5_01 = "|" Else Fila5_01 = Fila5_01 & "|"
                            If Len(Trim(Fila5_02)) = 0 Or Fila5_02 = "0.00" Then Fila5_02 = "|" Else Fila5_02 = Fila5_02 & "|"
                            If Len(Trim(Fila5_03)) = 0 Or Fila5_03 = "0.00" Then Fila5_03 = "|" Else Fila5_03 = Fila5_03 & "|"
                            If Len(Trim(Fila5_04)) = 0 Or Fila5_04 = "0.00" Then Fila5_04 = "|" Else Fila5_04 = Fila5_04 & "|"
                            If Len(Trim(Fila5_05)) = 0 Or Fila5_05 = "0.00" Then Fila5_05 = "|" Else Fila5_05 = Fila5_05 & "|"
                            If Len(Trim(Fila5_06)) = 0 Or Fila5_06 = "0.00" Then Fila5_06 = "|" Else Fila5_06 = Fila5_06 & "|"
                            If Len(Trim(Fila5_07)) = 0 Or Fila5_07 = "0.00" Then Fila5_07 = "|" Else Fila5_07 = Fila5_07 & "|"
                            If Len(Trim(Fila5_08)) = 0 Then Fila5_08 = "|" Else Fila5_08 = Fila5_08 & "|"
                            If Len(Trim(Fila5_09)) = 0 Then Fila5_09 = "|" Else Fila5_09 = Fila5_09 & "|"
                            Fila5 = Fila5_01 & Fila5_02 & Fila5_03 & Fila5_04 & Fila5_05 & Fila5_06 & Fila5_07 & Fila5_08 & Fila5_09
                            sw.Write(Fila5 & vbNewLine)
                            Fila5_01 = ""
                            Fila5_02 = ""
                            Fila5_03 = ""
                            Fila5_04 = ""
                            Fila5_05 = ""
                            Fila5_06 = ""
                            Fila5_07 = ""
                            Fila5_08 = ""
                            Fila5_09 = ""
                        End While
                    End Using
                End With
                CONE.Close()
                '06
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC UPDATOS_ADICIONALES_CONCEPTOS " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()
                '07
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC UPDATOS_DE_LOS_IMPUESTOS_TRASLADADOS_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()

                '08
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "UPDATOS_DE_LOS_IMPUESTOS_RETENIDOS_DIGITAL " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))
                            Fila6_04 = CStr(reader.GetValue(4))
                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"
                            If Len(Trim(Fila6_04)) = 0 Or Fila6_04 = "0.00" Then Fila6_04 = "|" Else Fila6_04 = Fila6_04 & "|"
                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03 & Fila6_04
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""
                            Fila6_04 = ""
                        End While
                    End Using
                End With
                CONE.Close()

                '09
                CONE.Open()
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CONE
                    .CommandText = "EXEC UPDATOS_ADICIONALES_COMPROBANTE " & clv_factura
                    .CommandType = CommandType.Text
                    .CommandTimeout = 0
                    reader = comando.ExecuteReader()
                    Using reader
                        While reader.Read
                            Fila6_01 = CStr(reader.GetValue(1))
                            Fila6_02 = CStr(reader.GetValue(2))
                            Fila6_03 = CStr(reader.GetValue(3))

                            '
                            If Len(Trim(Fila6_01)) = 0 Or Fila6_01 = "0.00" Then Fila6_01 = "|" Else Fila6_01 = Fila6_01 & "|"
                            If Len(Trim(Fila6_02)) = 0 Or Fila6_02 = "0.00" Then Fila6_02 = "|" Else Fila6_02 = Fila6_02 & "|"
                            If Len(Trim(Fila6_03)) = 0 Or Fila6_03 = "0.00" Then Fila6_03 = "|" Else Fila6_03 = Fila6_03 & "|"

                            '
                            Fila6 = Fila6_01 & Fila6_02 & Fila6_03
                            sw.Write(Fila6 & vbNewLine)
                            Fila6_01 = ""
                            Fila6_02 = ""
                            Fila6_03 = ""

                        End While
                    End Using
                End With
                CONE.Close()


                sw.Close()
            End Using
            My.Computer.Network.UploadFile(Nom_Archivo, "ftp://201.148.4.65/" & nomArchi, "usr_facturacion", "Xsw23edC", True, 10000)
            'MsgBox("El archivo se genero en la siguiente ruta : " & Nom_Archivo)
            'End If
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & Referencia_Servicio & " son Invalidos")
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub Genera_Factura_Digital(ByVal Clv_Factura As Long)
        Dim Con1 As New SqlConnection(MiConexion)
        Try
            Dim cmd As New SqlClient.SqlCommand()
            Con1.Open()
            With cmd
                .CommandText = "HAZME_FACTURA_DIGITAL"
                .Connection = Con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@Clave bigint output,@NomArchivo varchar(20) output
                Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Clv_Factura
                .Parameters.Add(prm1)
                Dim i As Integer = .ExecuteNonQuery()
            End With
            Con1.Close()
        Catch ex As Exception
            If Con1.State <> ConnectionState.Closed Then Con1.Close()
        End Try
    End Sub

    Public Sub Genera_Factura_Digital_Nota(ByVal MClv_NotadeCredito As Long)
        Dim Con1 As New SqlConnection(MiConexion)
        Try
            Dim cmd As New SqlClient.SqlCommand()
            Con1.Open()
            With cmd
                .CommandText = "HAZME_FACTURA_DIGITALNOTA_CREDITO"
                .Connection = Con1
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@Clave bigint output,@NomArchivo varchar(20) output
                Dim prm1 As New SqlParameter("@Clv_NotaCredito", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = MClv_NotadeCredito
                .Parameters.Add(prm1)
                Dim i As Integer = .ExecuteNonQuery()
            End With
            Con1.Close()
        Catch ex As Exception
            If Con1.State <> ConnectionState.Closed Then Con1.Close()
        End Try
    End Sub

End Module
