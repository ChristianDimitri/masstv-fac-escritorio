﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdelantaParcialidades
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblEncabezado = New System.Windows.Forms.Label()
        Me.nudAdelantados = New System.Windows.Forms.NumericUpDown()
        Me.gbAdelantaPagos = New System.Windows.Forms.GroupBox()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        CType(Me.nudAdelantados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAdelantaPagos.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(23, 142)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(103, 30)
        Me.btnAceptar.TabIndex = 0
        Me.btnAceptar.Text = "&Adelantar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(172, 142)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(103, 30)
        Me.btnCancelar.TabIndex = 1
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblEncabezado
        '
        Me.lblEncabezado.AutoSize = True
        Me.lblEncabezado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEncabezado.Location = New System.Drawing.Point(65, 43)
        Me.lblEncabezado.Name = "lblEncabezado"
        Me.lblEncabezado.Size = New System.Drawing.Size(182, 20)
        Me.lblEncabezado.TabIndex = 2
        Me.lblEncabezado.Text = "# Pagos a Adelantar :"
        '
        'nudAdelantados
        '
        Me.nudAdelantados.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudAdelantados.Location = New System.Drawing.Point(101, 85)
        Me.nudAdelantados.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudAdelantados.Name = "nudAdelantados"
        Me.nudAdelantados.Size = New System.Drawing.Size(80, 26)
        Me.nudAdelantados.TabIndex = 3
        Me.nudAdelantados.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'gbAdelantaPagos
        '
        Me.gbAdelantaPagos.Controls.Add(Me.lblEncabezado)
        Me.gbAdelantaPagos.Controls.Add(Me.nudAdelantados)
        Me.gbAdelantaPagos.Controls.Add(Me.btnAceptar)
        Me.gbAdelantaPagos.Controls.Add(Me.btnCancelar)
        Me.gbAdelantaPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAdelantaPagos.Location = New System.Drawing.Point(12, 12)
        Me.gbAdelantaPagos.Name = "gbAdelantaPagos"
        Me.gbAdelantaPagos.Size = New System.Drawing.Size(302, 186)
        Me.gbAdelantaPagos.TabIndex = 4
        Me.gbAdelantaPagos.TabStop = False
        Me.gbAdelantaPagos.Text = "Adelantar Pagos"
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'frmAdelantaParcialidades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(323, 205)
        Me.Controls.Add(Me.gbAdelantaPagos)
        Me.Name = "frmAdelantaParcialidades"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Adelantar Parcialidades"
        CType(Me.nudAdelantados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAdelantaPagos.ResumeLayout(False)
        Me.gbAdelantaPagos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblEncabezado As System.Windows.Forms.Label
    Friend WithEvents nudAdelantados As System.Windows.Forms.NumericUpDown
    Friend WithEvents gbAdelantaPagos As System.Windows.Forms.GroupBox
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
