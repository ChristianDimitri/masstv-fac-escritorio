﻿Public Class FrmSaldoCliente

    Private Sub FrmSaldoCliente_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(GloContratoSaldo))
        DataGridView1.DataSource = BaseII.ConsultaDT("Usp_SaldoClienteOxxo")

        DataGridView1.Columns("Concepto").Width = 180
        DataGridView1.Columns("Fecha_de_Pago").Width = 120

        DataGridView1.Columns("Fecha_de_Pago").HeaderText = "Fecha de Pago"
        DataGridView1.Columns("Por_Pagar").HeaderText = "Por Pagar"

        With DataGridView1.ColumnHeadersDefaultCellStyle
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)
        End With

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class