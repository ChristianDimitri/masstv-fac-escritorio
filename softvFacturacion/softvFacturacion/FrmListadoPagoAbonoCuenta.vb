﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmListadoPagoAbonoCuenta

    Private Sub FrmListadoPagoAbonoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.GroupBox1.BackColor = Me.BackColor
        Me.GroupBox2.BackColor = Me.BackColor
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.RadioButton1.Checked = True
        Me.RadioButton3.Checked = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim frmic As New FrmImprimirCentralizada()
        Dim DS As New DataSet
        Dim SALDADO As Integer = 0
        Dim STATUS As String = ""
        Dim rd As New ReportDocument

        If Me.RadioButton1.Checked = True Then
            STATUS = "D"
        Else
            STATUS = "B"
        End If

        If Me.RadioButton3.Checked = True Then
            SALDADO = 0
        Else
            SALDADO = 1
        End If

        DS.Clear()

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, CDate(Me.DateTimePicker1.Text))
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, CDate(Me.DateTimePicker2.Text))
        BaseII.CreateMyParameter("@STATUS", SqlDbType.VarChar, STATUS, 50)
        BaseII.CreateMyParameter("@SALDADO", SqlDbType.Int, SALDADO)
        Dim listatablas As New List(Of String)
        listatablas.Add("UspReportePagoAbonoCuenta")
        DS = BaseII.ConsultaDS("UspReportePagoAbonoCuenta", listatablas)
        rd.Load(RutaReportes + "\ReportePagoAbonoACuenta" + ".rpt")

        rd.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        rd.DataDefinition.FormulaFields("Titulo").Text = "'Reporte Pago Abono a Cuenta '"
        rd.DataDefinition.FormulaFields("subtitulo").Text = "'Reporte del " & Me.DateTimePicker1.Text & "-" & Me.DateTimePicker2.Text & "'"
        rd.SetDataSource(DS)
        frmic.rd = rd
        frmic.ShowDialog()
        Me.Close()
    End Sub
End Class