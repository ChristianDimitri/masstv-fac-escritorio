Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelFechas3

    Private Sub MuestraTipServEric()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipServEric 0,0")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraOperadores()
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataadapter As New SqlDataAdapter("EXEC MuestraOperadores", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataadapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox2.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub FrmSelFechas3_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
        If eOpRep = 1 Then
            Me.Text = "Reporte Ingresos de Clientes por un Monto"
            Me.ComboBox1.Visible = True
            Me.GroupBox3.Visible = True
            Me.GroupBox1.Text = "Tipo de Servicio"
            MuestraTipServEric()
            MuestraOperadores()
        ElseIf eOpRep = 2 Then
            Me.Text = "Reporte N�mero de Bonificaciones"
            Me.TextBox1.Visible = True
            Me.GroupBox1.Text = "No. de Bonificaciones"
        End If

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value

        If eOpRep = 1 Then

            If Len(Me.ComboBox1.Text) = 0 Then
                MsgBox("Selecciona un Tipo de Servicio.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If Len(Me.ComboBox2.Text) = 0 Then
                MsgBox("Selecciona un Operador.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            If Len(Me.TextBox2.Text) = 0 Then
                MsgBox("Teclea un Monto.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            eClvTipSer = Me.ComboBox1.SelectedValue
            eDescripcion = Me.ComboBox1.Text
            eOp = Me.ComboBox2.SelectedValue
            eOperador = Me.ComboBox2.Text
            eMonto = Me.TextBox2.Text

            Dim reporte As New FrmImprimirRepGral
            reporte.ReporteMontos()
            reporte.Show()
            Me.Close()

        ElseIf eOpRep = 2 Then

            If Len(Me.TextBox1.Text) = 0 Then
                MsgBox("Teclea un n�mero de Bonificaciones.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            eNumero = Me.TextBox1.Text

            Dim reporte As New FrmImprimirRepGral
            reporte.ReporteBonificacion()
            reporte.Show()
            Me.Close()

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    
End Class