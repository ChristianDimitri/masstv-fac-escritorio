﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmFiltroOXXO

#Region "Eventos"
    Private Sub FrmFiltroOXXO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        GroupBox1.BackColor = Color.LightGray
    End Sub
#End Region

#Region "Controles"
    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim mySelectFormula As String
        Dim FechasOXXO As String
        Fecha_ini = Me.DateTimePicker1.Value.Date
        Fecha_Fin = Me.DateTimePicker2.Value.Date
        If Me.RadioButton1.Checked = True Then
            Locop = 1
        ElseIf Me.RadioButton2.Checked = True Then
            Locop = 2
        ElseIf Me.RadioButton3.Checked = True Then
            Locop = 3
        End If

        Dim ruta As String

        Dim imprimirCentralizada As New FrmImprimirCentralizada()
        Dim reporte As New ReportDocument()
        Dim _DataSet As New DataSet
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, Fecha_ini)
            BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, Fecha_Fin)
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, Locop)
            Dim lp As List(Of String) = New List(Of String)

            lp.Add("UspReporteOXXO")
            _DataSet = BaseII.ConsultaDS("UspReporteOXXO", lp)

            ruta = RutaReportes & "\ReportesOXXO.rpt"

            reporte.Load(ruta)
            reporte.SetDataSource(_DataSet)
            mySelectFormula = "Reporte de Recepción de OXXO"
            FechasOXXO = Fecha_ini + " - " + Fecha_Fin

            reporte.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            reporte.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            reporte.DataDefinition.FormulaFields("Subtitulo").Text = "'" & FechasOXXO & "'"
           
            imprimirCentralizada.rd = reporte
            imprimirCentralizada.ShowDialog()
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
#End Region


End Class