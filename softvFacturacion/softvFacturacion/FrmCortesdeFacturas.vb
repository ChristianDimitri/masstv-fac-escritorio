Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmCortesdeFacturas
    Private customersByCityReport As ReportDocument
    'Private Const PARAMETER_FIELD_NAME As String = "Op"
    Private Titulo As String
    Private bndfiscal As Boolean = False
    Private dSetReporte As DataSet

   
    Private Sub ConfigureCrystalReportsResumen(ByVal op As Integer)

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim mySelectFormula As String = " "
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"



        If Me.ORSTATUS.Checked = True Then
            OpOrdenar = "2"
        Else
            OpOrdenar = "1"
        End If


        If (Me.ComboBox2.Text = "Cajas") Then
            GloOpRepGral = "C"

        ElseIf (Me.ComboBox2.Text = "Ventas") Then
            GloOpRepGral = "V"

        ElseIf (Me.ComboBox2.Text = "Resumen") Then
            GloOpRepGral = "T"

        End If

        Dim reportPath As String = Nothing
        If GloOpRepGral = "V" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
                    op = 11
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt"
                    op = 12
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
                    ElseIf bndfiscal = True Then

                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
                    End If
                    op = 13
                Case 4
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
                    op = 14
                Case 7
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
                    op = 101
                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros y Facturas con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas de Ventas por Cajero y Facturas con Puntos."
                    End If
                Case Else
                    reportPath = RutaReportes + "\ReporteResumenFacturas.rpt"
                    op = 10
            End Select
        ElseIf GloOpRepGral = "C" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt"
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteCorteFacturas_Sucursal(Resumen).rpt"
                    ElseIf bndfiscal = True Then

                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
                    End If
                Case 7
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
                    op = 100

                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
                    End If
                Case Else
                    reportPath = RutaReportes + "\ReporteResumenFacturas.rpt"
            End Select
        ElseIf GloOpRepGral = "T" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
                    op = 16
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
                    ElseIf bndfiscal = True Then

                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
                    End If
                    op = 16
                Case 7
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
                    op = 102
                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
                    End If
            End Select
        End If

        rDocument.Load(reportPath)

        If op = 1 Then

            If (Resumen2 = 1 And NomCajera = "TUSU") Then
                mySelectFormula = "Resumen de Facturas por Todos los Cajeros."
            Else
                mySelectFormula = "Resumen de Facturas por Cajero."
            End If
            If GloOpRepGral = "T" Then
                op = 15
            End If

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

        ElseIf op = 2 Then

            If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
                mySelectFormula = "Resumen de Facturas por Todos los Puntos de Ventas de Cajas."
            Else
                mySelectFormula = "Resumen de Facturas Punto de Venta de Cajas."
            End If

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, SelCajaResumen, NomSucursal, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

        ElseIf op = 3 Then

            If bndfiscal = False Then
                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Cajas."
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal de Cajas."
                End If

                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then
                bndfiscal = False

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Cajas."
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Cajas."
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)

            End If

        End If

        If op = 11 Then

            If (Resumen2 = 1 And NomCajera = "10000") Then
                mySelectFormula = "Resumen de Facturas por Todos los Vendedores."
            Else
                mySelectFormula = "Resumen de Facturas por Vendedor."
            End If

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, NomCajera, cbCompania.SelectedValue)

        ElseIf op = 12 Then

            If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
                mySelectFormula = "Resumen de Facturas por Todos los Puntos de Venta de Ventas."
            Else
                mySelectFormula = "Resumen de Facturas por Punto de Venta de Ventas."
            End If

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, SelCajaResumen, NomSucursal, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

        ElseIf op = 13 Then

            If bndfiscal = False Then

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Ventas."
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal de Ventas."
                End If

                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then
                bndfiscal = False

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Ventas."
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Ventas."
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)

            End If

        ElseIf op = 14 Then

            If (Resumen2 = 1 And NomCajera = "TUSU") Then
                mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros."
            Else
                mySelectFormula = "Resumen de Facturas de Ventas por Cajero."
            End If

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

        ElseIf op = 16 Then

            If bndfiscal = False Then

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales"
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal"
                End If

                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then

                bndfiscal = False

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales"
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal"
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)

            End If

        ElseIf op >= 100 Then

            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0, cbCompania.SelectedValue)

        End If

        Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin

        Dim sucursal As String = Nothing
        sucursal = "Sucursal: " + GloNomSucursal

        rDocument.PrintOptions.DissociatePageSizeAndPrinterPaperSize = False

        If GloOpRepGral = "R" Or GloOpRepGral = "T" Then
            If op = 3 Or op = 2 Or op = 16 Then
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        ElseIf GloOpRepGral <> "R" Or GloOpRepGral <> "T" Then
            If op = 3 Or op = 13 Or op = 16 Then
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        End If

        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        rDocument.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"
        rDocument.SetDataSource(dSetReporte)

        CrystalReportViewer1.ReportSource = rDocument
        CrystalReportViewer1.ShowGroupTreeButton = False
        rDocument = Nothing

        'Miguel Aleman
        '3 de Febrero de 2012
        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDataBaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim mySelectFormula As String = " "
        'Dim Fecha1 As String = " "
        'Dim Extra As String = " "
        'Dim OpOrdenar As String = "0"



        'If Me.ORSTATUS.Checked = True Then
        '    OpOrdenar = "2"
        'Else
        '    OpOrdenar = "1"
        'End If


        'If (Me.ComboBox2.Text = "Cajas") Then
        '    GloOpRepGral = "C"

        'ElseIf (Me.ComboBox2.Text = "Ventas") Then
        '    GloOpRepGral = "V"

        'ElseIf (Me.ComboBox2.Text = "Resumen") Then
        '    GloOpRepGral = "T"

        'End If

        'Dim reportPath As String = Nothing
        'If GloOpRepGral = "V" Then
        '    Select Case op
        '        Case 1
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
        '            op = 11
        '        Case 2
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt"
        '            op = 12
        '        Case 3
        '            If bndfiscal = False Then
        '                reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
        '            ElseIf bndfiscal = True Then

        '                reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
        '            End If
        '            op = 13
        '        Case 4
        '            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
        '            op = 14
        '        Case 7
        '            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
        '            op = 101
        '            If (Resumen2 = 1 And NomCajera = "TUSU") Then
        '                mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros y Facturas con Puntos."
        '            Else
        '                mySelectFormula = "Resumen de Facturas de Ventas por Cajero y Facturas con Puntos."
        '            End If
        '        Case Else
        '            reportPath = RutaReportes + "\ReporteResumenFacturas.rpt"
        '            op = 10
        '    End Select
        'ElseIf GloOpRepGral = "C" Then
        '    Select Case op
        '        Case 1
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
        '        Case 2
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt"
        '        Case 3
        '            If bndfiscal = False Then
        '                reportPath = RutaReportes + "\ReporteCorteFacturas_Sucursal(Resumen).rpt"
        '            ElseIf bndfiscal = True Then

        '                reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
        '            End If
        '        Case 7
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt"
        '            op = 100

        '            If (Resumen2 = 1 And NomCajera = "TUSU") Then
        '                mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
        '            Else
        '                mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
        '            End If
        '        Case Else
        '            reportPath = RutaReportes + "\ReporteResumenFacturas.rpt"
        '    End Select
        'ElseIf GloOpRepGral = "T" Then
        '    Select Case op
        '        Case 1
        '            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
        '        Case 2
        '            reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
        '            op = 16
        '        Case 3
        '            If bndfiscal = False Then
        '                reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt"
        '            ElseIf bndfiscal = True Then

        '                reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt"
        '            End If
        '            op = 16
        '        Case 7
        '            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
        '            op = 102
        '            If (Resumen2 = 1 And NomCajera = "TUSU") Then
        '                mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
        '            Else
        '                mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
        '            End If
        '    End Select
        'End If

        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        'If op = 1 Then
        '    If (Resumen2 = 1 And NomCajera = "TUSU") Then
        '        mySelectFormula = "Resumen de Facturas por Todos los Cajeros."
        '    Else
        '        mySelectFormula = "Resumen de Facturas por Cajero."
        '    End If
        '    If GloOpRepGral = "T" Then
        '        op = 15
        '    End If


        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, NomCajera)
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, "0")
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, Resumen)
        '    '@SelCaja1
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    If IdSistema = "LO" Then
        '        '@fecha1
        '        customersByCityReport.SetParameterValue(10, Fecha_ini)
        '        '@fecha2
        '        customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '        '@NomCajera
        '        customersByCityReport.SetParameterValue(12, NomCajera)
        '    End If
        'ElseIf op = 2 Then
        '    If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
        '        mySelectFormula = "Resumen de Facturas por Todos los Puntos de Ventas de Cajas."
        '    Else
        '        mySelectFormula = "Resumen de Facturas Punto de Venta de Cajas."
        '    End If
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, CStr(SelCajaResumen))
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, NomSucursal)
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, Resumen)
        '    '@SelCaja1
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")

        'ElseIf op = 3 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")
        '        'If bndfiscal = False Then
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        If (Resumen1 = 1 And NomSucursal = "999") Then
        '            mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Cajas."
        '        Else
        '            mySelectFormula = "Resumen de Facturas por Sucursal de Cajas."
        '        End If
        '        'ElseIf bndfiscal = True Then
        '        '    bndfiscal = False
        '        '    '@SelCaja
        '        '    customersByCityReport.SetParameterValue(4, "1")
        '        '    If (Resumen1 = 1 And NomSucursal = "999") Then
        '        '        mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Cajas."
        '        '    Else
        '        '        mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Cajas."
        '        '    End If
        '        'End If
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, NomSucursal)
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, Resumen)
        '        '@SelCaja1
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '    ElseIf bndfiscal = True Then
        '        bndfiscal = False




        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        If (Resumen1 = 1 And NomSucursal = "999") Then
        '            mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Cajas."
        '        Else
        '            mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Cajas."
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        '    End If
        'End If



        'If op = 11 Then
        '    If (Resumen2 = 1 And NomCajera = "10000") Then
        '        mySelectFormula = "Resumen de Facturas por Todos los Vendedores."
        '    Else
        '        mySelectFormula = "Resumen de Facturas por Vendedor."
        '    End If

        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, NomCajera)
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, "0")
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, Resumen)
        '    '@SelCaja1
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, NomCajera)


        'ElseIf op = 12 Then
        '    If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
        '        mySelectFormula = "Resumen de Facturas por Todos los Puntos de Venta de Ventas."
        '    Else
        '        mySelectFormula = "Resumen de Facturas por Punto de Venta de Ventas."
        '    End If
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, CStr(SelCajaResumen))
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, NomSucursal)
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, Resumen)
        '    '@SelCaja1
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")

        'ElseIf op = 13 Then

        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")

        '        'If bndfiscal = False Then
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")

        '        If (Resumen1 = 1 And NomSucursal = "999") Then
        '            mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Ventas."
        '        Else
        '            mySelectFormula = "Resumen de Facturas por Sucursal de Ventas."
        '        End If

        '        'ElseIf bndfiscal = True Then
        '        '    bndfiscal = False
        '        '@SelCaja
        '        'customersByCityReport.SetParameterValue(4, "1")
        '        'If (Resumen1 = 1 And NomSucursal = "999") Then
        '        '    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Ventas."
        '        'Else
        '        '    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Ventas."
        '        'End If

        '        'End If
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, NomSucursal)
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, Resumen)
        '        '@SelCaja1
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '    ElseIf bndfiscal = True Then
        '        bndfiscal = False

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        If (Resumen1 = 1 And NomSucursal = "999") Then
        '            mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Ventas."
        '        Else
        '            mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Ventas."
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        'End If
        'ElseIf op = 14 Then
        'If (Resumen2 = 1 And NomCajera = "TUSU") Then
        '    mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros."
        'Else
        '    mySelectFormula = "Resumen de Facturas de Ventas por Cajero."
        'End If


        ''Fec_Ini
        'customersByCityReport.SetParameterValue(0, Fecha_ini)
        ''@Fec_Fin 
        'customersByCityReport.SetParameterValue(1, Fecha_Fin)
        ''@Op
        'customersByCityReport.SetParameterValue(2, CStr(op))
        ''@SelCajera
        'customersByCityReport.SetParameterValue(3, NomCajera)
        ''@SelCaja
        'customersByCityReport.SetParameterValue(4, "0")
        '',@SelSucursal
        'customersByCityReport.SetParameterValue(5, "0")
        ''@Op_Ordena
        'customersByCityReport.SetParameterValue(6, OpOrdenar)
        ''@Resumen
        'customersByCityReport.SetParameterValue(7, Resumen)
        ''@SelCaja1
        'customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        ''@clv_vendedor
        'customersByCityReport.SetParameterValue(9, "0")

        'If IdSistema = "LO" Then
        '    '@fecha1
        '    customersByCityReport.SetParameterValue(10, Fecha_ini)
        '    '@fecha2
        '    customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '    '@NomCajera
        '    customersByCityReport.SetParameterValue(12, NomCajera)
        'End If

        'ElseIf op = 16 Then
        'If bndfiscal = False Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, "0")
        '    'If bndfiscal = False Then
        '    '    '@SelCaja

        '    customersByCityReport.SetParameterValue(4, "0")
        '    If (Resumen1 = 1 And NomSucursal = "999") Then
        '        mySelectFormula = "Resumen de Facturas por Todas las Sucursales"
        '    Else
        '        mySelectFormula = "Resumen de Facturas por Sucursal"
        '    End If
        '    'ElseIf bndfiscal = True Then
        '    '    bndfiscal = False
        '    '    '@SelCaja
        '    '    customersByCityReport.SetParameterValue(4, "1")
        '    '    If (Resumen1 = 1 And NomSucursal = "999") Then
        '    '        mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales"
        '    '    Else
        '    '        mySelectFormula = "Resumen de Facturas Fiscales por Sucursal"
        '    '    End If
        '    'End If
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, NomSucursal)
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, Resumen)
        '    '@SelCaja1
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        'ElseIf bndfiscal = True Then
        '    bndfiscal = False

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        If (Resumen1 = 1 And NomSucursal = "999") Then
        '            mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales"
        '        Else
        '            mySelectFormula = "Resumen de Facturas Fiscales por Sucursal"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)
        '        End If
        'ElseIf op >= 100 Then

        ''Fec_Ini
        'customersByCityReport.SetParameterValue(0, Fecha_ini)
        ''@Fec_Fin 
        'customersByCityReport.SetParameterValue(1, Fecha_Fin)
        ''@Op
        'customersByCityReport.SetParameterValue(2, CStr(op))
        ''@SelCajera
        'customersByCityReport.SetParameterValue(3, NomCajera)
        ''@SelCaja
        'customersByCityReport.SetParameterValue(4, "0")
        '',@SelSucursal
        'customersByCityReport.SetParameterValue(5, "0")
        ''@Op_Ordena
        'customersByCityReport.SetParameterValue(6, OpOrdenar)
        ''@Resumen
        'customersByCityReport.SetParameterValue(7, Resumen)
        ''@SelCaja1
        'customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        ''@clv_vendedor
        'customersByCityReport.SetParameterValue(9, "0")
        'If IdSistema = "LO" Then
        '    '@fecha1
        '    customersByCityReport.SetParameterValue(10, Fecha_ini)
        '    '@fecha2
        '    customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '    '@NomCajera
        '    customersByCityReport.SetParameterValue(12, NomCajera)
        'End If
        'End If



        ''Creacion de la Cabecera Del Reporte

        'Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin

        'Dim sucursal As String = Nothing
        'sucursal = "Sucursal: " + GloNomSucursal

        'If GloOpRepGral = "R" Or GloOpRepGral = "T" Then
        '    If op = 3 Or op = 2 Or op = 16 Then
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '    Else
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    End If
        'ElseIf GloOpRepGral <> "R" Or GloOpRepGral <> "T" Then
        '    If op = 3 Or op = 13 Or op = 16 Then
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '    Else
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    End If
        'End If



        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        'customersByCityReport.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"


        'CrystalReportViewer1.ReportSource = customersByCityReport
        'SetDBLogonForReport2(connectionInfo)
        'Me.CrystalReportViewer1.ShowGroupTreeButton = False
        'customersByCityReport = Nothing
    End Sub


    Private Sub ConfigureCrystalReportsFac(ByVal op As Integer, ByVal Titulo As String)

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        If GloOpRepGral = "V" Then
            Select Case op
                Case 0 : op = 10
                    Extra = ""
                Case 1 : op = 11
                    Extra = "Vendedor: " & ExtraT
                Case 2 : op = 12
                    Extra = "Caja: " & ExtraT
                Case 3 : op = 13
                    Extra = "Sucursal: " & ExtraT
                Case 4 : op = 14
                    Extra = "Cajera(o): " & ExtraT
                Case 7 : op = 101
                    Extra = "Cajera(o):" & ExtraT
            End Select
        ElseIf GloOpRepGral = "C" Then
            Select Case op
                Case 0
                    Extra = " "
                Case 1
                    Extra = "Cajera(o): " & ExtraT
                Case 2
                    Extra = "Caja: " & ExtraT
                Case 3
                    Extra = "Sucursal: " & ExtraT
                Case 7
                    Extra = "Cajera(o) :" & ExtraT
                    op = 100
            End Select
        ElseIf GloOpRepGral = "T" Then
            Select Case op
                Case 1
                    Extra = "Cajera(o): " & ExtraT
                    op = 15
                Case 2
                    Extra = "Sucursal: " & ExtraT
                    op = 16
                Case 3
                    Extra = "Sucursal: " & ExtraT
                    op = 16
                Case 7
                    Extra = "Cajera(o): " & ExtraT
                    op = 102
            End Select

        End If

        If Me.ORSTATUS.Checked = True Then
            OpOrdenar = "2"
        Else
            OpOrdenar = "1"
        End If

        Dim reportPath As String = Nothing
        If op = 0 Or op = 10 Then
            If bndfiscal = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(general).rpt"
            ElseIf bndfiscal = True Then
                reportPath = RutaReportes + "\CorteFacturasFiscal.rpt"
            End If
        ElseIf op = 12 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajas).rpt"
        ElseIf op = 3 Or op = 13 Or op = 16 Then
            If bndfiscal = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(sucursales).rpt"
            ElseIf bndfiscal = True Then
                reportPath = RutaReportes + "\CorteFacturasFiscalSucursal.rpt"
            End If

        ElseIf op = 14 Or op = 15 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1.rpt"
        ElseIf op = 1 Then
            If Locbndcortedet = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt"
                'reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)LO.rpt"
            Else
                'reporte nuevo
                reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt"
            End If
        ElseIf op = 11 Then
            If Locbndcortedet = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(vendedores).rpt"
            ElseIf Locbndcortedet = True Then
                reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt"
            End If
        ElseIf op = 2 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajas)1.rpt"
        ElseIf op >= 100 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt"
        End If

        



        If op = 0 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte General de Cajas"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, 0, OpOrdenar, False, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then

                mySelectFormula = "Corte General de Cajas de Facturas Fiscales"
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If
                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, 0, OpOrdenar, cbCompania.SelectedValue)

            End If
        ElseIf op = 1 Then

            If Locbndcortedet = False Then

                mySelectFormula = "Corte por Cajero(a)"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf Locbndcortedet = True Then

                mySelectFormula = "Corte Detallado por Cajero(a)"
                corte_facturas_detallado(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, cbCompania.SelectedValue)

            End If

        ElseIf op = 2 Then

            mySelectFormula = "Corte por Punto de Venta de Cajas"
            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, NomCaja, 0, OpOrdenar, 0, 0, 0, cbCompania.SelectedValue)

        ElseIf op = 3 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Cajas"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, 0, 0, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then
                bndfiscal = False

                mySelectFormula = "Corte Fiscal por Sucursal de Cajas"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)

            End If
        End If

        If op = 10 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte General de Ventas"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, 0, OpOrdenar, 0, Resumen2, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then
                mySelectFormula = "Corte General de Ventas de Facturas Fiscales"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, 0, OpOrdenar, cbCompania.SelectedValue)

            End If

        ElseIf op = 11 Then
            If Locbndcortedet = False Then

                mySelectFormula = "Corte por Vendedor(a)"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, Resumen2, NomCajera, cbCompania.SelectedValue)

            ElseIf Locbndcortedet = True Then

                mySelectFormula = "Corte Detallado por Vendedor(a)"
                corte_facturas_detallado(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, NomCajera, cbCompania.SelectedValue)

            End If
        ElseIf op = 12 Then

            mySelectFormula = "Corte por Punto de Venta de Ventas"
            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, NomCaja, 0, OpOrdenar, 0, 0, 0, cbCompania.SelectedValue)
         
        ElseIf op = 13 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Ventas"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, 0, 0, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then
                mySelectFormula = "Corte Fiscal por Sucursal de Ventas"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)
            End If
        ElseIf op = 14 Then

            mySelectFormula = "Corte por Cajero(a) de Ventas"
            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, Resumen2, 0, cbCompania.SelectedValue)

        ElseIf op = 15 Then

            mySelectFormula = "Corte por Cajero(a)"
            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, Resumen2, 0, cbCompania.SelectedValue)
    
        ElseIf op = 16 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Ventas y Cajas"
                Corte_de_facturas(Fecha_ini, Fecha_Fin, op, 0, 0, NomSucursal, OpOrdenar, 0, 0, 0, cbCompania.SelectedValue)

            ElseIf bndfiscal = True Then

                mySelectFormula = "Corte Fiscal Por Sucursal de Ventas y Cajas"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar, cbCompania.SelectedValue)

            End If
        End If

        If op >= 100 Then

            mySelectFormula = "Corte por Cajero(a) y Facturas Con Puntos"
            Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0, Resumen2, 0, cbCompania.SelectedValue)

        End If

        Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin


        Dim sucursal As String = Nothing
        sucursal = "Sucursal: " + GloNomSucursal

        rDocument.PrintOptions.DissociatePageSizeAndPrinterPaperSize = False

        If GloOpRepGral = "T" Or GloOpRepGral = "R" Then
            If op = 3 Or op = 2 Or op = 16 Then
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        ElseIf GloOpRepGral <> "T" Or GloOpRepGral <> "R" Then
            If op = 3 Or op = 13 Or op = 16 Or op = 12 Or op = 10 Then
                If op = 10 And bndfiscal = False Then
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                ElseIf op = 10 And bndfiscal = True Then
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If

                If op = 3 Or op = 13 Or op = 16 And bndfiscal = True Then
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
            Else
                rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If

            If op = 11 Then
                If Locbndcortedet = True Then
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Else
                    rDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                End If
            End If
        End If


        If bndfiscal = True Then
            bndfiscal = False
        End If

        rDocument.Load(reportPath)
        rDocument.SetDataSource(dSetReporte)
        rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        rDocument.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        rDocument.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        rDocument.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"

        Locbndcortedet = False
        'rDocument.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        CrystalReportViewer1.ReportSource = rDocument
        'rDocument = Nothing

        'Miguel Alem�n
        '3 de Febrero de 2012

        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDataBaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        'Dim mySelectFormula As String = Nothing
        'Dim Fecha1 As String = " "
        'Dim Extra As String = " "
        'Dim OpOrdenar As String = "0"

        'If GloOpRepGral = "V" Then
        '    Select Case op
        '        Case 0 : op = 10
        '            Extra = ""
        '        Case 1 : op = 11
        '            Extra = "Vendedor: " & ExtraT
        '        Case 2 : op = 12
        '            Extra = "Caja: " & ExtraT
        '        Case 3 : op = 13
        '            Extra = "Sucursal: " & ExtraT
        '        Case 4 : op = 14
        '            Extra = "Cajera(o): " & ExtraT
        '        Case 7 : op = 101
        '            Extra = "Cajera(o):" & ExtraT
        '    End Select
        'ElseIf GloOpRepGral = "C" Then
        '    Select Case op
        '        Case 0
        '            Extra = " "
        '        Case 1
        '            Extra = "Cajera(o): " & ExtraT
        '        Case 2
        '            Extra = "Caja: " & ExtraT
        '        Case 3
        '            Extra = "Sucursal: " & ExtraT
        '        Case 7
        '            Extra = "Cajera(o) :" & ExtraT
        '            op = 100
        '    End Select
        'ElseIf GloOpRepGral = "T" Then
        '    Select Case op
        '        Case 1
        '            Extra = "Cajera(o): " & ExtraT
        '            op = 15
        '        Case 2
        '            Extra = "Sucursal: " & ExtraT
        '            op = 16
        '        Case 3
        '            Extra = "Sucursal: " & ExtraT
        '            op = 16
        '        Case 7
        '            Extra = "Cajera(o): " & ExtraT
        '            op = 102
        '    End Select

        'End If

        'If Me.ORSTATUS.Checked = True Then
        '    OpOrdenar = "2"
        'Else
        '    OpOrdenar = "1"
        'End If

        'Dim reportPath As String = Nothing
        'If op = 0 Or op = 10 Then
        '    If bndfiscal = False Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(general).rpt"
        '    ElseIf bndfiscal = True Then
        '        reportPath = RutaReportes + "\CorteFacturasFiscal.rpt"
        '    End If
        'ElseIf op = 12 Then
        '    reportPath = RutaReportes + "\ReporteCorteFacturas(cajas).rpt"
        'ElseIf op = 3 Or op = 13 Or op = 16 Then
        '    If bndfiscal = False Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(sucursales).rpt"
        '    ElseIf bndfiscal = True Then
        '        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal.rpt"
        '    End If

        '    ElseIf op = 14 Or op = 15 Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1.rpt"
        '    ElseIf op = 1 Then
        '        If Locbndcortedet = False Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt"
        '        'reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)LO.rpt"
        '        Else
        '            'reporte nuevo
        '            reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt"
        '        End If
        '    ElseIf op = 11 Then
        '        If Locbndcortedet = False Then
        '            reportPath = RutaReportes + "\ReporteCorteFacturas(vendedores).rpt"
        '        ElseIf Locbndcortedet = True Then
        '            reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt"
        '        End If
        '    ElseIf op = 2 Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(cajas)1.rpt"
        '    ElseIf op >= 100 Then
        '        reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt"
        '    End If
        '    customersByCityReport.Load(reportPath)
        '    SetDBLogonForReport(connectionInfo, customersByCityReport)

        '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'If op = 0 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")
        '        '@SelCaja
        '        'If bndfiscal = True Then
        '        '    bndfiscal = False
        '        '    'Titulo del Reporte
        '        '    mySelectFormula = "Corte General de Cajas de Facturas Fiscales"
        '        '    customersByCityReport.SetParameterValue(4, "1")
        '        'ElseIf bndfiscal = False Then
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte General de Cajas"
        '        customersByCityReport.SetParameterValue(4, "0")
        '        'End If

        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '        'If IdSistema = "LO" Then
        '        '    '@fecha1
        '        '    customersByCityReport.SetParameterValue(10, Fecha_ini)
        '        '    '@fecha2
        '        '    customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '        '    '@NomCajera
        '        '    customersByCityReport.SetParameterValue(12, "TUSU")
        '        'End If
        '    ElseIf bndfiscal = True Then

        '        mySelectFormula = "Corte General de Cajas de Facturas Fiscales"
        '        ' @op int,@FechaIni datetime,@FechaFin datetime,@Tipo varchar(1),@SelSucursal int,@Op_Ordena int

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, 0)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)


        '    End If
        'ElseIf op = 1 Then
        '    If Locbndcortedet = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, NomCajera)
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")

        '        If IdSistema = "LO" Then
        '            '@fecha1
        '            customersByCityReport.SetParameterValue(10, Fecha_ini)
        '            '@fecha2
        '            customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '            '@NomCajera
        '            customersByCityReport.SetParameterValue(12, NomCajera)
        '        End If

        '        'Titulo del Reporte
        '        mySelectFormula = "Corte por Cajero(a)"
        '    ElseIf Locbndcortedet = True Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, NomCajera)
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(7, "0")
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte Detallado por Cajero(a)"
        '    End If


        'ElseIf op = 2 Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, NomCaja)
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(8, "0")
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    'Titulo del Reporte
        '    mySelectFormula = "Corte por Punto de Venta de Cajas"

        'ElseIf op = 3 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")
        '        '@SelCaja
        '        'If bndfiscal = False Then
        '        customersByCityReport.SetParameterValue(4, "0")
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte por Sucursal de Cajas"
        '        'ElseIf bndfiscal = True Then
        '        '    bndfiscal = False
        '        '    customersByCityReport.SetParameterValue(4, "1")
        '        '    'Titulo del Reporte
        '        '    mySelectFormula = "Corte Fiscal por Sucursal de Cajas"
        '        'End If
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, NomSucursal)
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, "0")
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '    ElseIf bndfiscal = True Then
        '        bndfiscal = False

        '        mySelectFormula = "Corte Fiscal por Sucursal de Cajas"

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        '    End If
        'End If

        ''Ventas
        'If op = 10 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        'customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")
        '        '@SelCaja
        '        'Titulo del Reporte

        '        mySelectFormula = "Corte General de Ventas"
        '        customersByCityReport.SetParameterValue(4, "0")

        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '        If IdSistema = "LO" Then
        '            '@fecha1
        '            customersByCityReport.SetParameterValue(10, Fecha_ini)
        '            '@fecha2
        '            customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '            '@NomCajera
        '            customersByCityReport.SetParameterValue(12, "TUSU")
        '        End If
        '    ElseIf bndfiscal = True Then
        '        mySelectFormula = "Corte General de Ventas de Facturas Fiscales"

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, 0)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        '    End If



        'ElseIf op = 11 Then
        '    If Locbndcortedet = False Then

        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, CStr(NomCajera))
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, NomCajera)
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte por Vendedor(a)"
        '    ElseIf Locbndcortedet = True Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, CStr(NomCajera))
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, "0")
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(7, NomCajera)
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte Detallado por Vendedor(a)"
        '    End If
        'ElseIf op = 12 Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, NomCaja)
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(8, "0")
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    'Titulo del Reporte
        '    mySelectFormula = "Corte por Punto de Venta de Ventas"
        'ElseIf op = 13 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")
        '        '@SelCaja
        '        'If bndfiscal = False Then
        '        customersByCityReport.SetParameterValue(4, "0")
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte por Sucursal de Ventas"
        '        'ElseIf bndfiscal = True Then
        '        '    bndfiscal = False
        '        '    customersByCityReport.SetParameterValue(4, "1")
        '        '    'Titulo del Reporte
        '        '    mySelectFormula = "Corte Fiscal por Sucursal de Ventas"
        '        'End If
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, NomSucursal)
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, "0")
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '    ElseIf bndfiscal = True Then
        '        'bndfiscal = False

        '        mySelectFormula = "Corte Fiscal por Sucursal de Ventas"

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        '    End If


        'ElseIf op = 14 Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, NomCajera)
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, "0")
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    'Titulo del Reporte
        '    mySelectFormula = "Corte por Cajero(a) de Ventas"
        '    If IdSistema = "LO" Then
        '        '@fecha1
        '        customersByCityReport.SetParameterValue(10, Fecha_ini)
        '        '@fecha2
        '        customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '        '@NomCajera
        '        customersByCityReport.SetParameterValue(12, NomCajera)
        '    End If
        'ElseIf op = 15 Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, NomCajera)
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, "0")
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    'Titulo del Reporte
        '    mySelectFormula = "Corte por Cajero(a)"
        '    If IdSistema = "LO" Then
        '        '@fecha1
        '        customersByCityReport.SetParameterValue(10, Fecha_ini)
        '        '@fecha2
        '        customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '        '@NomCajera
        '        customersByCityReport.SetParameterValue(12, NomCajera)
        '    End If
        'ElseIf op = 16 Then
        '    If bndfiscal = False Then
        '        'Fec_Ini
        '        customersByCityReport.SetParameterValue(0, Fecha_ini)
        '        '@Fec_Fin 
        '        customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '        '@Op
        '        customersByCityReport.SetParameterValue(2, CStr(op))
        '        '@SelCajera
        '        customersByCityReport.SetParameterValue(3, "0")

        '        'If bndfiscal = False Then
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(4, "0")
        '        'Titulo del Reporte
        '        mySelectFormula = "Corte por Sucursal de Ventas y Cajas"
        '        'ElseIf bndfiscal = True Then
        '        '    bndfiscal = False
        '        '    '@SelCaja
        '        '    customersByCityReport.SetParameterValue(4, "1")
        '        '    'Titulo del Reporte
        '        '    mySelectFormula = "Corte Fiscal Por Sucursal de Ventas y Cajas"
        '        'End If
        '        ',@SelSucursal
        '        customersByCityReport.SetParameterValue(5, NomSucursal)
        '        '@Op_Ordena
        '        customersByCityReport.SetParameterValue(6, OpOrdenar)
        '        '@Resumen
        '        customersByCityReport.SetParameterValue(7, "0")
        '        '@SelCaja
        '        customersByCityReport.SetParameterValue(8, "0")
        '        '@clv_vendedor
        '        customersByCityReport.SetParameterValue(9, "0")
        '    ElseIf bndfiscal = True Then
        '        'bndfiscal = False

        '        mySelectFormula = "Corte Fiscal Por Sucursal de Ventas y Cajas"

        '        If GloOpRepGral = "T" Then
        '            GloOpRepGral = "R"
        '        End If

        '        customersByCityReport.SetParameterValue(0, 0)
        '        customersByCityReport.SetParameterValue(1, Fecha_ini)
        '        customersByCityReport.SetParameterValue(2, Fecha_Fin)
        '        customersByCityReport.SetParameterValue(3, GloOpRepGral)
        '        customersByCityReport.SetParameterValue(4, NomSucursal)
        '        customersByCityReport.SetParameterValue(5, OpOrdenar)

        '    End If
        'End If

        'If op >= 100 Then
        '    'Fec_Ini
        '    customersByCityReport.SetParameterValue(0, Fecha_ini)
        '    '@Fec_Fin 
        '    customersByCityReport.SetParameterValue(1, Fecha_Fin)
        '    '@Op
        '    customersByCityReport.SetParameterValue(2, CStr(op))
        '    '@SelCajera
        '    customersByCityReport.SetParameterValue(3, NomCajera)
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(4, "0")
        '    ',@SelSucursal
        '    customersByCityReport.SetParameterValue(5, "0")
        '    '@Op_Ordena
        '    customersByCityReport.SetParameterValue(6, OpOrdenar)
        '    '@Resumen
        '    customersByCityReport.SetParameterValue(7, "0")
        '    '@SelCaja
        '    customersByCityReport.SetParameterValue(8, CStr(Resumen2))
        '    '@clv_vendedor
        '    customersByCityReport.SetParameterValue(9, "0")
        '    'Titulo del Reporte
        '    mySelectFormula = "Corte por Cajero(a) y Facturas Con Puntos"
        '    If IdSistema = "LO" Then
        '        '@fecha1
        '        customersByCityReport.SetParameterValue(10, Fecha_ini)
        '        '@fecha2
        '        customersByCityReport.SetParameterValue(11, Fecha_Fin)
        '        '@NomCajera
        '        customersByCityReport.SetParameterValue(12, NomCajera)
        '    End If
        'End If


        '    'Creacion de la Cabecera Del Reporte
        '    Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin


        '    Dim sucursal As String = Nothing
        '    sucursal = "Sucursal: " + GloNomSucursal

        '    ''If (op < 4 And op < 10) Or op = 14 Or op = 15 Or op = 16 Then
        '    ''    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    ''Else
        '    ''    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        ' ''End If


        'If GloOpRepGral = "T" Or GloOpRepGral = "R" Then
        '    If op = 3 Or op = 2 Or op = 16 Then
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '    Else
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    End If
        'ElseIf GloOpRepGral <> "T" Or GloOpRepGral <> "R" Then
        '    If op = 3 Or op = 13 Or op = 16 Or op = 12 Or op = 10 Then
        '        If op = 10 And bndfiscal = False Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '        ElseIf op = 10 And bndfiscal = True Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '        End If

        '        If op = 3 Or op = 13 Or op = 16 And bndfiscal = True Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '        Else
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '        End If
        '    Else
        '        customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '    End If

        '    If op = 11 Then
        '        If Locbndcortedet = True Then
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
        '        Else
        '            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        '        End If
        '    End If
        'End If


        'If bndfiscal = True Then
        '    bndfiscal = False
        'End If




        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        'customersByCityReport.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"


        'Locbndcortedet = False

        'CrystalReportViewer1.ReportSource = customersByCityReport
        ''Me.CrystalReportViewer1.ShowGroupTreeButton = False
        'SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub





    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If cbCompania.Text.Length = 0 Then
            MessageBox.Show("Selecciona una Compa��a.")
            Exit Sub
        End If
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Reporte_cortesTableAdapter.Connection = CON
        Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
        CON.Close()
        If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            'ConfigureCrystalReportsFac(op, Titulo)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 4) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 5) Then
            op = 0
            bndfiscal = True
            My.Forms.FrmOpRep.Show()
            Titulo = "General de Facturas Fiscales"
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 6) Then
            op = 3
            bndfiscal = True
            My.Forms.FrmOpRep.Show()
            Titulo = "General De Facturas Fiscales por Sucursal"
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 7) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            'bndreportecajeropuntos = True
            My.Forms.FrmOpRep.Show()
            Titulo = "Reporte Por Cajero y Facturas Con Puntos"

            '            MsgBox("En Proceso De Desarrollo", MsgBoxStyle.Information)

            '           Exit Sub
        Else
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        End If
    End Sub

    Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If cbCompania.Text.Length = 0 Then
                MessageBox.Show("Selecciona una Compa��a.")
                Exit Sub
            End If
            If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
                'ConfigureCrystalReportsFac(op, Titulo)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 4) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 5) Then
                op = 0
                bndfiscal = True
                My.Forms.FrmOpRep.Show()
                Titulo = "General de Facturas Fiscales"
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 6) Then
                op = 3
                bndfiscal = True
                My.Forms.FrmOpRep.Show()
                Titulo = "General De Facturas Fiscales por Sucursal"
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 7) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                'bndreportecajeropuntos = True
                My.Forms.FrmOpRep.Show()
                Titulo = "Reporte Por Cajero y Facturas Con Puntos"
            Else
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            End If

        End If
    End Sub



    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        myReportDocument.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmCortesdeFacturas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If BanderaReporte = True And Resumen = False Then
            BanderaReporte = False
            Me.ConfigureCrystalReportsFac(op, Titulo)
        ElseIf Resumen3 = True Then
            Resumen3 = False
            If Resumen = True And (Resumen1 = 1 Or Resumen2 = 1) Then
                Me.ConfigureCrystalReportsResumen(op)

            End If
        End If
    End Sub

    Private Sub FrmCortesdeFacturas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientos_arnoldo.Borra_Reporte_cortes' Puede moverla o quitarla seg�n sea necesario.
        Me.Borra_Reporte_cortesTableAdapter.Connection = CON
        Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter.Connection = CON
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter.Fill(Me.Procedimientos_arnoldo.MUESTRAOP_REPORTES_FACTURA, 0)
        GloOpRepGral = "C"
        Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
        Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 0)
        CON.Close()
        MUESTRAtblCompanias(2, 0)
        Me.CrystalReportViewer1.RefreshReport()
        Me.CrystalReportViewer1.RefreshReport()
    End Sub

    'Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
    '    Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
    '    If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        'ConfigureCrystalReportsFac(op, Titulo)
    '    ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    Else
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    End If
    'End Sub



    Private Sub FrmCortesdeFacturas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp

    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub


    Private Sub ComboBox1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If (Me.ComboBox2.Text = "Cajas") Then
            GloOpRepGral = "C"
            Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 0)
        ElseIf (Me.ComboBox2.Text = "Ventas") Then
            GloOpRepGral = "V"
            Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 1)
        ElseIf (Me.ComboBox2.Text = "Resumen") Then
            GloOpRepGral = "T"
            Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 2)
        End If
        CON.Close()
    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '            'ConfigureCrystalReportsFac(op, Titulo)
    '        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        Else
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        End If

    '    End If
    'End Sub

    Private Sub MUESTRAtblCompanias(ByVal Op As Integer, ClvCompania As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MUESTRAtblCompanias " + Op.ToString() + ", " + ClvCompania.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Try
            dAdapter.Fill(dTable)
            cbCompania.DataSource = dTable
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Corte_de_facturas(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Op As Integer, ByVal SelCajera As String, ByVal SelCaja As Integer, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer, ByVal Resumen As Boolean, ByVal SelCaja1 As Integer, ByVal Clv_Vendedor As Integer, ByVal ClvCompania As Integer)
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim sBuilder As New StringBuilder("EXEC Corte_de_facturas '" + FechaIni + "', '" + FechaFin + "', " + Op.ToString + ", '" + SelCajera + "', " + SelCaja.ToString() + ", " + SelSucursal.ToString() + ", " + Op_Ordena.ToString() + ", " + Resumen.ToString() + ", " + SelCaja1.ToString() + ", " + Clv_Vendedor.ToString() + ", " + ClvCompania.ToString())
        'Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)



        'Dim dSet As New DataSet
        Try

            'dAdapter.Fill(dSet)
            'dSet.Tables(0).TableName = "Corte_de_facturas"
            'dSet.Tables(1).TableName = "General"
            'dSet.Tables(2).TableName = "Parametros"
            'dSet.Tables(3).TableName = "Reporte_Cortes_Liga"
            'dSetReporte = Nothing

            'dSetReporte = dSet
            '@FechaIni DateTime, @FechaFin DateTime,@Op int,@SelCajera varchar(250),
            '@SelCaja int,@SelSucursal int,@Op_Ordena int,@Resumen bit,@SelCaja1 INT,
            '@clv_vendedor int, @CLVCOMPANIA INT

            Dim listaTablas As New List(Of String)
            listaTablas.Add("Corte_de_facturas")
            listaTablas.Add("General")
            listaTablas.Add("Parametros")
            listaTablas.Add("Reporte_Cortes_Liga")
            ' FechaIni + "', '" + FechaFin + "', " + Op.ToString + ", 
            '" + SelCajera + "', " + SelCaja.ToString() + ", " + SelSucursal.ToString() + ",
            ' " + Op_Ordena.ToString() + ", " + Resumen.ToString() + ", " + SelCaja1.ToString() + ", " + Clv_Vendedor.ToString() + ", " + ClvCompania.ToString())
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, CInt(Op.ToString))
            BaseII.CreateMyParameter("@SelCajera", SqlDbType.VarChar, SelCajera, 250)
            BaseII.CreateMyParameter("@SelCaja", SqlDbType.Int, CInt(SelCaja.ToString()))
            BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, CInt(SelSucursal.ToString()))
            BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, CInt(Op_Ordena.ToString()))
            BaseII.CreateMyParameter("@Resumen", SqlDbType.Bit, CBool(Resumen.ToString()))
            BaseII.CreateMyParameter("@SelCaja1", SqlDbType.Int, CInt(SelCaja1.ToString()))
            BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, CInt(Clv_Vendedor.ToString()))
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CInt(ClvCompania.ToString()))
            


            dSetReporte = BaseII.ConsultaDS("Corte_de_facturas", listaTablas)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Corte_Facturas_Fiscal(ByVal Op As Integer, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Tipo As String, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer, ByVal ClvCompania As Integer)
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim sBuilder As New StringBuilder("EXEC Corte_Facturas_Fiscal " + Op.ToString() + ", '" + FechaIni + "', '" + FechaFin + "', '" + Tipo + "', " + SelSucursal.ToString() + ", " + Op_Ordena.ToString() + ", " + ClvCompania.ToString())
        'Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        'Dim dSet As New DataSet
        Try
            'dAdapter.Fill(dSet)
            'dSet.Tables(0).TableName = "Corte_Facturas_Fiscal"
            'dSet.Tables(1).TableName = "General"
            'dSet.Tables(2).TableName = "Parametros"
            'dSetReporte = Nothing
            'dSetReporte = dSet
            Dim listaTablas As New List(Of String)
            listaTablas.Add("Corte_Facturas_Fiscal")
            listaTablas.Add("General")
            listaTablas.Add("Parametros")
            '" + Op.ToString() + ", '" + FechaIni + "', '" + FechaFin + "', '" + Tipo + "',
            ' " + SelSucursal.ToString() + ", " + Op_Ordena.ToString() + ", " + ClvCompania.ToString()
            '@op int,@FechaIni datetime,@FechaFin datetime,@Tipo varchar(1),@SelSucursal int,@Op_Ordena int,
            '@CLVCOMPANIA INT
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, CInt(Op.ToString))
            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
            BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, Tipo, 1)
            BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, CInt(SelSucursal.ToString()))
            BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, CInt(Op_Ordena.ToString()))
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CInt(ClvCompania.ToString()))

            dSetReporte = BaseII.ConsultaDS("Corte_Facturas_Fiscal", listaTablas)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub corte_facturas_detallado(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Op As Integer, ByVal SelCajera As String, ByVal SelCaja As Integer, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer, ByVal Clv_Vendedor As Integer, ByVal ClvCompania As Integer)
        'Dim conexion As New SqlConnection(MiConexion)
        'Dim sBuilder As New StringBuilder("EXEC corte_facturas_detallado '" + FechaIni + "', '" + FechaFin + "', " + Op.ToString + ", '" + SelCajera + "', " + SelCaja.ToString() + ", " + SelSucursal.ToString() + ", " + Op_Ordena.ToString() + ", " + Clv_Vendedor.ToString() + ", " + ClvCompania.ToString())
        'Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        'Dim dSet As New DataSet
        Try
            'dAdapter.Fill(dSet)
            'dSet.Tables(0).TableName = "corte_facturas_detallado"
            'dSet.Tables(1).TableName = "General"
            'dSet.Tables(2).TableName = "Parametros"
            'dSetReporte = Nothing
            'dSetReporte = dSet

            Dim listaTablas As New List(Of String)
            listaTablas.Add("corte_facturas_detallado")
            listaTablas.Add("General")
            listaTablas.Add("Parametros")
            '" + FechaIni + "', '" + FechaFin + "', " + Op.ToString + ", '" + SelCajera + "', " + SelCaja.ToString() + ", " + SelSucursal.ToString() + ", " + Op_Ordena.ToString() + ", " + Clv_Vendedor.ToString() + ", " + ClvCompania.ToString())
            '@FechaIni DateTime, @FechaFin DateTime,@Op int,@SelCajera varchar(250),@SelCaja int,@SelSucursal int,
            '@Op_Ordena int, @clv_vendedor bigint, @CLVCOMPANIA INT
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
            BaseII.CreateMyParameter("@Op", SqlDbType.Int, CInt(Op.ToString))
            BaseII.CreateMyParameter("@SelCajera", SqlDbType.VarChar, SelCajera, 250)
            BaseII.CreateMyParameter("@SelCaja", SqlDbType.Int, CInt(SelCaja.ToString()))
            BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, CInt(SelSucursal.ToString()))
            BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, CInt(Op_Ordena.ToString()))
            BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.BigInt, CInt(Clv_Vendedor.ToString()))
            BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, CInt(ClvCompania.ToString()))



            dSetReporte = BaseII.ConsultaDS("corte_facturas_detallado", listaTablas)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub cbCompania_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCompania.SelectedIndexChanged
        If cbCompania.Text.Length = 0 Then
            Exit Sub
        End If
        GloClvCompania = cbCompania.SelectedValue
    End Sub
End Class