﻿

Public Class FrmVerDetalledelCobro

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub FrmVerDetalledelCobro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, gloClv_Session)
        BaseII.CreateMyParameter("@Clv_Unicanet", SqlDbType.BigInt, gloClv_UnicaNet)
        Dim tblImpuestos As DataTable = BaseII.ConsultaDT("UspVerelDetalle")
        dgvConceptos.DataSource = tblImpuestos

    End Sub
End Class