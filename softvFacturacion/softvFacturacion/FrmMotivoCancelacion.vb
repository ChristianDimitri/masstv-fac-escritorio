﻿Public Class FrmMotivoCancelacion

    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub ButtonCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelar.Click
        Me.Close()
    End Sub

    Private Sub AgregaMotCan(ByVal oClv_Session As Long, ByVal oClv_MotCan As Integer)
        '@Clv_Session bigint,@Clv_Txt varchar(10),@op int,@Clv_Telefono int,@Contrato bigint
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, oClv_Session)
        BaseII.CreateMyParameter("@Clv_MotCan", SqlDbType.Int, oClv_MotCan)
        BaseII.EjecutaDT("Usp_InsertaMotCanFac")
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        AgregaMotCan(gloClv_Session, CmbPaqAdic.SelectedValue)
    End Sub

    Private Sub Llena_Mot_Can()
        BaseII.limpiaParametros()
        Dim DS As DataTable = BaseII.ConsultaDT("Up_MuestraMotCanFac")
        CmbPaqAdic.DisplayMember = "MOTCAN"
        CmbPaqAdic.ValueMember = "Clv_MOTCAN"
        CmbPaqAdic.DataSource = DS
    End Sub

    Private Sub FrmMotivoCancelacion_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        Llena_Mot_Can()
    End Sub
End Class