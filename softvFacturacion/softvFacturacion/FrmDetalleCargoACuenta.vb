﻿Imports System.Data.SqlClient

Public Class FrmDetalleCargoACuenta
    Dim PagoMens As Decimal
    Dim PagPen As Integer
    Dim Bandera As Boolean = False
    Private Sub FrmDetalleCargoACuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        Label1.ForeColor = Color.Black
        Label2.ForeColor = Color.Black
        Label3.ForeColor = Color.Black
        Label4.ForeColor = Color.Black
        Label5.ForeColor = Color.Black
        Label6.ForeColor = Color.Black

        UspMostrarAbonoACuenta(GloContrato)

        DameImportePagSiguiente()
        Me.Label4.Text = PagoMens
        Me.Label5.Text = PagPen

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'GloBndPagAbnCobBaj = False
        'BNDLIMPIAR = True
        Bandera = True
        InsertaPagoParaAbonoACuenta(gloClv_Session, PagoMens)
        GloAbonoACuenta = True
        GloBndPagAbnCobBaj = True
        Me.Close()
    End Sub

    Private Sub UspMostrarAbonoACuenta(ByVal PRMCONTRATO As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, PRMCONTRATO)
            DataGridView1.DataSource = BaseII.ConsultaDT("UspMostrarAbonoACuenta")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMostrarPreDetPagoAbonoACuenta(ByVal PRMCLVSESSION As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVSESSION", SqlDbType.BigInt, PRMCLVSESSION)
            DataGridView2.DataSource = BaseII.ConsultaDT("UspMostrarPreDetPagoAbonoACuenta")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        If DataGridView1.SelectedRows.Count > 0 Then
            UspMostrarPreDetPagoAbonoACuenta(CLng(DataGridView1.SelectedCells(0).Value))
            UspMostrarTblPagoAbonoACuenta(GloContrato)
        End If

    End Sub

    Private Sub ButtonPagoAbono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPagoAbono.Click
        GloBndPagAbnCobBaj = False
        eBotonGuardar = False
        GloClvSessionAbono = CLng(DataGridView1.SelectedCells(0).Value)
        GloImporteTotalEstadoDeCuenta = (DataGridView1.SelectedCells(1).Value - DataGridView1.SelectedCells(2).Value)
        FrmPagoAbonoACuenta.Show()
        Bandera = True
        Me.Close()

    End Sub

    Private Sub UspMostrarTblPagoAbonoACuenta(ByVal GloContrato As Long)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
            DataGridView3.DataSource = BaseII.ConsultaDT("UspMostrarTblPagoAbonoACuenta")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FrmDetalleCargoACuenta_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If Bandera = False Then
            GloBndPagAbnCobBaj = False
            BNDLIMPIAR = True
        End If

    End Sub

    Private Sub InsertaPagoParaAbonoACuenta(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarEconoPak", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = GloContrato
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@CLVSESSIONABONO", SqlDbType.BigInt)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = GloClvSessionAbono
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@BND", SqlDbType.BigInt)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = bnd
        comando.Parameters.Add(parametro5)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)

            Log_Descripcion = ex.Message.ToString
            Log_Formulario = Me.Name.ToString
            Log_ProcedimientoAlmacenado = "InsertaPagoParaAbonarEconoPak"
            GuardaLogError_Facturacion(Log_Descripcion, Log_Formulario, Log_ProcedimientoAlmacenado)

        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DameImportePagSiguiente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, GloContrato)
        BaseII.CreateMyParameter("@PagoMensual", ParameterDirection.Output, SqlDbType.Money)
        BaseII.CreateMyParameter("@PagPen", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameImportePagSiguiente")
        PagoMens = CInt(BaseII.dicoPar("@PagoMensual").ToString)
        PagPen = CDbl(BaseII.dicoPar("@PagPen").ToString)
    End Sub

End Class