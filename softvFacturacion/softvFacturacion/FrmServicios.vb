Imports System.Data.SqlClient

Public Class FrmServicios

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox1.SelectedValue = "CEXTV" Then
            GloClv_Txt = "CEXTV"
            FrmExtecionesTv.Show()
        ElseIf Me.ComboBox1.SelectedValue = "COEXP" Or Me.ComboBox1.SelectedValue = "COEX6" Then
            'AGREGE ESO  EDGAR EL 27/JUNIO/2011 PARA COBRO CONFIGURABLE
            GloBndExt = True
            GloClv_Txt = Me.ComboBox1.SelectedValue
            gloImpCoexp = TxtImporte.Text
        ElseIf Me.ComboBox1.SelectedValue = "CAMDO" Or Me.ComboBox1.SelectedValue = "CADIG" Or Me.ComboBox1.SelectedValue = "CANET" Or Me.ComboBox1.SelectedValue = "CADIC" Then
            'If Me.ComboBox1.SelectedValue = "CADIG" Then
            '    GloClv_Txt = "CADIG"
            'End If
            'If Me.ComboBox1.SelectedValue = "CANET" Then
            '    GloClv_Txt = "CANET"
            'End If
            'If Me.ComboBox1.SelectedValue = "CAMDO" Then
            '    GloClv_Txt = "CAMDO"
            'End If
            GloClv_Txt = ComboBox1.SelectedValue.ToString
            FormCAMDO.Show()

        ElseIf Me.ComboBox1.SelectedValue = "CEXTE" Then
            FrmCEXTETMP.Show()
        Else
            GloClv_Txt = Me.ComboBox1.SelectedValue
            GloBndExt = True
        End If
        Me.Close()
    End Sub

    Private Sub FrmServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameServiciosFacturacionTableAdapter.Connection = CON
        Me.DameServiciosFacturacionTableAdapter.Fill(Me.NewsoftvDataSet.DameServiciosFacturacion, GloContrato)
        CON.Close()
        'Edga 27/junio/2011 Cobro Configurable
        Me.LblImporte.ForeColor = Color.Black
        gloImpCoexp = 0
        '
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            'eDGAR 27/JUN/2011 AGREGO EL COBRO QUE SE PUEDE MODIFICAR EL IMPORTE CUANDO ELLOS QUIERAN

            Me.TxtImporte.Text = ""
            If ComboBox1.SelectedValue = "COEXP" Or ComboBox1.SelectedValue = "COEX6" Then
                Me.LblImporte.Visible = True
                Me.TxtImporte.Visible = True
            Else
                Me.LblImporte.Visible = False
                Me.TxtImporte.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TxtImporte_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtImporte.KeyPress
        e.KeyChar = Chr(ValidaKey(TxtImporte, Asc(LCase(e.KeyChar)), "L"))
    End Sub

  
End Class