Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmImprimirIngresosConcepto

    Private customersByCityReport As ReportDocument

    Private op As String = Nothing
    Private Titulo As String = Nothing
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    



    Private Sub ConfigureCrystalReports_IngresosDesglosados()

        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dS As New DataSet
        Dim Sucursal As String = " Sucursal: " + GloNomSucursal
        Dim Titulo As String = "Relación de Ingresos por Conceptos"
        Dim Ciudades As String = " Ciudad(es): " + LocCiudades
        dS = REPORTEDesglosePorConceptos()

        rDocument.Load(RutaReportes + "\Genera_Poliza_New_Desgloce_Saltillo.rpt")

        rDocument.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
        rDocument.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        rDocument.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

        rDocument.SetDataSource(dS)
        CrystalReportViewer1.ReportSource = rDocument
        CrystalReportViewer1.Zoom(100)
        CrystalReportViewer1.ShowPrintButton = True
        CrystalReportViewer1.ShowExportButton = True
        CrystalReportViewer1.ShowRefreshButton = False

        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo


        'Dim reportPath As String = Nothing
        'Dim Titulo As String = Nothing
        'Dim Sucursal As String = Nothing
        'Dim Ciudades As String = Nothing
        'Sucursal = " Sucursal: " + GloNomSucursal
        'Titulo = "Relación de Ingresos por Conceptos"
        'Ciudades = " Ciudad(es): " + LocCiudades
        'reportPath = RutaReportes + "\Genera_Poliza_New_Desgloce_Saltillo.rpt"
        ''Dim cnn As New SqlConnection(MiConexion)
        ''Dim cmd As New SqlCommand("Genera_Poliza_New_Desgloce_Saltillo", cnn)
        ''cmd.CommandType = CommandType.StoredProcedure
        ''cmd.CommandTimeout = 0
        ''Dim parametro1 As New SqlParameter("@Clv_llave_Poliza", SqlDbType.BigInt)
        ''parametro1.Direction = ParameterDirection.Input
        ''parametro1.Value = LocGloClv_poliza
        ''cmd.Parameters.Add(parametro1)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Dim comando As SqlClient.SqlCommand
        'comando = New SqlClient.SqlCommand
        'With comando
        '    .Connection = CON
        '    '.CommandText = "GeneraPoliza "
        '    .CommandText = "Genera_Poliza_New_Desgloce_Saltillo"
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandTimeout = 0
        '    '  varchar(10)
        '    Dim prm As New SqlParameter("@Fecha_Ini", SqlDbType.DateTime)
        '    prm.Direction = ParameterDirection.Input
        '    prm.Value = eFechaInicial
        '    .Parameters.Add(prm)

        '    Dim prm1 As New SqlParameter("@Fecha_Fin", SqlDbType.DateTime)
        '    prm1.Direction = ParameterDirection.Input
        '    prm1.Value = eFechaFinal
        '    .Parameters.Add(prm1)

        '    Dim prm2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        '    prm2.Direction = ParameterDirection.Input
        '    prm2.Value = "C"
        '    .Parameters.Add(prm2)

        '    Dim prm3 As New SqlParameter("@sucursal", SqlDbType.Int)
        '    prm3.Direction = ParameterDirection.Input
        '    prm3.Value = 0
        '    .Parameters.Add(prm3)

        '    Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
        '    prm4.Direction = ParameterDirection.Input
        '    prm4.Value = 0
        '    .Parameters.Add(prm4)

        '    Dim prm5 As New SqlParameter("@Cajera", SqlDbType.VarChar)
        '    prm5.Direction = ParameterDirection.Input
        '    prm5.Value = GloUsuario
        '    .Parameters.Add(prm5)

        '    Dim prm6 As New SqlParameter("@Op", SqlDbType.VarChar)
        '    prm6.Direction = ParameterDirection.Input
        '    prm6.Value = 0
        '    .Parameters.Add(prm6)

        '    Dim prm7 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        '    prm7.Direction = ParameterDirection.Input
        '    prm7.Value = gloClv_Session
        '    .Parameters.Add(prm7)

        '    Dim prm8 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar, 11)
        '    prm8.Direction = ParameterDirection.Input
        '    prm8.Value = GloUsuario
        '    .Parameters.Add(prm8)

        '    'Dim i As Integer = comando.ExecuteNonQuery()

        'End With

        'Dim da As New SqlDataAdapter(comando)
        'Dim ds As New DataSet()
        'da.Fill(ds)
        'ds.Tables(0).TableName = "Genera_Poliza_New_Desgloce_Saltillo"
        'customersByCityReport.Load(reportPath)
        'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        ''customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        ''customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloCiudadEmpresa & "'"

        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        'eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
        'customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"
        ''customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'"
        'customersByCityReport.SetDataSource(ds)
        'CrystalReportViewer1.ReportSource = customersByCityReport
        'CrystalReportViewer1.Zoom(100)
        'Me.CrystalReportViewer1.ShowPrintButton = True
        'Me.CrystalReportViewer1.ShowExportButton = True
        'Me.CrystalReportViewer1.ShowRefreshButton = False
    End Sub


    Private Sub ConfigureCrystalReports()
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing
        Dim Titulo As String = Nothing
        Dim Sucursal As String = Nothing
        Dim Ciudades As String = Nothing
        Ciudades = " Ciudad(es): " + LocCiudades
        If GloBnd_Des_Cont = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Contrataciones.rpt"
            Titulo = "Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            reportPath = RutaReportes + "\ReporteDesgloce_Mensualidades.rpt"
            Titulo = "Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            LocClientesPagosAdelantados = False
            reportPath = RutaReportes + "\ClientesconPagosAdelantados.rpt"
            Titulo = "Relación de Clientes con Pagos Adelantados"
        ElseIf LocClientesPagosAdelantados = False Then
            reportPath = RutaReportes + "\DesgloceporConceptos.rpt"
            Titulo = "Relación de Ingresos por Conceptos"
        End If
        Sucursal = " Sucursal: " + GloNomSucursal
        customersByCityReport.Load(reportPath)

        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

        '@FECHA_INI
        customersByCityReport.SetParameterValue(0, eFechaInicial)
        '@FECHA_FIN
        customersByCityReport.SetParameterValue(1, eFechaFinal)
        '@TIPO
        customersByCityReport.SetParameterValue(2, "")
        '@SUCURSAL
        customersByCityReport.SetParameterValue(3, "0")
        '@CAJA
        customersByCityReport.SetParameterValue(4, "0")
        '@CAJERA
        customersByCityReport.SetParameterValue(5, "")
        '@OP
        customersByCityReport.SetParameterValue(6, "0")
        'Clv_Session
        customersByCityReport.SetParameterValue(7, gloClv_Session)




        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
        eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.Zoom(75)



        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
        GloBnd_Des_Men = False
        GloBnd_Des_Cont = False

    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub




    Private Sub FrmImprimirIngresosConcepto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If GloBnd_Des_Cont = True Then
            Me.Text = "Impresión Desgloce de Contrataciones"
        ElseIf GloBnd_Des_Men = True Then
            Me.Text = "Impresión Desgloce de Mensualidades"
        ElseIf LocClientesPagosAdelantados = True Then
            Me.Text = "Impresión Relación de Clientes con Pagos Adelantados"
        Else
            Me.Text = "Impresión Relación de Ingresos por Conceptos"
            ConfigureCrystalReports_IngresosDesglosados()
            Exit Sub
        End If

        ConfigureCrystalReports()
        Me.CrystalReportViewer1.ShowPrintButton = True
        Me.CrystalReportViewer1.ShowExportButton = True
        Me.CrystalReportViewer1.ShowRefreshButton = False
    End Sub

    Private Function REPORTEDesglosePorConceptos() As DataSet
        Dim listaTablas As New List(Of String)
        listaTablas.Add("Genera_Poliza_New_Desgloce_Saltillo")
        listaTablas.Add("tblCompanias")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, eFechaInicial)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, eFechaFinal)
        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, "C", 1)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, GloUsuario, 11)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, gloClv_Session)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, GloUsuario, 10)
        BaseII.CreateMyParameter("@CLVCOMPANIA", SqlDbType.Int, GloClvCompania)
        REPORTEDesglosePorConceptos = BaseII.ConsultaDS("REPORTEDesglosePorConceptos", listaTablas)
    End Function

End Class