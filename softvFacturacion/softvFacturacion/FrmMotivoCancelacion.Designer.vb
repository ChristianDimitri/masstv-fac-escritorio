﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMotivoCancelacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonCancelar = New System.Windows.Forms.Button()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CmbPaqAdic = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'ButtonCancelar
        '
        Me.ButtonCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonCancelar.ForeColor = System.Drawing.Color.Black
        Me.ButtonCancelar.Location = New System.Drawing.Point(299, 85)
        Me.ButtonCancelar.Name = "ButtonCancelar"
        Me.ButtonCancelar.Size = New System.Drawing.Size(124, 43)
        Me.ButtonCancelar.TabIndex = 10
        Me.ButtonCancelar.Text = "&CANCELAR"
        Me.ButtonCancelar.UseVisualStyleBackColor = False
        Me.ButtonCancelar.Visible = False
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.ForeColor = System.Drawing.Color.Black
        Me.ButtonAceptar.Location = New System.Drawing.Point(118, 85)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(184, 43)
        Me.ButtonAceptar.TabIndex = 9
        Me.ButtonAceptar.Text = "&Siguiente >>"
        Me.ButtonAceptar.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 26)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(290, 16)
        Me.CMBLabel1.TabIndex = 8
        Me.CMBLabel1.Text = "Seleccione el Motivo de la Cancelación :"
        '
        'CmbPaqAdic
        '
        Me.CmbPaqAdic.DisplayMember = "DescripcionFac"
        Me.CmbPaqAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbPaqAdic.FormattingEnabled = True
        Me.CmbPaqAdic.Location = New System.Drawing.Point(15, 45)
        Me.CmbPaqAdic.Name = "CmbPaqAdic"
        Me.CmbPaqAdic.Size = New System.Drawing.Size(408, 24)
        Me.CmbPaqAdic.TabIndex = 7
        Me.CmbPaqAdic.ValueMember = "Clv_Txt"
        '
        'FrmMotivoCancelacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 143)
        Me.ControlBox = False
        Me.Controls.Add(Me.ButtonCancelar)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CmbPaqAdic)
        Me.Name = "FrmMotivoCancelacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Motivo de la Cancelación "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonCancelar As System.Windows.Forms.Button
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CmbPaqAdic As System.Windows.Forms.ComboBox
End Class
