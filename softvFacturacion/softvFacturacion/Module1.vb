Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine


Module Module1

    '*****Pago abonar a cuenta Inicio
    Public GloBndPagAbnCobBaj As Boolean = False
    Public Log_Descripcion As String
    Public Log_Formulario As String
    Public Log_ProcedimientoAlmacenado As String
    Public GloImporteTotalEstadoDeCuenta As Decimal = 0
    Public GloClvSessionAbono As Long = 0
    Public BNDLIMPIAR As Boolean = False
    '*****Pago abonar a cuenta fin

    Public opcionClientes As Boolean = False
    Public eMsj As String = Nothing
    Public GloClvCompania As Integer = 0
    'EDGAR
    '***********************
    Public gLOoPrEPpOLIZA As Integer = 0
    Public path As String = ""
    '27/JUNIO/2011 COBRO CON MONTO CONFIGURABLE
    'inicio
    Public gloImpCoexp As Double = 0
    Public miLetra2 As String = Nothing
    'Fin
    'Factura Digital
    Public MClv_Session As Long = 0
    'Factura Digital
    Public MiConexion As String = Nothing
    '***********************
    'Eric Variables
    Public bnd1pardep1 As Integer = 0
    Public bnd1pardep2 As Integer = 0
    Public bnd2pardep As Boolean = False
    Public bnd2pardep1 As Boolean = False
    Public locclvsessionpardep As Long = 0
    Public Loccontratopardep As Long = 0
    Public locbndborracabiosdep As Boolean = False
    'Edgar me Va servir para SAber si es un Estado de cuenta o un Cobro Normal
    Public GloOpCobro As Integer = 0
    Public GloCEXTTE As Boolean = False
    Public GloComproEquipo As Boolean = False
    Public GloAbonoACuenta As Boolean = False
    Public LocBndrelingporconceptos As Boolean = False
    Public eBotonGuardar As Boolean
    Public LocBndNotasReporteTick As Boolean = False
    Public locoprepnotas As Integer = 0
    Public Repetido As Boolean
    Public eFechaInicial, eFechaFinal As Date
    Public eFechaTitulo As String
    Public eCveFactura As Long
    Public eEntra As Boolean
    Public eReImprimirF As Integer = 0
    Public eEntraReImprimir As Boolean
    Public eMotivoBonificacion As String = Nothing
    Public eLoginCajera As String = Nothing
    Public ePassCajera As String = Nothing
    Public eConModDesglose As Boolean = False
    Public eCveCajera As String = Nothing
    Public eCorresponde As Boolean = False
    Public eClv_Progra As Long = 0
    Public eClv_Txt As String = 0
    Public eFechaServidor As Date
    Public eRes As Integer = 0
    Public eBndPPE As Boolean = False
    Public eAccesoAdmin As Boolean = False
    Public eClv_Session As Long = 0
    Public eClvTipSer As Integer = 0
    Public eDescripcion As String = String.Empty
    Public eNumero As Integer = 0
    Public eMonto As Decimal = 0
    Public eFechaIni As Date
    Public eFechaFin As Date
    Public eOp As Integer = 0
    Public eOperador As String = String.Empty
    Public eOpRep As Integer = 0
    Public eBndDatosFiscales As Boolean = False
    Public eBndMembretadas As Boolean = False
    'Public GloClv_Detalle As Long = 0
    Public NoRevistas As Integer = 0
    '**************************************
    Public RangoFacturasIni As Integer
    Public RangoFacturasFin As Integer
    Public RutaReportes As String
    Public OPCION As Char
    Public BLOQUEA As Boolean = False
    Public band As Boolean = False
    Public locnomsupervisor As String = Nothing
    Public bec_tipo As String = Nothing
    Public bec_impIva As Decimal
    Public bec_serie As String
    Public bec_letra As String
    Public bec_factura As String
    Public bec_fecha As String
    Public bec_importe As String
    Public bec_bandera As Integer = 0
    Public bec_consecutivo As Integer
    Public GLOMENUS As Integer
    Public glomenu As String
    Public GloBnd As Boolean
    Public GloAdelantados As Integer
    Public gloClv_Session As Long = 0
    Public gloClv_Servicio As Long = 0
    Public gloClv_llave As Long = 0
    Public gloClv_UnicaNet As Long = 0
    Public gloClave As Long = 0
    Public OrdRetiro As Integer = 0
    Public GloContrato As Long = 0
    Public GloContratoSaldo As Long = 0
    Public StatusDesc As Integer = 0
    Public Recon As Integer = 0
    Public EcoMeses As Integer = 0
    Public EcoImpMin As Double = 0
    Public BndEco As Boolean = False
    Public TipoDoB As String
    Public IdSistema As String = " "
    Public GloBndExt As Boolean = False
    Public GloExt As Integer = 0
    Public GloClv_Txt As String = Nothing
    Public GloCajera As String = "SISTE"
    Public GLONOMCAJERAARQUEO As String = Nothing
    Public GloSucursal As Integer = 0
    Public GloCaja As Integer = 0
    Public GlonOMCaja As String
    Public GloClv_Factura As Long = 0
    Public GloIpMaquina As String = Nothing
    Public GloHostName As String = Nothing
    Public GloUsuario As String = Nothing
    Public op As String
    Public Fecha_ini As String
    Public Fecha_Fin As String
    Public NomCajera As String
    Public NomCaja As String
    Public NomSucursal As String
    Public BanderaReporte As Boolean = False
    Public ExtraT As String
    Public Resumen As Boolean = False
    Public Resumen1 As Integer
    Public Resumen2 As Integer
    Public Resumen3 As Boolean = False
    Public GloReporte As Integer = 0
    Public GloClv_SessionBancos As Long = 0
    Public GloTitulo As String = Nothing
    Public GloSubTitulo As String = Nothing
    Public GloBndControl As Boolean = False
    Public GloTipo As String = "C"
    Public GloServerName As String = Nothing
    Public GloDatabaseName As String = Nothing
    Public GloUserID As String = Nothing
    Public GloPassword As String = Nothing
    Public SelCajaResumen As Integer
    Public FlagCaja As Boolean = False
    Public Consecutivo As Long = 0
    Public Suma As Double = 0
    Public GloConsecutivo As Long
    Public loctitulo As String = Nothing
    Public gloClv_Detalle As Long = 0
    Public GloDes_Ser As String = Nothing
    Public locband_pant As Integer = 0
    Public GloNomSucursal As String
    Public GloOpFacturas As Integer = 0
    Public GLOIMPTOTAL As Double = 0
    Public GLOSIPAGO As Integer = 0
    Public GloTipoUsuario As Integer
    Public glolec As Integer
    Public gloescr As Integer
    Public gloctr As Integer
    Public GloBonif As Integer = 0
    Public GloImprimeTickets As Boolean = False
    Public LocNomEmpresa As String = Nothing
    '--Variables Generales de Facturacion
    Public GloEmpresa As String = Nothing
    Public GloDireccionEmpresa As String = Nothing
    Public GloColonia_CpEmpresa As String = Nothing
    Public GloCiudadEmpresa As String = Nothing
    Public GloRfcEmpresa As String = Nothing
    Public GloTelefonoEmpresa As String = Nothing
    '--Variables para el tipo de Pago
    Public GLOCLV_NOTA As Long = 0
    Public GLONOTA As Double = 0
    Public GLOEFECTIVO As Double = 0
    Public GLOCHEQUE As Double = 0
    Public GLOCLV_BANCOCHEQUE As Integer = 0
    Public NUMEROCHEQUE As String = Nothing
    Public GLOTARJETA As Double = 0
    Public GLOCLV_BANCOTARJETA As Integer = 0
    Public NUMEROTARJETA As String = Nothing
    Public TARJETAAUTORIZACION As String = Nothing
    Public Glocontratosel As Long = 0
    Public Glocontratosel2 As Long = 0
    ' Fin varirables para el tipo de Pago

    Public GLOTRANSFERENCIA As Double = 0
    Public GLOBANCO As Integer = 0
    Public GLONUMEROTRANSFERENCIA As String = ""
    Public GLOAUTORIZACION As String = ""

    Public Glo_Clv_SessionVer As Long = 0
    Public Glo_BndErrorVer As Integer = 0
    Public Glo_MsgVer As String = Nothing

    Public AuxImprimirTicket As Integer = 0

    Public GloOpRepGral As String = Nothing
    Public LocEfectivo As Decimal = 0
    Public LocTarjeta As Decimal = 0
    Public LocCheque As Decimal = 0
    Public LocAuto As Decimal = 0
    Public LocParciales As Decimal = 0
    Public LocDesglose As Decimal = 0
    Public LocClv_session As Integer = 0
    Public Gloop As String = Nothing
    Public Glo_Apli_Pnt_Ade As Boolean = False

    Public GloClv_Periodo_Num As Integer = 1
    Public GloClv_Periodo_Txt As String = " Primer Periodo "
    Public GloActPeriodo As Integer = 0
    Public facnormal As Boolean
    Public facticket As Boolean
    Public impresorafiscal As String
    Public Locclv_sucursalglo As String = Nothing
    Public Tipo As String = Nothing
    Public LocSerieglo As String = Nothing
    Public LocFacturaGlo As Integer = 0
    Public LocFechaGlo As Date
    Public LocImporteGlo As Double = 0
    Public LocCajeroGlo As String = Nothing
    Public Locclv_empresa As String = Nothing

    ''VARIABLES PARA ESPECIFICACIONES---------
    Public ColorBut As Integer
    Public ColorLetraBut As Integer
    Public ColorMenu As Integer
    Public ColorMenuLetra As Integer
    Public ColorBwr As Integer
    Public ColorBwrLetra As Integer
    Public ColorGrid As Integer
    Public ColorForm As Integer
    Public ColorLetraForm As Integer
    Public ColorLabel As Integer
    Public ColorLetraLabel As Integer
    Public bytesImg() As Byte
    Public bytesImg2() As Byte
    Public num, num2 As Integer
    Public bfac As softvFacturacion.NewsoftvDataSet2.BusFacFiscalDataTable
    Public busfac As NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Public horaini As String
    Public horafin As String
    Public bndcontt As Boolean
    Public res As Integer
    Public LocImpresoraTickets As String = Nothing
    Public LocFecha1 As String = Nothing
    Public LocFecha2 As String = Nothing
    Public LocResumenBon As Boolean = False
    Public LocBndBon As Boolean = False
    Public locBndBon1 As Boolean = False
    Public LocSupBon As String = Nothing
    Public LocLoginUsuario As String = Nothing
    Public LocBanderaRep1 As Integer = 0
    Public LocBndrepfac1 As Boolean = False
    Public Locclv_usuario As String = Nothing
    Public LocNombreusuario As String = Nothing
    Public Factura_inicial As Integer
    Public Factura_final As Integer
    Public Unico As Boolean = True
    Public LocCiudades As String = Nothing
    Public gloMotivoCan As Integer
    Public gloClvNota As Integer
    Public GloPoliza2 As Long
    Public glotipoNota As Short
    Public glotipoFacGlo As Short
    Public SubCiudad As String
    Public bnd As Integer = 0
    Public LiTipo As Integer = 0
    Public GloSistema As String = "Facturaci�n"
    Public LiContrato As Long
    Public LocFechaGloFinal As String
    Public Glo_tipSer As Integer
    'variables de Datos
    Public locclv_id As String
    Public LocBdd As String
    Public LocClv_Ciudad As String
    Public LocNomciudad As String
    Public LIFACTURA As Long
    Public refrescar As Boolean = False
    Public status As String
    Public LocbndNotas As Boolean = False
    Public LocActiva As Boolean = False
    Public LocSaldada As Boolean = False
    Public LocCancelada As Boolean = False
    Public LocUsuariosNotas As String = Nothing
    Public LocClientesPagosAdelantados As Boolean = False
    'Proceso de Bancos
    Public BndPasaBancos As Boolean = False
    Public FechaPasaBancos As String = Nothing
    'Fin Proceso de Bancos
    'proceso de Oxxo
    Public GloClv_Recibo As Long = 0
    'Fin Proceso de Oxxo
    'Polizas
    Public Locbndactualizapoiza As Boolean = True
    Public LocopPoliza As String = "C"
    Public LocGloClv_poliza As Long = 0
    Public LocbndPolizaCiudad As Boolean = False
    'Polizas
    'Reporte_Desgloce_Moneda
    Public GloBnd_Des_Men As Boolean = False
    'Clave Session para Facturacion
    Public GloTelClv_Session As Long = 0
    Public GloTiene_Cancelaciones As Boolean = False

    '
    Public GloPagorecibido As Double = 0
    'Reporte_Desgloce_Moneda Contratacion
    Public GloBnd_Des_Cont As Boolean = False
    Public Locbndcortedet As Boolean = False

    Public Locbndrepnotas As Integer = 0
    Public locbndrepnotas2 As Boolean = False
    Public Locclv_sucursalnotas As Integer = 0
    Public Verifica As Integer
    Public BndAlerta As Boolean
    Public LocbndDesPagos As Boolean = False
    Public guardabitacora As softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
    Public guardabitacorabuena As softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter
    'SAUL   
    Public GloCity As String = Nothing
    Public GloNotasC As Boolean = False
    '(Fin)

    Public opcionCuenta As Integer = 1


    Public Function SP_REFERENCIA_OXXOFactura(ByVal eClv_Factura As Long) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            SP_REFERENCIA_OXXOFactura = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "SP_REFERENCIA_OXXOFactura"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eClv_Factura
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Texto", SqlDbType.VarChar, 50)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)
                Dim i As Integer = .ExecuteNonQuery
                SP_REFERENCIA_OXXOFactura = prm3.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()

        End Try
    End Function

    Public Function SP_REFERENCIA_OXXO(ByVal eContrato As Long) As String
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            'Regresa La Session
            SP_REFERENCIA_OXXO = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "SP_REFERENCIA_OXXO"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = eContrato
                .Parameters.Add(prm1)

                Dim prm3 As New SqlParameter("@Texto", SqlDbType.VarChar, 50)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = 0
                .Parameters.Add(prm3)
                Dim i As Integer = .ExecuteNonQuery
                SP_REFERENCIA_OXXO = prm3.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()

        End Try
    End Function

    Public Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Dim COn85 As New SqlConnection(MiConexion)
        COn85.Open()
        guardabitacora = New softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
        guardabitacorabuena = New softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter
        COn85.Close()
        If valorant <> valornuevo Then
            COn85.Open()
            guardabitacorabuena.Connection = COn85
            guardabitacorabuena.Fill(guardabitacora, usuario, contrato, sistema, pantalla, control, valorant, valornuevo, clv_ciudad)
            COn85.Close()
        End If
    End Sub


    Public Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            menu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
            menu = Nothing
            If oOpcionMenu.DropDownItems.Count > 0 Then
                RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Public Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                submenu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
                submenu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub
    Public Sub bwrpanel(ByVal panel2 As Panel)
        Dim data As DataGridView
        Dim label As Label
        Dim boton As Button
        Dim split As SplitContainer
        Dim panel3 As Panel
        Dim text As TextBox
        Dim GROUP As GroupBox
        Dim var As String
        If panel2.BackColor <> Color.WhiteSmoke Then
            panel2.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
            ' panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
        End If
        For Each ctr As Control In panel2.Controls
            If ctr.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctr
                bwrpanel(split.Panel1)
                bwrpanel(split.Panel2)
            End If
            If ctr.GetType Is GetType(CrystalDecisions.Windows.Forms.CrystalReportViewer) Then
                ctr.BackColor = Color.WhiteSmoke
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                data = New DataGridView
                data = ctr
                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                data = Nothing
            End If
            var = Mid(ctr.Name, 1, 3)
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctr.BackColor <> Color.WhiteSmoke And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Black
                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Red
                label.BackColor = Color.Yellow
                label = Nothing
            End If

            If ctr.GetType Is GetType(System.Windows.Forms.TextBox) And var = "CMB" Then
                text = New TextBox
                text = ctr
                text.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                text.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                text = Nothing
            End If


            If ctr.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctr
                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                boton = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                panel3 = New Panel
                panel3 = ctr
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GROUP = New GroupBox
                GROUP = ctr
                GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                bwrgroup(GROUP)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var <> "CMB" Then
                bwrpanel(ctr)
            End If
        Next
    End Sub
    Public Sub bwrgroup(ByVal grup As GroupBox)
        Dim panel3 As Panel
        Dim label As Label
        Dim var As String
        For Each ctm As Control In grup.Controls
            var = Mid(ctm.Name, 1, 3)
            If ctm.GetType Is GetType(System.Windows.Forms.Panel) And ctm.BackColor <> Color.WhiteSmoke Then
                panel3 = New Panel
                panel3 = ctm
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctm.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctm.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctm
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
        Next
    End Sub

    Public Sub colorea(ByVal formulario As Form)
        Dim boton As Button
        Dim panel As Panel
        Dim label As Label
        Dim split As SplitContainer
        Dim var As String
        Dim data As DataGridView
        Dim Menupal As MenuStrip
        Dim GROUP As GroupBox

        For Each ctl As Control In formulario.Controls
            var = Mid(ctl.Name, 1, 3)

            If var <> "CNO" Then

                If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                    boton = New Button
                    boton = ctl
                    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                    boton = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.MenuStrip) Then
                    Menupal = New MenuStrip
                    Menupal = ctl
                    Menupal.BackColor = System.Drawing.Color.FromArgb(ColorMenu)
                    RecorrerEstructuraMenu(Menupal)
                    Menupal = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    split = New SplitContainer
                    split = ctl
                    bwrpanel(split.Panel1)
                    bwrpanel(split.Panel2)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor <> Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    panel.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    bwrpanel(panel)
                    panel = Nothing

                ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    GROUP = New GroupBox
                    GROUP = ctl
                    GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    bwrgroup(GROUP)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor = Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    bwrpanel(panel)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = System.Drawing.Color.FromArgb(ColorLetraLabel)
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Red
                    label.BackColor = Color.WhiteSmoke
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Black
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    data = New DataGridView
                    data = ctl
                    data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    If ctl.Name = "SumaDetalleDataGridView" Then
                        data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorForm)
                    Else
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                    End If
                    data = Nothing
                End If
            End If
        Next
        formulario.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        formulario.ForeColor = System.Drawing.Color.FromArgb(ColorLetraForm)
    End Sub

    Public Function ValidaKey(ByVal ctl As Object, ByRef nChar As Integer, ByVal Tipo As String) As Integer
        'Solo Enteros
        If Tipo = "N" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'Con Enteros y Decimales
        ElseIf Tipo = "L" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Or nChar = 46 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'String
        ElseIf Tipo = "S" Then
            nChar = Asc(LCase(Chr(nChar)))
            If ctl.SelectionStart = 0 Then
                ' This is the first character, so change to uppercase.
                nChar = Asc(UCase(Chr(nChar)))
            Else
                ' If the previous character is a space, capitalize
                ' the current character.
                'If Mid(ctl, ctl.SelectionStart, 1) = Space(1) Then
                'MsgBox(Mid(ctl.ToString, ctl.SelectionStart, 1))
                'MsgBox(Mid(ctl.text, ctl.SelectionStart, 1))
                If Mid(ctl.text, ctl.SelectionStart, 1) = Space(1) Then
                    nChar = Asc(UCase(Chr(nChar)))
                End If
            End If
            ValidaKey = nChar
        ElseIf Tipo = "M" Then
            nChar = Asc(UCase(Chr(nChar)))
            ValidaKey = nChar
            'ElseIf Tipo = "D" Then
            ' If KeyAscii <> 64 Then
            '     a = Right(Txt, 1)
            '     L = Len(Txt)
            '     If a = " " Or L = 0 Or a = "." Then
            '         ValidaKey = (Asc(UCase(Chr(KeyAscii))))
            '     Else
            '         ValidaKey = (Asc(LCase(Chr(KeyAscii))))
            '     End If
            '  End If
        End If
    End Function

    Public Function DAMESclv_Sessionporfavor() As Long
        Dim cON_x As New SqlConnection(MiConexion)
        Try
            DAMESclv_Sessionporfavor = 0
            Dim cmd As New SqlCommand
            cON_x.Open()
            With cmd
                .CommandText = "DameClv_Session_Servicios"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = cON_x
                Dim prm2 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery
                DAMESclv_Sessionporfavor = prm2.Value
            End With
            cON_x.Close()
        Catch ex As Exception
            cON_x.Close()
        End Try
    End Function

    Function DesEncriptaME(ByVal Pass As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Dim j As Integer

        Clave = "%��T@#$A_"
        Pass2 = ""
        j = 1
        For i = 1 To Len(Pass) Step 2
            CAR = Mid(Pass, i, 2)
            Codigo = Mid(Clave, ((j - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Chr(Asc(Codigo) Xor Val("&h" + CAR))
            j = j + 1
        Next i
        DesEncriptaME = Pass2
    End Function


    Function Rellena_Text(ByVal Valor As String, ByVal Longitud As Integer, ByVal Relleno As String) As String
        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Contador = Len(Valor)
        Rellena_Text = ""
        Total = Longitud - Contador
        Dim i As Integer
        i = 1
        For i = 1 To Total
            Rellena_Text = Rellena_Text + Relleno
            'i = i + 1
        Next i
        Rellena_Text = Rellena_Text + Valor
    End Function

    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Function ValidaDecimal(ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal Text As TextBox) As Integer

        Dim dig As Integer = Len(Text.Text & e.KeyChar)
        Dim a, esDecimal, NumDecimales As Integer
        Dim esDec As Boolean
        ' se verifica si es un digito o un punto 
        If e.KeyChar.IsDigit(e.KeyChar) Or e.KeyChar = "." Then
            e.Handled = False
        ElseIf e.KeyChar.IsControl(e.KeyChar) Then
            e.Handled = False
            Return a
        Else
            e.Handled = True
        End If
        ' se verifica que el primer digito ingresado no sea un punto al seleccionar
        If Text.SelectedText <> "" Then
            If e.KeyChar = "." Then
                e.Handled = True
                Return a
            End If
        End If
        If dig = 1 And e.KeyChar = "." Then
            e.Handled = True
            Return a
        End If
        'aqui se hace la verificacion cuando es seleccionado el valor del texto
        'y no sea considerado como la adicion de un digito mas al valor ya contenido en el textbox
        If Text.SelectedText = "" Then
            ' aqui se hace el for para controlar que el numero sea de dos digitos - contadose a partir del punto decimal.
            For a = 0 To dig - 1
                Dim car As String = CStr(Text.Text & e.KeyChar)
                If car.Substring(a, 1) = "." Then
                    esDecimal = esDecimal + 1
                    esDec = True
                End If
                If esDec = True Then
                    NumDecimales = NumDecimales + 1
                End If
                ' aqui se controla los digitos a partir del punto numdecimales = 4 si es de dos decimales 
                If NumDecimales >= 4 Or esDecimal >= 2 Then
                    e.Handled = True
                End If
            Next
        End If
    End Function
    Public Sub GuardaLogError_Facturacion(ByRef Descripcion As String, ByRef Formulario As String, ByRef ProcedimientoAlmacenado As String)


        Dim CON80 As New SqlClient.SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlCommand()

        Try
            CMD = New SqlClient.SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "sp_Guarda_LogError_Facturacion"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prmDescripcion As New SqlParameter("@Descripcion", SqlDbType.Text)
                prmDescripcion.Value = Log_Descripcion
                .Parameters.Add(prmDescripcion)

                Dim prmFormulario As New SqlParameter("@Formulario", SqlDbType.Text)
                prmFormulario.Value = Log_Formulario
                .Parameters.Add(prmFormulario)

                Dim prmProcedimientoAlmacenado As New SqlParameter("@ProcedimientoAlmacenado", SqlDbType.Text)
                prmProcedimientoAlmacenado.Value = Log_ProcedimientoAlmacenado
                .Parameters.Add(prmProcedimientoAlmacenado)

                Dim prmCiudad As New SqlParameter("@Ciudad", SqlDbType.Text)
                prmCiudad.Value = GloEmpresa
                .Parameters.Add(prmCiudad)

                Dim i As Integer = .ExecuteNonQuery()

            End With
            CON80.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module

